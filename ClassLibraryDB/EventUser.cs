﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp.DBClasses
{
    public class EventUser
    {
        public Event _Event { get; set; }
        public User _User { get; set; }
        public DateTime? AcknowledgeTime { get; set; }
        public string AcknowledgeText { get; set; }

        public EventUser(Event _event, User user, DateTime? acknowledgeTime, string acknowledgeText)
        {
            _Event = _event;
            _User = user;
            AcknowledgeTime = acknowledgeTime;
            AcknowledgeText = acknowledgeText;
        }
    }
}
