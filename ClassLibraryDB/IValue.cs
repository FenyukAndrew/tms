﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp.DBClasses
{
    public interface IValue
    {
        int getId();
        string getValue();
    }
}
