﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp
{
    public class User : IValue, IComparable<User>
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public int Services { get; set; }
        public Boolean Delete { get; set; }
        public int Workgroups { get; set; }
        public Boolean Approval { get; set; }

        public User(int id, string fullName, string phone, string email, string login, int services, Boolean delete, int workgroups, Boolean approval)
        {
            Id = id;
            FullName = fullName;
            Phone = phone;
            Email = email;
            Login = login;
            Services = services;
            Delete = delete;
            Workgroups = workgroups;
            Approval = approval;
        }

        public string getValue()
        {
            return FullName;
        }

        public int CompareTo(User other)
        {
            return this.FullName.CompareTo(other.FullName);
        }

        public int getId()
        {
            return Id;
        }

        public User()
        {

        }
    }
}
