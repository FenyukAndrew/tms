﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TaskApp
{
    [Serializable]
    public class ConnectionSettings
    {
        public string ServerName { get; set; }
        public string ServerPort { get; set; }
        public string DatabaseName { get; set; }
        public string LoginName { get; set; }
        [NonSerialized]
        private string password;
        [XmlIgnore]
        public string Password { get { return password; } set { password = value; } }

        public ConnectionSettings()
        {

        }

        public string UpdateConnectionString(string pass = "", string user = "")
        {
            return "server=" + ServerName +
                (ServerPort != "" ? (";port=" + ServerPort) : "") +
                ";user=" + (user == "" ? LoginName : user) +
                ";database=" + DatabaseName +
                ";password=" + (pass == "" ? Password : pass) +
                ";" + "Allow Zero Datetime = True;charset=utf8" +
                //";Connect Timeout = 3000" +
                ";default command timeout=3600;";
        }
    }
}
