﻿#define TYPE_VERSION_FOR_MASTER

namespace TaskApp
{
    public class SELECT_VERSION
        {
        //сервера Академии model._Settings.UpdateServerURL = "https://195.239.107.138:88/up/";

#if (TYPE_VERSION_FOR_MASTER)
        public static readonly string AdditionalTitle = " Диспетчер";
        public static readonly string server_update = "http://10.8.0.45/Upgrade_Master/";
        public static readonly bool visibleRealCustomer = true;
#else
        public static readonly string AdditionalTitle = "";
        public static readonly string server_update = "http://10.8.0.45/Upgrade/";
        public static readonly bool visibleRealCustomer = false;
#endif

        public SELECT_VERSION()
            {
            }
        }
}
