﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormComment
{
    public partial class CommentForm : Form, ICommentForm
    {
        public CommentForm()
        {
            InitializeComponent();
        }

        public bool ButtonOkEnabled
        {
            get { return buttonOk.Enabled; }
            set { buttonOk.Enabled = value; }
        }

        public string FormText
        {
            set { Text = value; }
        }

        public bool PictureBoxVisable
        {
            get { return pictureBoxWait.Visible; }
            set { pictureBoxWait.Visible = value; }
        }

        public bool TextBoxCommentEnabled
        {
            get { return textBoxComment.Enabled; }
            set { textBoxComment.Enabled = value; }
        }

        public string TextBoxCommentText
        {
            get { return textBoxComment.Text; }
            set { textBoxComment.Text = value; }
        }

        Button ICommentForm.buttonSelectFile
        {
            get { return buttonSelectFile; }
            set { buttonSelectFile = value; }
        }

        RadioButton ICommentForm.radioButtonFilePath
        {
            get { return radioButtonFilePath; }
            set { radioButtonFilePath = value; }
        }

        RadioButton ICommentForm.radioButtonText
        {
            get { return radioButtonText; }
            set { radioButtonText = value; }
        }

        TextBox ICommentForm.textBoxFilePath
        {
            get { return textBoxFilePath; }
            set { textBoxFilePath = value; }
        }

        public event EventHandler<EventArgs> asyncButtonsClick;

        public void formClose(bool flag = false)
        {
            if (!flag)
                if (this.InvokeRequired)
                    this.Invoke(new Action(() => Close()));
                else
                    Close();
            else
            {
                textBoxComment.Enabled = true;
                buttonOk.Enabled = true;
                pictureBoxWait.Visible = false;
            }
        }

        private void buttons_Click(object sender, EventArgs e)
        {
            asyncButtonsClick?.Invoke(sender, e);
        }
    }
}
