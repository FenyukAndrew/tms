﻿using GID;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using TaskApp.DBClasses;
using TaskApp.FormComment;
using TaskApp.FormEventAcknowledge;
using TaskApp.FormGetNewVersion;
using TaskApp.FormReports;
using UpdateMe;

namespace TaskApp
{
    public class TelegramEventArgs : EventArgs
    {
        public bool telegramAnswer;
    }

    public class PMainForm
    {
        public IMainForm _mainForm { get; set; }
        public MMainForm model { get; set; }
        FormAddEditTask.AddEditTaskForm addEditTaskForm = new FormAddEditTask.AddEditTaskForm();
        static Mutex mut = new Mutex();
        static System.Threading.Timer UpdateTaskTimer { get; set; }

        public Dictionary<int, Event> tempOffEvent { get; set; }//временный словарь отключенных событий, после рестарта приложения обнуляется

        /// <summary>
        /// UPDATE
        /// </summary>
        public const string updaterPrefix = "M1234_";
        public const string versionFileName = "version";
        ///

        public PMainForm(MainForm mainForm)
        {
            tempOffEvent = new Dictionary<int, Event>();

            _mainForm = mainForm;
            model = new MMainForm();
            model.updateListViewVirtualItemsSize += Model_updateListViewVirtualItemsSize;
            model.updateStatusLabelComment += Model_updateStatusLabelComment;
            model.reloadTaskFromDataBase += Model_reloadTaskFromDataBase;
            model.enabledListViewCommentAndHistory += Model_enabledListViewCommentAndHistory;
            model.updateStatusLabelEvent += Model_updateStatusLabelEvent;
            model.updateStatusLabelEventError += Model_updateStatusLabelEventError;
            model.startSerialization += Model_startSerialization;

            _mainForm.asyncButtonsClick += _mainForm_asyncButtonsClick;
            _mainForm.formLoad += _mainForm_formLoad;
            _mainForm.formClosing += _mainForm_formClosing;
            _mainForm.listViewRetrieveVirtualItem += _mainForm_listViewRetrieveVirtualItem;
            _mainForm.contextMenuTaskItemsClick += _mainForm_contextMenuTaskItemsClick;
            _mainForm.listViewTaskSelectedIndexChange += _mainForm_listViewTaskSelectedIndexChange;
            _mainForm.listViewCommentRetrieveVirtualItem += _mainForm_listViewCommentRetrieveVirtualItem;
            _mainForm.listViewHistoryRetrieveVirtualItem += _mainForm_listViewHistoryRetrieveVirtualItem;
            _mainForm.listViewTaskColumnClick += _mainForm_listViewTaskColumnClick;
            _mainForm.listViewTaskDrawSubItem += _mainForm_listViewTaskDrawSubItem;
            _mainForm.listViewTaskDoubleClick += _mainForm_listViewTaskDoubleClick;
            _mainForm.listViewCommentDoubleClick += _mainForm_listViewCommentDoubleClick;
            _mainForm.toolStripButtonConnectionSettingsClick += _mainForm_toolStripButtonConnectionSettingsClick;
            _mainForm.checkBoxAutoUpdateCheckedChanged += _mainForm_checkBoxAutoUpdateCheckedChanged;
            _mainForm.menuItemExportCSVClick += _mainForm_menuItemExportCSVClick;
            _mainForm.toolStripButtonReportsClick += _mainForm_toolStripButtonReportsClick;

            _mainForm.listViewCloseSelectedTask += _mainForm_closeSelectedTask;
            _mainForm.listViewExportExcelSelectedTask += _mainForm_exportExcelSelectedTask;
            _mainForm.listViewExportWordSelectedTask += _mainForm_exportWordSelectedTask;
        }

        List<Task> getSelectedTask()
        {
            List<Task> tasks = new List<Task>();
            int[] indexs = _mainForm.ListViewTaskSelectedIndexs;
            foreach (int id in indexs)
            {
                var task = model.TaskSnapShot.ElementAt(id).Value;
                if (task != null)
                {
                    tasks.Add(task);
                }
            }
            return tasks;
        }

        private void _mainForm_exportExcelSelectedTask(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "Задачи " + DateTime.Now.ToString("dd.MM.yy");
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                FormProgress.ProgressForm progressForm = new FormProgress.ProgressForm();
                FormProgress.ExportTask.PExportExcelForm pProgressForm = new FormProgress.ExportTask.PExportExcelForm(progressForm, getSelectedTask() , sfd.FileName + ".xlsx");
                progressForm.ShowDialog();
            }
        }

        private void _mainForm_exportWordSelectedTask(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = "Задачи " + DateTime.Now.ToString("dd.MM.yy");
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                FormProgress.ProgressForm progressForm = new FormProgress.ProgressForm();
                FormProgress.ExportTask.PExportWordForm pProgressForm = new FormProgress.ExportTask.PExportWordForm(progressForm, getSelectedTask(), sfd.FileName + ".docx");
                progressForm.ShowDialog();
            }
        }

        private void _mainForm_closeSelectedTask(object sender, TelegramEventArgs e)
        {
            foreach (var task in getSelectedTask())
            {
                model.closeTask(task, e.telegramAnswer);
            }
        }

        private void _mainForm_toolStripButtonReportsClick(object sender, EventArgs e)
        {
            ReportsForm reportsForm = new ReportsForm();
            PReportsForm pReportsForm = new PReportsForm(reportsForm, this, _mainForm.checkedListBoxStatus, _mainForm.checkedListBoxService, _mainForm.checkedListBoxWorkGroup, _mainForm.checkedListBoxCreator, _mainForm.checkedListBoxExecutant);
            reportsForm.StartPosition = FormStartPosition.CenterScreen;
            reportsForm.ShowDialog();
        }

        private void Model_startSerialization(object sender, EventArgs e)
        {
            if (_mainForm.checkedListBoxStatus.CheckedIndices.Count == 0)
                return;

            SerializableDictionary<int, Status> tempStatus = new SerializableDictionary<int, Status>();

            for(int i = 0; i < _mainForm.checkedListBoxStatus.CheckedIndices.Count; i++)
            {
                if (_mainForm.checkedListBoxStatus.CheckedIndices[i] - 1 < 0)   //если значение отрицательное, значит выбран первый элемент в таблице "ВСЕ", выходим из цыкла, в функцию передаем пустой
                    break;                                                      //список, чтобы в дальнейшем были выбраны все элементы.
                var status = model._Status.ElementAt(_mainForm.checkedListBoxStatus.CheckedIndices[i] - 1);
                tempStatus.Add(status.Key, status.Value);
            }

            SerializableDictionary<int, Service> tempService = new SerializableDictionary<int, Service>();

            for (int i = 0; i < _mainForm.checkedListBoxService.CheckedIndices.Count; i++)
            {
                if (_mainForm.checkedListBoxService.CheckedIndices[i] - 1 < 0)   //если значение отрицательное, значит выбран первый элемент в таблице "ВСЕ", выходим из цыкла, в функцию передаем пустой
                    break;                                                      //список, чтобы в дальнейшем были выбраны все элементы.
                int index = _mainForm.checkedListBoxService.CheckedIndices[i];
                var service = _mainForm.checkedListBoxService.Items[index].ToString() == "" ? new System.Collections.Generic.KeyValuePair<int, Service>() : model._Service.ElementAt(index - 1);
                tempService.Add(service.Key, service.Value);
            }

            SerializableDictionary<int, WorkGroup> tempWorkGroup = new SerializableDictionary<int, WorkGroup>();

            for (int i = 0; i < _mainForm.checkedListBoxWorkGroup.CheckedIndices.Count; i++)
            {
                if (_mainForm.checkedListBoxWorkGroup.CheckedIndices[i] - 1 < 0)   //если значение отрицательное, значит выбран первый элемент в таблице "ВСЕ", выходим из цыкла, в функцию передаем пустой
                    break;                                                      //список, чтобы в дальнейшем были выбраны все элементы.
                int index = _mainForm.checkedListBoxWorkGroup.CheckedIndices[i];
                var wg = _mainForm.checkedListBoxWorkGroup.Items[index].ToString() == "" ? new System.Collections.Generic.KeyValuePair<int, WorkGroup>() : model._WorkGroup.ElementAt(index - 1);
                tempWorkGroup.Add(wg.Key, wg.Value);
            }

            SerializableDictionary<int, User> tempCreatorUsers = new SerializableDictionary<int, User>();

            foreach(CheckListBoxItem itemListBox in _mainForm.checkedListBoxCreator.Items)
            {
                //itemListBox.Tag;
            }

            //Возвращает список выбранных индексов, но проблема - спиосок пользователей содержит удаленных пользователей
            //Почему не сохраняется ссылка на объект, а только его id?
            for (int i = 0; i < _mainForm.checkedListBoxCreator.CheckedIndices.Count; i++)
            {
                if (_mainForm.checkedListBoxCreator.CheckedIndices[i] < 1)   //если выбран первый элемент в таблице "ВСЕ", выходим из цыкла, в функцию передаем пустой
                    break;                                                      //список, чтобы в дальнейшем были выбраны все элементы.
                //var creator = model._User.ElementAt(_mainForm.checkedListBoxCreator.CheckedIndices[i] - 1);
                //tempCreatorUsers.Add(creator.Key, creator.Value);
                CheckListBoxItem itemCreator = _mainForm.checkedListBoxCreator.Items[ _mainForm.checkedListBoxCreator.CheckedIndices[i] ] as CheckListBoxItem;
                int idCreator = (int) itemCreator.Tag;
                var creator = model._User[idCreator];
                tempCreatorUsers.Add(creator.getId(), creator);
            }


            SerializableDictionary<int, User> tempExecutantUsers = new SerializableDictionary<int, User>();

            for (int i = 0; i < _mainForm.checkedListBoxExecutant.CheckedIndices.Count; i++)
            {
                int index = _mainForm.checkedListBoxExecutant.CheckedIndices[i];

                if (index < 1)   //если выбран первый элемент в таблице "ВСЕ", выходим из цыкла, в функцию передаем пустой
                    break;                                                      //список, чтобы в дальнейшем были выбраны все элементы.
                //var executantUser = _mainForm.checkedListBoxExecutant.Items[index].ToString() == "" ? new System.Collections.Generic.KeyValuePair<int, User>() : model._User.ElementAt(index - 1);
                //tempExecutantUsers.Add(executantUser.Key, executantUser.Value);
                if (_mainForm.checkedListBoxExecutant.Items[index].ToString() == "")
                {

                }
                else
                {
                    CheckListBoxItem itemExecutantUser = _mainForm.checkedListBoxExecutant.Items[index] as CheckListBoxItem;
                    int idExecutantUser = (int)itemExecutantUser.Tag;
                    var executantUser = model._User[idExecutantUser];
                    tempExecutantUsers.Add(executantUser.getId(), executantUser);
                }
            }

            model.UpdateSelectedValues(tempStatus, tempService, tempCreatorUsers, tempExecutantUsers, tempWorkGroup);
        }

        private void _mainForm_menuItemExportCSVClick(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            string fileName = lv.Name == "listViewTask" ? "Список задач " + DateTime.Now.ToString("ddMMyyyy HHmm") :
                                lv.Name == "listViewHistory" ? "История изменений к задаче " + lv.Tag : "Комментарии к задаче " + lv.Tag;
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = fileName;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                model.ExportCSV((ListView)sender, sfd.FileName + ".csv");
            }
        }

        private void _mainForm_checkBoxAutoUpdateCheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                AutoUpdatetasks();
        }

        public void AutoUpdatetasks()
        {
            mut.WaitOne();

            if (_mainForm.ComboBoxCritical.InvokeRequired)
            {
                if (_mainForm.CheckBoxAutoupdateChecked)
                    _mainForm.ComboBoxCritical.Invoke(new Action(() => _mainForm.ToolStripButtonUpdate.PerformClick()));
            }
            else
                _mainForm.ToolStripButtonUpdate.PerformClick();

            mut.ReleaseMutex();

            if (this != null)
                if (_mainForm.CheckBoxAutoupdateChecked)
                    UpdateTaskTimer = new System.Threading.Timer(_ => AutoUpdatetasks(), null, 1000 * _mainForm.numericUpDownUpdateIntervalValue, Timeout.Infinite);
        }

        private void Model_enabledListViewCommentAndHistory(object sender, EventArgs e)
        {
            _mainForm.ListViewCommentEnabled = (bool)sender;
            _mainForm.ListViewHistoryEnabled = (bool)sender;
        }

        private void _mainForm_toolStripButtonConnectionSettingsClick(object sender, EventArgs e)
        {
            FormConnectionSettings.ConnectionSettingsForm connectionSettingsForm = new FormConnectionSettings.ConnectionSettingsForm();
            FormConnectionSettings.PConnectionSettingsForm pConnectionSettingsForm = new FormConnectionSettings.PConnectionSettingsForm(connectionSettingsForm, this);
            connectionSettingsForm.StartPosition = FormStartPosition.CenterScreen;
            connectionSettingsForm.ShowDialog();
        }

        private void Model_updateStatusLabelEventError(object sender, EventArgs e)
        {
            _mainForm.UpdateStatusError = (string)sender;
        }

        private void Model_updateStatusLabelEvent(object sender, EventArgs e)
        {
            _mainForm.UpdateStatus = (string)sender;
        }
        
        private void _mainForm_formClosing(object sender, EventArgs e)
        {
            model._Settings.FormX1 = _mainForm.FormX1 < -100 ? 0 : _mainForm.FormX1;
            model._Settings.FormX2 = _mainForm.FormX2 < -100 ? 0 : _mainForm.FormX2;
            model._Settings.FormY1 = _mainForm.FormY1;
            model._Settings.FormY2 = _mainForm.FormY2;
            model._Settings.Splitter1 = _mainForm.SplitContainerSpliter1Distance;
            model._Settings.Splitter2 = _mainForm.SplitContainerSpliter2Distance;
            model._Settings.TaskID = _mainForm.ListViewTaskColumnId;
            model._Settings.TaskDate = _mainForm.ListViewTaskColumnDate;
            model._Settings.TaskTheme = _mainForm.ListViewTaskColumnTheme;
            model._Settings.TaskDescription = _mainForm.ListViewTaskColumnDescription;
            model._Settings.TaskCreator = _mainForm.ListViewTaskColumnCreator;
            model._Settings.TaskExecutant = _mainForm.ListViewTaskColumnExecutant;
            model._Settings.TaskStatus = _mainForm.ListViewTaskColumnStatus;
            model._Settings.TaskService = _mainForm.ListViewTaskColumnService;
            model._Settings.TaskWorkGroup = _mainForm.ListViewTaskColumnWorkGroup;
            model._Settings.CommentDate = _mainForm.ListViewCommentColumnDate;
            model._Settings.CommentDescription = _mainForm.ListViewCommentColumnDescription;
            model._Settings.CommentUser = _mainForm.ListViewCommentColumnUser;
            model._Settings.HistoryDate = _mainForm.ListViewHistoryColumnDate;
            model._Settings.HistoryUser = _mainForm.ListViewHistoryColumnUser;
            model._Settings.HistoryDescription = _mainForm.ListViewHistoryColumnDescription;

            model._Settings.DisplayIndexs = _mainForm.ListViewTaskDisplayIndexsColumns;

            model._Settings.TaskFontSize = _mainForm.ListViewTaskFontSize;
            model._Settings.CommentFontSize = _mainForm.ListViewCommentFontSize;
            model._Settings.HistoryFontSize = _mainForm.ListViewHistoryFontSize;

            model._Settings.AutoUpdateInterval = _mainForm.NumericUpDownUpdateInterval;
            model._Settings.AutoupdateIntervalState = _mainForm.CheckBoxUpdateState;

            model.SerilizationSettings();
        }

        private void Model_endDeserialization(object sender, EventArgs e)
        {
            try
            {
                _mainForm.FormX1 = -8;
                _mainForm.FormY1 = -8;

                if (model._Settings.FormX2 != 0 && model._Settings.FormX2 > -100)
                    _mainForm.FormX2 = model._Settings.FormX2;
                if (model._Settings.FormY2 != 0 && model._Settings.FormY2 > -100)
                    _mainForm.FormY2 = model._Settings.FormY2;
                if (model._Settings.Splitter1 > 0)
                    _mainForm.SplitContainerSpliter1Distance = model._Settings.Splitter1;
                if (model._Settings.Splitter2 > 0)
                    _mainForm.SplitContainerSpliter2Distance = model._Settings.Splitter2;
                if (model._Settings.TaskID != 0)
                    _mainForm.ListViewTaskColumnId = model._Settings.TaskID;
                if (model._Settings.TaskDate != 0)
                    _mainForm.ListViewTaskColumnDate = model._Settings.TaskDate;
                if (model._Settings.TaskTheme != 0)
                    _mainForm.ListViewTaskColumnTheme = model._Settings.TaskTheme;
                if (model._Settings.TaskDescription != 0)
                    _mainForm.ListViewTaskColumnDescription = model._Settings.TaskDescription;
                if (model._Settings.TaskCreator != 0)
                    _mainForm.ListViewTaskColumnCreator = model._Settings.TaskCreator;
                if (model._Settings.TaskExecutant != 0)
                    _mainForm.ListViewTaskColumnExecutant = model._Settings.TaskExecutant;
                if (model._Settings.TaskStatus != 0)
                    _mainForm.ListViewTaskColumnStatus = model._Settings.TaskStatus;
                if (model._Settings.TaskService != 0)
                    _mainForm.ListViewTaskColumnService = model._Settings.TaskService;
                if (model._Settings.TaskWorkGroup != 0)
                    _mainForm.ListViewTaskColumnWorkGroup = model._Settings.TaskWorkGroup;

                if (model._Settings.DisplayIndexs != null)
                {
                    _mainForm.ListViewTaskDisplayIndexsColumns = model._Settings.DisplayIndexs;
                }

                if (model._Settings.CommentDate != 0)
                    _mainForm.ListViewCommentColumnDate = model._Settings.CommentDate;
                if (model._Settings.CommentDescription != 0)
                    _mainForm.ListViewCommentColumnDescription = model._Settings.CommentDescription;
                if (model._Settings.CommentUser != 0)
                    _mainForm.ListViewCommentColumnUser = model._Settings.CommentUser;
                if (model._Settings.HistoryDate != 0)
                    _mainForm.ListViewHistoryColumnDate = model._Settings.HistoryDate;
                if (model._Settings.HistoryUser != 0)
                    _mainForm.ListViewHistoryColumnUser = model._Settings.HistoryUser;
                if (model._Settings.HistoryDescription != 0)
                    _mainForm.ListViewHistoryColumnDescription = model._Settings.HistoryDescription;
                if (model._Settings.TaskFontSize != 0)
                    _mainForm.ListViewTaskFontSize = model._Settings.TaskFontSize;
                if (model._Settings.CommentFontSize != 0)
                    _mainForm.ListViewCommentFontSize = model._Settings.CommentFontSize;
                if (model._Settings.HistoryFontSize != 0)
                    _mainForm.ListViewHistoryFontSize = model._Settings.HistoryFontSize;
                if (model._Settings.AutoUpdateInterval != 0)
                    _mainForm.NumericUpDownUpdateInterval = model._Settings.AutoUpdateInterval;

                Logger.Log.ErrorFormat("String URL={0}", model._Settings.UpdateServerURL);

                //if (model._Settings.UpdateServerURL == null)
                {
                    model._Settings.UpdateServerURL = SELECT_VERSION.server_update;
                }
                _mainForm.CheckBoxUpdateState = model._Settings.AutoupdateIntervalState;

                Logger.Log.ErrorFormat("NEW String URL={0}", model._Settings.UpdateServerURL);

            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "endDeserialization.txt");
            }
        }

        delegate void AttachmentDelegate(Comment comment, string file, string path);
        private void _mainForm_listViewCommentDoubleClick(object sender, EventArgs e)
        {
            var taskId = ((int[])sender)[0];
            var commentId = ((int[])sender)[1];
            var task = model.TaskSnapShot.ElementAt(taskId).Value;
            var comment = task.Comments.ElementAt(commentId);
            if (task != null)
            {
                if (comment.CommentText.Length > 11 && comment.CommentText.Substring(0, 11) == "[Вложение]:")
                {
                    string fileName = comment.CommentText.Substring(11, comment.CommentText.Length - 11);

                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    DialogResult dr = fbd.ShowDialog();

                    if (dr == DialogResult.OK)
                    {
                        _mainForm.ToolStripButtonUpdateEnabled = false;
                        AttachmentDelegate del = new AttachmentDelegate(model.DownloadAttachment);
                        System.Threading.Tasks.Task newTask = System.Threading.Tasks.Task.Factory.StartNew(() => del.BeginInvoke(comment, fileName, fbd.SelectedPath, new AsyncCallback(GetDataFromMySQLCallBackFunc), null));
                    }
                }
                else
                {
                    CommentForm cf = new CommentForm();
                    PCommentForm pcf = new PCommentForm(cf, this, task, comment);
                    cf.StartPosition = FormStartPosition.CenterScreen;
                    cf.ShowDialog();
                }
            }
        }

        private void _mainForm_listViewTaskDoubleClick(object sender, EventArgs e)
        {
            int id = (int)sender;

            var task = model.TaskSnapShot.ElementAt(id).Value;
            if (task != null)
            {
                addEditTaskForm = new FormAddEditTask.AddEditTaskForm();
                FormAddEditTask.PAddEditTaskForm paddEditTaskForm = new FormAddEditTask.PAddEditTaskForm(addEditTaskForm, this, task);
                addEditTaskForm.StartPosition = FormStartPosition.CenterScreen;
                paddEditTaskForm.ShowForm();
            }

        }

        private void _mainForm_listViewTaskDrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            var task = model.TaskSnapShot[e.ItemIndex].Value;
            int eventState = 0;

            foreach (var _event in task.EventList)
            {
                var time = _event.Value.EventTime - DateTime.Now;

                if (e.Item.Selected)
                    e.DrawDefault = true;
                else
                {
                    bool flag = true;
                    if (_event.Value.EventType == 0)
                    {
                        foreach (var v in _event.Value.ExecutantUsers)
                        {
                            if (v.Value.AcknowledgeText == null || v.Value.AcknowledgeText == "")
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        var res = _event.Value.ExecutantUsers.Where(v => v.Value.AcknowledgeText != null && v.Value.AcknowledgeText != "").Count();
                        if (res == 0)
                            flag = false;
                    }

                    if (flag)
                    {
                        if (eventState != 2 && eventState != 3 && eventState != 4)
                            eventState = 1;
                    }
                    else
                    {
                        //предупредительный цвет
                        if (time > TimeSpan.Zero && time < model.BeforeEventTime)
                        {
                            eventState = 2;
                        }
                        else
                        {
                            //цвет просроченного события
                            if (time < TimeSpan.Zero)
                            {
                                eventState = 3;
                                break;
                            }
                            else
                            {
                                if (time > model.BeforeEventTime)
                                {
                                    if (eventState != 2 && eventState != 3)
                                        eventState = 4;
                                }
                            }
                        }
                    }
                }
            }


            ListView lv = (ListView)sender;
            using (StringFormat sf = new StringFormat())
            {
                if (e.Item.Selected)
                {
                    e.DrawDefault = true;
                }
                else
                {
                    try
                    {
                        if (model.TaskSnapShot[e.ItemIndex].Value.Critical)
                        {
                            e.Graphics.FillRectangle(new SolidBrush(Color.LightCoral), e.Bounds);
                            e.Graphics.DrawString(e.SubItem.Text, lv.Font, new SolidBrush(Color.Black), e.Bounds, sf);
                        }
                        else
                        {
                            if (eventState == 1)
                            {
                                e.Graphics.FillRectangle(new SolidBrush(Color.LightGray), e.Bounds);
                                e.Graphics.DrawString(e.SubItem.Text, lv.Font, new SolidBrush(Color.Black), e.Bounds, sf);
                            }
                            else
                            {
                                if (eventState == 2)
                                {
                                    e.Graphics.FillRectangle(new SolidBrush(Color.Yellow), e.Bounds);
                                    e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf);
                                }
                                else
                                {
                                    if (eventState == 3)
                                    {
                                        e.Graphics.FillRectangle(new SolidBrush(Color.Red), e.Bounds);
                                        e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf);

                                    }
                                    else
                                    {
                                        if (eventState == 4)
                                        {
                                            e.Graphics.FillRectangle(new SolidBrush(Color.LightGreen), e.Bounds);
                                            e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf);

                                        }
                                        else
                                        {
                                            e.DrawDefault = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorEventLog.LogError(ex.Message, "listViewTaskDrawSubItem.txt");
                    }
                    return;
                }
            }
        }

        private void _mainForm_listViewTaskColumnClick(object sender, ColumnClickEventArgs e)
        {
            model.Sort(e.Column);
            _mainForm.listViewTaskInvalidate();
        }

        private void _mainForm_listViewHistoryRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
             model.ListViewHistoryRetrieveVirtualItem(_mainForm.ListViewTaskSelectedIndex, e);
        }

        private void Model_updateStatusLabelComment(object sender, EventArgs e)
        {
            _mainForm.UpdateComment = (string)sender;
        }

        private void _mainForm_listViewTaskSelectedIndexChange(object sender, EventArgs e)
        {
            var lv = ((ListView)sender);
            int v = ((ListView)sender).SelectedIndices.Count > 0 ? ((ListView)sender).SelectedIndices[0] : -1;
            if (lv.SelectedIndices.Count > 0)
            {
                _mainForm.ListViewCommentVirtualListSize = model.TaskSnapShot.ElementAt(lv.SelectedIndices[0]).Value.Comments.Count;
                _mainForm.ListViewCommentTag = model.TaskSnapShot.ElementAt(lv.SelectedIndices[0]).Value.Id.ToString();
                _mainForm.ListViewHistoryVirtualListSize = model.TaskSnapShot.ElementAt(lv.SelectedIndices[0]).Value._History.Count;
                _mainForm.ListViewHistoryTag = model.TaskSnapShot.ElementAt(lv.SelectedIndices[0]).Value.Id.ToString();
            }
            else
            {
                _mainForm.ListViewCommentVirtualListSize = 0;
                _mainForm.ListViewHistoryVirtualListSize = 0;
            }
        }

        private void _mainForm_listViewCommentRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            model.ListViewCommentRetrieveVirtualItem(_mainForm.ListViewTaskSelectedIndex, e);
        }

        private void Model_reloadTaskFromDataBase(object sender, EventArgs e)
        {
            _mainForm_asyncButtonsClick(null, EventArgs.Empty);
           // addEditTaskForm.CloseForm();
        }

        private void Model_updateListViewVirtualItemsSize(object sender, EventArgs e)
        {
            if (model.TaskSnapShot == null)
            {
               // _mainForm.ListViewTaskVirtualListSize = 0;
                _mainForm.ListViewCommentVirtualListSize = 0;
                _mainForm.ListViewHistoryVirtualListSize = 0;
            }
            else
            {
                _mainForm.ListViewTaskVirtualListSize = model.TaskSnapShot.Length;
            }
        }

        private void _mainForm_contextMenuTaskItemsClick(object sender, EventArgs e)
        {
            if (sender.ToString() == "Создать задачу")
            {
                addEditTaskForm = new FormAddEditTask.AddEditTaskForm();
                FormAddEditTask.PAddEditTaskForm paddEditTaskForm = new FormAddEditTask.PAddEditTaskForm(addEditTaskForm, this);
                addEditTaskForm.StartPosition = FormStartPosition.CenterScreen;
                paddEditTaskForm.ShowForm();
            }
            else
            {
                if (sender.ToString() == "Изменить задачу")
                {
                    int id = _mainForm.ListViewTaskSelectedIndex;
                    {
                        var task = model.TaskSnapShot.ElementAt(id).Value;
                        if (task != null)
                        {
                            addEditTaskForm = new FormAddEditTask.AddEditTaskForm();
                            FormAddEditTask.PAddEditTaskForm paddEditTaskForm = new FormAddEditTask.PAddEditTaskForm(addEditTaskForm, this, task);
                            addEditTaskForm.StartPosition = FormStartPosition.CenterScreen;
                            paddEditTaskForm.ShowForm();
                        }
                    }
                }
                else
                {
                    if (sender.ToString() == "Добавить комментарий")
                    {
                        int id = _mainForm.ListViewTaskSelectedIndex;
                        {
                            var task = model.TaskSnapShot.ElementAt(id).Value;
                            if (task != null)
                            {
                                CommentForm cf = new CommentForm();
                                PCommentForm pcf = new PCommentForm(cf, this, task);
                                cf.StartPosition = FormStartPosition.CenterScreen;
                                cf.ShowDialog();
                            }
                        }
                    }
                    else
                    {
                        if (sender.ToString() == "События")
                        {
                            int id = _mainForm.ListViewTaskSelectedIndex;
                            {
                                var task = model.TaskSnapShot.ElementAt(id).Value;
                                if (task != null)
                                {
                                    FormAddEvent.AddEventForm addEventForm = new FormAddEvent.AddEventForm();
                                    FormAddEvent.PAddEventForm pAddEventForm = new FormAddEvent.PAddEventForm(addEventForm, this, task);
                                    addEventForm.StartPosition = FormStartPosition.CenterScreen;
                                    addEventForm.ShowDialog();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void _mainForm_listViewRetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            model.ListViewRetrieveVirtualItem(sender, e);
        }

        delegate void UpdateDelegate(string url, string fileName);
        private void _mainForm_formLoad(object sender, EventArgs e)
        {
            _mainForm.FormText = "Task Manager v." + model.GetProgramVersion() + SELECT_VERSION.AdditionalTitle;
            _mainForm.UpdateComment = "";
            _mainForm.ComboBoxCritical.SelectedIndex = 0;

            model.DeserializationSettings();
            Model_endDeserialization(sender, e);

            update.updateMe(updaterPrefix, Application.StartupPath + @"\");
            unpackCommandline();

            UpdateDelegate updateDel = new UpdateDelegate(model.CheckNewVersion);
            System.Threading.Tasks.Task updTask = System.Threading.Tasks.Task.Factory.StartNew(() => updateDel.BeginInvoke(model._Settings.UpdateServerURL, versionFileName, new AsyncCallback(CheckNewVersionCallBackFunc), null));
        }

        private void CheckNewVersionCallBackFunc(IAsyncResult aRes)
        {
            if (model.Version == null)
            {
                //ERROR
                _mainForm.CheckUpdateProgramStatus = "Обновления недоступны";
            }
            else
            {
                try
                {
                    //if (int.Parse(model.Version[1].Replace(".", "")) > int.Parse(model.GetProgramVersion().Replace(".", "")))
                    var version_new = new Version(model.Version[1]);
                    var version_old = new Version(model.GetProgramVersion());
                    if (version_new.CompareTo(version_old)>0)
                    {
                        _mainForm.CheckUpdateProgramStatus = "Доступна новая версия";
                        //NEED UPDATE!
                        GetNewVersionForm gNVF = new GetNewVersionForm();
                        PGetNewVersionForm pGetNewVersionForm = new PGetNewVersionForm(gNVF, this);
                        pGetNewVersionForm.CloseApplication += PGetNewVersionForm_CloseApplication;
                        gNVF.ShowDialog();
                    }
                    else
                    {
                        //DO NOTHINK
                        _mainForm.CheckUpdateProgramStatus = "Обновление не требуется";
                    }
                }
                catch(Exception ex)
                {
                    _mainForm.CheckUpdateProgramStatusError = "Новая версия программы не корректна. Обратитесь в СЦ ФК. " + ex.Message;
                }
            }
        }

        private void PGetNewVersionForm_CloseApplication(object sender, EventArgs e)
        {
            _mainForm.CloseForm();
        }

        delegate void Delegate(int index);
        public void _mainForm_asyncButtonsClick(object sender, EventArgs e)
        {
            if (_mainForm.ToolStripButtonUpdateEnabled == false)
                return;
            if (_mainForm.ToolStripButtonUpdateEnabled)
            {
                if (_mainForm.ComboBoxCritical.InvokeRequired)
                    _mainForm.ComboBoxCritical.Invoke(new Action(()=> _mainForm.ToolStripButtonUpdateEnabled = false));
                else
                    _mainForm.ToolStripButtonUpdateEnabled = false;

                int listViewTaskSelIndex = _mainForm.ListViewTaskSelectedIndex;
                Delegate del = new Delegate(GetDataFromMySQL);
                System.Threading.Tasks.Task newTask = System.Threading.Tasks.Task.Factory.StartNew(() => del.BeginInvoke(listViewTaskSelIndex, new AsyncCallback(GetDataFromMySQLCallBackFunc), null));
            }
            else
            {
                _mainForm.ToolStripButtonUpdateEnabled = true;
            }
        }

        private void GetDataFromMySQLCallBackFunc(IAsyncResult aRes)
        {
            _mainForm.ToolStripButtonUpdateEnabled = true;
        }

        private void ShowAlarmWindow(Task alarmTask, Event alarmEvent)
        {
            bool flag = true;
            _mainForm.mainForm.Invoke(new Action(() =>
            {
                foreach (var form in _mainForm.mainForm.OwnedForms)
                {
                    if (form.Text == "Задача: " + alarmTask.Id + " Событие: " + alarmEvent.EventID)
                    {
                        flag = false;
                        break;
                    }
                }

                if (flag)
                {
                    if (alarmEvent.ExecutantUsers.Where(v => v.Key.ToString() == model.CurrentUserID).ToArray().Length > 0)
                    {
                        if (!tempOffEvent.ContainsKey(alarmEvent.EventID))
                        {
                            EventAcknowledgeForm eAF = new EventAcknowledgeForm();
                            PEventAcknowledgeForm pEAF = new PEventAcknowledgeForm(eAF, this, alarmTask, alarmEvent, "Задача: " + alarmTask.Id + " Событие: " + alarmEvent.EventID, true);
                            eAF.TopMost = true;
                            eAF.StartPosition = FormStartPosition.CenterScreen;
                            eAF.ShowDialog(_mainForm.mainForm);
                        }
                    }
                }
            }));
        }

        private void GetDataFromMySQL(int listViewTaskSelIndex)
        {
            if (_mainForm.DateTimePickerEndDate.InvokeRequired)
                _mainForm.DateTimePickerEndDate.Invoke(new Action(()=> _mainForm.DateTimePickerEndDate.Value = DateTime.Now));
            else
                _mainForm.DateTimePickerEndDate.Value = DateTime.Now;

            string CommandTextSource = "SELECT * FROM source";
            string CommandTextService = "SELECT * FROM service ORDER BY Name";
            string CommandTextWorkGroup = "SELECT * FROM workgroup";
            string CommandTextUser = "SELECT * FROM user ORDER BY fullName";
            string CommandTextStatus = "SELECT * FROM status ORDER BY id";
            string CommandTextTask = _mainForm.TextBoxId.Length > 0 ? "SELECT * FROM task WHERE taskId = " + _mainForm.TextBoxId : "SELECT * FROM task WHERE CreationDate BETWEEN STR_TO_DATE('" + _mainForm.DateTimePickerStartDate.Value.ToString("yyyy-MM-dd") + " " +
                _mainForm.DateTimePickerStartTime.Value.ToString("HH:mm:ss") + "', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('" +
                _mainForm.DateTimePickerEndDate.Value.ToString("yyyy-MM-dd") + " " +
                _mainForm.DateTimePickerEndTime.Value.ToString("HH:mm:ss") + "', '%Y-%m-%d %H:%i:%s')";

            int selectedIndex = -1;
            if (_mainForm.ComboBoxCritical.InvokeRequired)
                _mainForm.ComboBoxCritical.Invoke(new Action(() => selectedIndex = _mainForm.ComboBoxCritical.SelectedIndex));
            else
                selectedIndex = _mainForm.ComboBoxCritical.SelectedIndex;
            if (selectedIndex > 0)
            {
                CommandTextTask += " AND Critical=" + (selectedIndex - 1).ToString();
            }

            MySqlConnection conn = new MySqlConnection(model.ConnectionString);
           // using (MySqlConnection conn = new MySqlConnection(model.ConnectionString))
            {
                try
                {
                    _mainForm.UpdateComment = "Подключение к БД";
                    conn.Open();
                    MySqlCommand myCommand;
                    if (model._User.Count == 0)
                    {
                        myCommand = new MySqlCommand(CommandTextUser, conn);
                        _mainForm.UpdateComment = "Загрузка списка пользователей...";
                        var res2 = model.getUsers(conn, myCommand);
                        if (model._User.Count > 0)
                        {
                            model.UpdateCurrentUser();

                            //Фильтрация удаленных пользователей
                            //Dictionary<int, User> list = (from t in model._User where (!t.Value.Delete) select t.Value).ToDictionary(t => t.getId());
                            //Dictionary<int, User> _realUser = model._User.Where(t => (!t.Value.Delete)).Select(t => t.Value).ToDictionary(t => t.getId());
                            model.updateCheckedListBoxItems(_mainForm.checkedListBoxCreator, model._RealUser, model.SelectedCreatorUser);

                            //Dictionary<int, User> _realExecutantUser = model._ExecutantUser.Where(t => (!t.Value.Delete)).Select(t => t.Value).ToDictionary(t => t.getId());
                            model.updateCheckedListBoxItems(_mainForm.checkedListBoxExecutant, model._RealExecutantUser, model.SelectedExecutantUser, 1);

                            if (_mainForm.checkedListBoxExecutant.InvokeRequired)
                                _mainForm.checkedListBoxExecutant.Invoke(new Action(()=> _mainForm.checkedListBoxExecutant.Items.Add(new CheckListBoxItem(_mainForm.checkedListBoxExecutant.Items.Count, ""), true)));
                            else
                                _mainForm.checkedListBoxExecutant.Items.Add(new CheckListBoxItem(_mainForm.checkedListBoxExecutant.Items.Count, ""), true);
                        }
                    }

                    if (model._Status.Count == 0)
                    {
                        _mainForm.UpdateComment = "Загрузка статусов...";
                        myCommand = new MySqlCommand(CommandTextStatus, conn);
                        var res2 = model.getStatuses(conn, myCommand);

                        if (model._Status.Count > 0)
                        {
                            model.updateCheckedListBoxItems(_mainForm.checkedListBoxStatus, model._Status, model.SelectedStatus);
                        }
                    }

                    if (model._Source.Count == 0)
                    {
                        _mainForm.UpdateComment = "Загрузка источников...";
                        myCommand = new MySqlCommand(CommandTextSource, conn);
                        var res2 = model.getSource(conn, myCommand);
                    }

                    if (model._Service.Count == 0)
                    {
                        _mainForm.UpdateComment = "Загрузка сервисов...";
                        myCommand = new MySqlCommand(CommandTextService, conn);
                        var res2 = model.getServices(conn, myCommand);
                        
                        if (model._Service.Count > 0)
                        {
                            model.updateCheckedListBoxItems(_mainForm.checkedListBoxService, model._Service, model.SelectedService, 1);
                            bool flagForEmptyService = false;
                            if (model.SelectedService.Contains(new System.Collections.Generic.KeyValuePair<int, Service>()) | model.SelectedService.Count == 0)
                            {
                                flagForEmptyService = true;
                            }
                            if (_mainForm.checkedListBoxService.InvokeRequired)
                            {
                                _mainForm.checkedListBoxService.Invoke(new Action(() => _mainForm.checkedListBoxService.Items.Add(new CheckListBoxItem(_mainForm.checkedListBoxService.Items.Count, ""), flagForEmptyService)));
                            }
                            else
                            {
                                _mainForm.checkedListBoxService.Items.Add(new CheckListBoxItem(_mainForm.checkedListBoxService.Items.Count, ""), flagForEmptyService);
                            }
                        }
                    }

                    if (model._WorkGroup.Count == 0)
                    {
                        _mainForm.UpdateComment = "Загрузка рабочих групп...";
                        myCommand = new MySqlCommand(CommandTextWorkGroup, conn);
                        var res2 = model.getWorkGroup(conn, myCommand);
                        
                        if (model._WorkGroup.Count > 0)
                        {
                            model.updateCheckedListBoxItems(_mainForm.checkedListBoxWorkGroup, model._WorkGroup, model.SelectedWorkGroup, 1);
                            bool flagForEmptyWG = false;
                            if (model.SelectedWorkGroup.Contains(new System.Collections.Generic.KeyValuePair<int, WorkGroup>()) | model.SelectedWorkGroup.Count == 0)
                                flagForEmptyWG = true;
                            if (_mainForm.checkedListBoxWorkGroup.InvokeRequired)
                                _mainForm.checkedListBoxWorkGroup.Invoke(new Action(() => _mainForm.checkedListBoxWorkGroup.Items.Add(new CheckListBoxItem(_mainForm.checkedListBoxWorkGroup.Items.Count, ""), flagForEmptyWG)));
                            else
                                _mainForm.checkedListBoxWorkGroup.Items.Add(new CheckListBoxItem(_mainForm.checkedListBoxWorkGroup.Items.Count, ""), flagForEmptyWG);
                        }
                    }

                    if (_mainForm.checkedListBoxStatus.Items.Count > 0)
                    {
                        if (!_mainForm.checkedListBoxStatus.GetItemChecked(0))
                        {
                            if (_mainForm.checkedListBoxStatus.CheckedItems.Count > 0)
                            {
                                CommandTextTask += " AND (";
                                for (int i = 0; i < _mainForm.checkedListBoxStatus.CheckedItems.Count; i++)
                                {
                                    CommandTextTask += "Status=" + ((CheckListBoxItem)_mainForm.checkedListBoxStatus.CheckedItems[i]).Tag.ToString() + " OR ";
                                }
                                int length = CommandTextTask.Length;
                                if (length - 4 > 0)
                                    CommandTextTask = CommandTextTask.Substring(0, CommandTextTask.Length - 4);
                                CommandTextTask += ")";
                            }
                        }
                    }



                    if (_mainForm.checkedListBoxCreator.Items.Count > 0)
                    {
                        if (!_mainForm.checkedListBoxCreator.GetItemChecked(0))
                        {
                            if (_mainForm.checkedListBoxCreator.CheckedItems.Count > 0)
                            {
                                CommandTextTask += " AND (";
                                for (int i = 0; i < _mainForm.checkedListBoxCreator.CheckedItems.Count; i++)
                                {
                                    CommandTextTask += "CreatorUserID=" + ((CheckListBoxItem)_mainForm.checkedListBoxCreator.CheckedItems[i]).Tag.ToString() + " OR ";
                                }
                                int length = CommandTextTask.Length;
                                if (length - 4 > 0)
                                    CommandTextTask = CommandTextTask.Substring(0, CommandTextTask.Length - 4);
                                CommandTextTask += ")";
                            }
                        }
                    }

                    if (_mainForm.checkedListBoxExecutant.Items.Count > 0)
                    {
                        if (!_mainForm.checkedListBoxExecutant.GetItemChecked(0))
                        {
                            if (_mainForm.checkedListBoxExecutant.CheckedItems.Count > 0)
                            {
                                CommandTextTask += " AND (";
                                for (int i = 0; i < _mainForm.checkedListBoxExecutant.CheckedItems.Count; i++)
                                {
                                    if (((CheckListBoxItem)_mainForm.checkedListBoxExecutant.CheckedItems[i]).Text.ToString() == "")
                                        CommandTextTask += "ExecutantUserID is null OR ";
                                    else
                                        CommandTextTask += "ExecutantUserID=" + ((CheckListBoxItem)_mainForm.checkedListBoxExecutant.CheckedItems[i]).Tag.ToString() + " OR ";
                                }
                                int length = CommandTextTask.Length;
                                if (length - 4 > 0)
                                    CommandTextTask = CommandTextTask.Substring(0, CommandTextTask.Length - 4);
                                CommandTextTask += ")";
                            }
                        }
                    }


                    if (_mainForm.checkedListBoxService.Items.Count > 0)
                    {
                        if (_mainForm.checkedListBoxService.CheckedItems.Count > 0)
                        {
                            CommandTextTask += " AND (";
                            for (int i = 0; i < _mainForm.checkedListBoxService.CheckedItems.Count; i++)
                            {
                                if (((CheckListBoxItem)_mainForm.checkedListBoxService.CheckedItems[i]).Text.ToString() == "")
                                    CommandTextTask += "Service is null OR ";
                                else
                                    CommandTextTask += "Service=" + ((CheckListBoxItem)_mainForm.checkedListBoxService.CheckedItems[i]).Tag.ToString() + " OR ";
                            }
                            int length = CommandTextTask.Length;
                            if (length - 4 > 0)
                                CommandTextTask = CommandTextTask.Substring(0, CommandTextTask.Length - 4);
                            CommandTextTask += ")";
                        }
                    }

                    if (_mainForm.checkedListBoxWorkGroup.Items.Count > 0)
                    {
                        if (_mainForm.checkedListBoxWorkGroup.CheckedItems.Count > 0)
                        {
                            CommandTextTask += " AND (";
                            for (int i = 0; i < _mainForm.checkedListBoxWorkGroup.CheckedItems.Count; i++)
                            {
                                if (((CheckListBoxItem)_mainForm.checkedListBoxWorkGroup.CheckedItems[i]).Text.ToString() == "")
                                    CommandTextTask += "Workgroup is null OR ";
                                else
                                    CommandTextTask += "Workgroup=" + ((CheckListBoxItem)_mainForm.checkedListBoxWorkGroup.CheckedItems[i]).Tag.ToString() + " OR ";
                            }
                            int length = CommandTextTask.Length;
                            if (length - 4 > 0)
                                CommandTextTask = CommandTextTask.Substring(0, CommandTextTask.Length - 4);
                            CommandTextTask += ")";
                        }
                    }

                    _mainForm.UpdateComment = "Загрузка списка задач с активными событиями...";
                    string tasksId = model.GetTasksIdWithNotAcknowledgeEvents(conn);

                    if (tasksId.Length > 0)
                        CommandTextTask += " OR (" + tasksId + ")";

                    _mainForm.UpdateComment = "Загрузка списка задач...";
                    myCommand = new MySqlCommand(CommandTextTask, conn);
                  
                    var res = model.getTasks(conn, myCommand);

                    List<Event> tempEvent = new List<Event>();
                    foreach(var ts in model.TaskSnapShot)
                    {
                        foreach(var ev in ts.Value.EventList)
                        {
                            if (ev.Value.EventTime < DateTime.Now)
                            {
                                if (ev.Value.EventType == 0)
                                {
                                    if (ev.Value.ExecutantUsers.Where(v=>v.Value.AcknowledgeText == null || v.Value.AcknowledgeText == "").Count() > 0)
                                    {
                                        tempEvent.Add(ev.Value);
                                    }
                                }
                                else
                                {
                                    if (ev.Value.ExecutantUsers.Where(v => v.Value.AcknowledgeText != null && v.Value.AcknowledgeText != "").Count() == 0)
                                    {
                                        tempEvent.Add(ev.Value);
                                    }
                                }
                            }
                        }
                    }

               //     var alarmEvents = model.TaskSnapShot.Where(v => v.Value.EventList.Where(v2 => v2.Value.EventTime < DateTime.Now).Count() > 0).SelectMany(v => v.Value.EventList).
               //        Where(v2 => v2.Value.EventType == 0 ? v2.Value.ExecutantUsers.Where(v3 => v3.Value.AcknowledgeText == null || v3.Value.AcknowledgeText == "").Count() > 0 :
               //        v2.Value.ExecutantUsers.Where(v3 => v3.Value.AcknowledgeText != null && v3.Value.AcknowledgeText != "").Count() == 0);

                    foreach (var v in tempEvent)
                    {
                        ShowAlarmWindow(v._Task, v);
                    }

                    _mainForm.listViewCommentInvalidate();
                    _mainForm.listViewHistoryInvalidate();
                    _mainForm.listViewTaskInvalidate();
                   
                    if (model.TaskSnapShot.Length > listViewTaskSelIndex && listViewTaskSelIndex != -1)
                    {
                        _mainForm.ListViewTaskSelectedIndex = listViewTaskSelIndex;
                    }

                    _mainForm.UpdateComment = "Всего задач: " + model.TaskSnapShot.Length;
                }
                catch (Exception ex)
                {
                    _mainForm.UpdateCommentError = "Ошибка: " + ex.Source + " " + ex.Message;
                    ErrorEventLog.LogError(ex.Message, "GetDataFromMySQL.txt");
                }
            }
        }

        private void unpackCommandline()
        {

            bool commandPresent = false;
            string tempStr = "";

            foreach (string arg in Environment.GetCommandLineArgs())
            {

                if (!commandPresent)
                {

                    commandPresent = arg.Trim().StartsWith("/");

                }

                if (commandPresent)
                {
                    tempStr += arg;
                }
            }


            if (commandPresent)
            {
                if (tempStr.Remove(0, 2) == "updated")
                {
                    MessageBox.Show("Программа была обновлена.");
                }
            }
        }


    }


}
