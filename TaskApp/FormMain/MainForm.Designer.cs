﻿namespace TaskApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusEvent = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelProgramVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelUpdateStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonUpdate = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonConnectionSettings = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonReports = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.listViewTask = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStripTask = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.создатьЗадачуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.изменитьЗадачуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.добавитьКомментарийToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.экспортCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.событияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelSettings = new System.Windows.Forms.Panel();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.numericUpDownHistory = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownTasks = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDownComments = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageStatus = new System.Windows.Forms.TabPage();
            this.checkedListBoxStatus = new System.Windows.Forms.CheckedListBox();
            this.tabPageService = new System.Windows.Forms.TabPage();
            this.checkedListBoxService = new System.Windows.Forms.CheckedListBox();
            this.tabPageWorkGroup = new System.Windows.Forms.TabPage();
            this.checkedListBoxWorkGroup = new System.Windows.Forms.CheckedListBox();
            this.tabPageCreator = new System.Windows.Forms.TabPage();
            this.checkedListBoxCreator = new System.Windows.Forms.CheckedListBox();
            this.tabPageExecutant = new System.Windows.Forms.TabPage();
            this.checkedListBoxExecutant = new System.Windows.Forms.CheckedListBox();
            this.checkBoxAutoUpdate = new System.Windows.Forms.CheckBox();
            this.numericUpDownUpdateInterval = new System.Windows.Forms.NumericUpDown();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dateTimePickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerEndTime = new System.Windows.Forms.DateTimePicker();
            this.comboBoxCritical = new System.Windows.Forms.ComboBox();
            this.dateTimePickerStartTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dateTimePickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.listViewComments = new System.Windows.Forms.ListView();
            this.columnDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnText = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStripCommentsAndHistory = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.экспортВCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listViewHistory = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStripMultiTask = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.закрытьЗадачиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ЭкспортExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ЭкспортWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.contextMenuStripTask.SuspendLayout();
            this.panelSettings.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownComments)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPageStatus.SuspendLayout();
            this.tabPageService.SuspendLayout();
            this.tabPageWorkGroup.SuspendLayout();
            this.tabPageCreator.SuspendLayout();
            this.tabPageExecutant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUpdateInterval)).BeginInit();
            this.contextMenuStripCommentsAndHistory.SuspendLayout();
            this.contextMenuStripMultiTask.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatus,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel1,
            this.toolStripStatusEvent,
            this.toolStripStatusLabel3,
            this.toolStripStatusLabelProgramVersion,
            this.toolStripStatusLabelUpdateStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 553);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1285, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.ForeColor = System.Drawing.Color.Green;
            this.toolStripStatus.LinkColor = System.Drawing.Color.Black;
            this.toolStripStatus.Name = "toolStripStatus";
            this.toolStripStatus.Size = new System.Drawing.Size(84, 17);
            this.toolStripStatus.Text = "toolStripStatus";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(16, 17);
            this.toolStripStatusLabel2.Text = "   ";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusEvent
            // 
            this.toolStripStatusEvent.Name = "toolStripStatusEvent";
            this.toolStripStatusEvent.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(1052, 17);
            this.toolStripStatusLabel3.Spring = true;
            this.toolStripStatusLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // toolStripStatusLabelProgramVersion
            // 
            this.toolStripStatusLabelProgramVersion.ForeColor = System.Drawing.SystemColors.Highlight;
            this.toolStripStatusLabelProgramVersion.Name = "toolStripStatusLabelProgramVersion";
            this.toolStripStatusLabelProgramVersion.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabelUpdateStatus
            // 
            this.toolStripStatusLabelUpdateStatus.Name = "toolStripStatusLabelUpdateStatus";
            this.toolStripStatusLabelUpdateStatus.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabelUpdateStatus.Text = "toolStripStatusLabel4";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonUpdate,
            this.toolStripSeparator1,
            this.toolStripButtonConnectionSettings,
            this.toolStripButton1,
            this.toolStripSeparator2,
            this.toolStripButtonReports});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1285, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonUpdate
            // 
            this.toolStripButtonUpdate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonUpdate.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonUpdate.Image")));
            this.toolStripButtonUpdate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonUpdate.Name = "toolStripButtonUpdate";
            this.toolStripButtonUpdate.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonUpdate.Text = "toolStripButton1";
            this.toolStripButtonUpdate.ToolTipText = "Обновить";
            this.toolStripButtonUpdate.Click += new System.EventHandler(this.toolStripButtonUpdate_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonConnectionSettings
            // 
            this.toolStripButtonConnectionSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonConnectionSettings.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonConnectionSettings.Image")));
            this.toolStripButtonConnectionSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonConnectionSettings.Name = "toolStripButtonConnectionSettings";
            this.toolStripButtonConnectionSettings.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonConnectionSettings.Text = "toolStripButton1";
            this.toolStripButtonConnectionSettings.ToolTipText = "Настройка подключения";
            this.toolStripButtonConnectionSettings.Click += new System.EventHandler(this.toolStripButtonConnectionSettings_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "Параметры запроса";
            this.toolStripButton1.Click += new System.EventHandler(this.GroupBoxSettings_DoubleClick);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonReports
            // 
            this.toolStripButtonReports.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonReports.Enabled = false;
            this.toolStripButtonReports.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonReports.Image")));
            this.toolStripButtonReports.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReports.Name = "toolStripButtonReports";
            this.toolStripButtonReports.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonReports.Text = "Отчеты";
            this.toolStripButtonReports.ToolTipText = "Отчеты";
            this.toolStripButtonReports.Click += new System.EventHandler(this.toolStripButtonReports_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listViewHistory);
            this.splitContainer1.Size = new System.Drawing.Size(1285, 528);
            this.splitContainer1.SplitterDistance = 413;
            this.splitContainer1.TabIndex = 4;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.listViewTask);
            this.splitContainer2.Panel1.Controls.Add(this.panelSettings);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.listViewComments);
            this.splitContainer2.Size = new System.Drawing.Size(1285, 413);
            this.splitContainer2.SplitterDistance = 978;
            this.splitContainer2.TabIndex = 6;
            // 
            // listViewTask
            // 
            this.listViewTask.AllowColumnReorder = true;
            this.listViewTask.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader7,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader8,
            this.columnHeader12,
            this.columnHeader13});
            this.listViewTask.ContextMenuStrip = this.contextMenuStripTask;
            this.listViewTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewTask.FullRowSelect = true;
            this.listViewTask.GridLines = true;
            this.listViewTask.Location = new System.Drawing.Point(399, 0);
            this.listViewTask.Name = "listViewTask";
            this.listViewTask.OwnerDraw = true;
            this.listViewTask.Size = new System.Drawing.Size(579, 413);
            this.listViewTask.TabIndex = 7;
            this.listViewTask.UseCompatibleStateImageBehavior = false;
            this.listViewTask.View = System.Windows.Forms.View.Details;
            this.listViewTask.VirtualMode = true;
            this.listViewTask.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewTask_ColumnClick);
            this.listViewTask.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewTask_DrawColumnHeader);
            this.listViewTask.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listViewTask_DrawSubItem);
            this.listViewTask.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.listViewData_RetrieveVirtualItem);
            this.listViewTask.SelectedIndexChanged += new System.EventHandler(this.listViewData_SelectedIndexChanged);
            this.listViewTask.DoubleClick += new System.EventHandler(this.listViewTask_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 72;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Дата";
            this.columnHeader2.Width = 80;
            // 
            // columnHeader7
            // 
            this.columnHeader7.DisplayIndex = 3;
            this.columnHeader7.Text = "Тема";
            this.columnHeader7.Width = 104;
            // 
            // columnHeader3
            // 
            this.columnHeader3.DisplayIndex = 4;
            this.columnHeader3.Text = "Описание";
            this.columnHeader3.Width = 108;
            // 
            // columnHeader4
            // 
            this.columnHeader4.DisplayIndex = 5;
            this.columnHeader4.Text = "Инициатор";
            this.columnHeader4.Width = 98;
            // 
            // columnHeader5
            // 
            this.columnHeader5.DisplayIndex = 6;
            this.columnHeader5.Text = "Исполнитель";
            this.columnHeader5.Width = 71;
            // 
            // columnHeader6
            // 
            this.columnHeader6.DisplayIndex = 7;
            this.columnHeader6.Text = "Статус";
            this.columnHeader6.Width = 73;
            // 
            // columnHeader8
            // 
            this.columnHeader8.DisplayIndex = 2;
            this.columnHeader8.Text = "Сервис";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "РГ";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "События";
            this.columnHeader13.Width = 28;
            // 
            // contextMenuStripTask
            // 
            this.contextMenuStripTask.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьЗадачуToolStripMenuItem,
            this.toolStripMenuItem1,
            this.изменитьЗадачуToolStripMenuItem,
            this.добавитьКомментарийToolStripMenuItem,
            this.toolStripMenuItem2,
            this.экспортCSVToolStripMenuItem,
            this.событияToolStripMenuItem});
            this.contextMenuStripTask.Name = "contextMenuStripTask";
            this.contextMenuStripTask.Size = new System.Drawing.Size(206, 126);
            this.contextMenuStripTask.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripTask_Opening);
            // 
            // создатьЗадачуToolStripMenuItem
            // 
            this.создатьЗадачуToolStripMenuItem.Name = "создатьЗадачуToolStripMenuItem";
            this.создатьЗадачуToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.создатьЗадачуToolStripMenuItem.Text = "Создать задачу";
            this.создатьЗадачуToolStripMenuItem.Click += new System.EventHandler(this.ContextMenuStripTaskItemsClick);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(202, 6);
            // 
            // изменитьЗадачуToolStripMenuItem
            // 
            this.изменитьЗадачуToolStripMenuItem.Name = "изменитьЗадачуToolStripMenuItem";
            this.изменитьЗадачуToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.изменитьЗадачуToolStripMenuItem.Text = "Изменить задачу";
            this.изменитьЗадачуToolStripMenuItem.Click += new System.EventHandler(this.ContextMenuStripTaskItemsClick);
            // 
            // добавитьКомментарийToolStripMenuItem
            // 
            this.добавитьКомментарийToolStripMenuItem.Name = "добавитьКомментарийToolStripMenuItem";
            this.добавитьКомментарийToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.добавитьКомментарийToolStripMenuItem.Text = "Добавить комментарий";
            this.добавитьКомментарийToolStripMenuItem.Click += new System.EventHandler(this.ContextMenuStripTaskItemsClick);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(202, 6);
            // 
            // экспортCSVToolStripMenuItem
            // 
            this.экспортCSVToolStripMenuItem.Name = "экспортCSVToolStripMenuItem";
            this.экспортCSVToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.экспортCSVToolStripMenuItem.Text = "Экспорт CSV";
            this.экспортCSVToolStripMenuItem.Click += new System.EventHandler(this.экспортВCSVToolStripMenuItem_Click);
            // 
            // событияToolStripMenuItem
            // 
            this.событияToolStripMenuItem.Name = "событияToolStripMenuItem";
            this.событияToolStripMenuItem.Size = new System.Drawing.Size(205, 22);
            this.событияToolStripMenuItem.Text = "События";
            this.событияToolStripMenuItem.Click += new System.EventHandler(this.ContextMenuStripTaskItemsClick);
            // 
            // panelSettings
            // 
            this.panelSettings.Controls.Add(this.groupBoxSettings);
            this.panelSettings.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSettings.Location = new System.Drawing.Point(0, 0);
            this.panelSettings.Name = "panelSettings";
            this.panelSettings.Size = new System.Drawing.Size(399, 413);
            this.panelSettings.TabIndex = 6;
            this.panelSettings.DoubleClick += new System.EventHandler(this.panel1_DoubleClick);
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.numericUpDownHistory);
            this.groupBoxSettings.Controls.Add(this.label6);
            this.groupBoxSettings.Controls.Add(this.numericUpDownTasks);
            this.groupBoxSettings.Controls.Add(this.label5);
            this.groupBoxSettings.Controls.Add(this.numericUpDownComments);
            this.groupBoxSettings.Controls.Add(this.label3);
            this.groupBoxSettings.Controls.Add(this.tabControl1);
            this.groupBoxSettings.Controls.Add(this.checkBoxAutoUpdate);
            this.groupBoxSettings.Controls.Add(this.numericUpDownUpdateInterval);
            this.groupBoxSettings.Controls.Add(this.textBoxID);
            this.groupBoxSettings.Controls.Add(this.label8);
            this.groupBoxSettings.Controls.Add(this.dateTimePickerStartDate);
            this.groupBoxSettings.Controls.Add(this.dateTimePickerEndTime);
            this.groupBoxSettings.Controls.Add(this.comboBoxCritical);
            this.groupBoxSettings.Controls.Add(this.dateTimePickerStartTime);
            this.groupBoxSettings.Controls.Add(this.label2);
            this.groupBoxSettings.Controls.Add(this.label1);
            this.groupBoxSettings.Controls.Add(this.label4);
            this.groupBoxSettings.Controls.Add(this.dateTimePickerEndDate);
            this.groupBoxSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSettings.Location = new System.Drawing.Point(0, 0);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(399, 413);
            this.groupBoxSettings.TabIndex = 46;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Параметры запроса";
            // 
            // numericUpDownHistory
            // 
            this.numericUpDownHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownHistory.Location = new System.Drawing.Point(272, 364);
            this.numericUpDownHistory.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownHistory.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHistory.Name = "numericUpDownHistory";
            this.numericUpDownHistory.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownHistory.TabIndex = 62;
            this.numericUpDownHistory.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownHistory.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(216, 367);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 61;
            this.label6.Text = "История:";
            // 
            // numericUpDownTasks
            // 
            this.numericUpDownTasks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownTasks.Location = new System.Drawing.Point(102, 364);
            this.numericUpDownTasks.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownTasks.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTasks.Name = "numericUpDownTasks";
            this.numericUpDownTasks.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownTasks.TabIndex = 60;
            this.numericUpDownTasks.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownTasks.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 367);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Задачи:";
            // 
            // numericUpDownComments
            // 
            this.numericUpDownComments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownComments.Location = new System.Drawing.Point(272, 388);
            this.numericUpDownComments.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownComments.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownComments.Name = "numericUpDownComments";
            this.numericUpDownComments.Size = new System.Drawing.Size(45, 20);
            this.numericUpDownComments.TabIndex = 58;
            this.numericUpDownComments.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownComments.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(189, 391);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 57;
            this.label3.Text = "Комментарии:";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageStatus);
            this.tabControl1.Controls.Add(this.tabPageService);
            this.tabControl1.Controls.Add(this.tabPageWorkGroup);
            this.tabControl1.Controls.Add(this.tabPageCreator);
            this.tabControl1.Controls.Add(this.tabPageExecutant);
            this.tabControl1.Location = new System.Drawing.Point(8, 128);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(387, 228);
            this.tabControl1.TabIndex = 56;
            // 
            // tabPageStatus
            // 
            this.tabPageStatus.Controls.Add(this.checkedListBoxStatus);
            this.tabPageStatus.Location = new System.Drawing.Point(4, 22);
            this.tabPageStatus.Name = "tabPageStatus";
            this.tabPageStatus.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageStatus.Size = new System.Drawing.Size(379, 202);
            this.tabPageStatus.TabIndex = 0;
            this.tabPageStatus.Text = "Статус";
            // 
            // checkedListBoxStatus
            // 
            this.checkedListBoxStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxStatus.FormattingEnabled = true;
            this.checkedListBoxStatus.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxStatus.Name = "checkedListBoxStatus";
            this.checkedListBoxStatus.Size = new System.Drawing.Size(379, 199);
            this.checkedListBoxStatus.TabIndex = 1;
            this.checkedListBoxStatus.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageService
            // 
            this.tabPageService.Controls.Add(this.checkedListBoxService);
            this.tabPageService.Location = new System.Drawing.Point(4, 22);
            this.tabPageService.Name = "tabPageService";
            this.tabPageService.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageService.Size = new System.Drawing.Size(379, 202);
            this.tabPageService.TabIndex = 1;
            this.tabPageService.Text = "Сервис";
            this.tabPageService.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxService
            // 
            this.checkedListBoxService.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxService.FormattingEnabled = true;
            this.checkedListBoxService.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxService.Name = "checkedListBoxService";
            this.checkedListBoxService.Size = new System.Drawing.Size(379, 199);
            this.checkedListBoxService.TabIndex = 1;
            this.checkedListBoxService.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageWorkGroup
            // 
            this.tabPageWorkGroup.Controls.Add(this.checkedListBoxWorkGroup);
            this.tabPageWorkGroup.Location = new System.Drawing.Point(4, 22);
            this.tabPageWorkGroup.Name = "tabPageWorkGroup";
            this.tabPageWorkGroup.Size = new System.Drawing.Size(379, 202);
            this.tabPageWorkGroup.TabIndex = 4;
            this.tabPageWorkGroup.Text = "Рабочие группы";
            this.tabPageWorkGroup.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxWorkGroup
            // 
            this.checkedListBoxWorkGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkedListBoxWorkGroup.FormattingEnabled = true;
            this.checkedListBoxWorkGroup.Location = new System.Drawing.Point(0, 2);
            this.checkedListBoxWorkGroup.Name = "checkedListBoxWorkGroup";
            this.checkedListBoxWorkGroup.Size = new System.Drawing.Size(379, 199);
            this.checkedListBoxWorkGroup.TabIndex = 2;
            this.checkedListBoxWorkGroup.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageCreator
            // 
            this.tabPageCreator.Controls.Add(this.checkedListBoxCreator);
            this.tabPageCreator.Location = new System.Drawing.Point(4, 22);
            this.tabPageCreator.Name = "tabPageCreator";
            this.tabPageCreator.Size = new System.Drawing.Size(379, 202);
            this.tabPageCreator.TabIndex = 2;
            this.tabPageCreator.Text = "Инициатор";
            this.tabPageCreator.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxCreator
            // 
            this.checkedListBoxCreator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxCreator.FormattingEnabled = true;
            this.checkedListBoxCreator.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxCreator.Name = "checkedListBoxCreator";
            this.checkedListBoxCreator.Size = new System.Drawing.Size(379, 202);
            this.checkedListBoxCreator.TabIndex = 1;
            this.checkedListBoxCreator.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageExecutant
            // 
            this.tabPageExecutant.Controls.Add(this.checkedListBoxExecutant);
            this.tabPageExecutant.Location = new System.Drawing.Point(4, 22);
            this.tabPageExecutant.Name = "tabPageExecutant";
            this.tabPageExecutant.Size = new System.Drawing.Size(379, 202);
            this.tabPageExecutant.TabIndex = 3;
            this.tabPageExecutant.Text = "Исполнитель";
            this.tabPageExecutant.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxExecutant
            // 
            this.checkedListBoxExecutant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxExecutant.FormattingEnabled = true;
            this.checkedListBoxExecutant.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxExecutant.Name = "checkedListBoxExecutant";
            this.checkedListBoxExecutant.Size = new System.Drawing.Size(379, 202);
            this.checkedListBoxExecutant.TabIndex = 0;
            this.checkedListBoxExecutant.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // checkBoxAutoUpdate
            // 
            this.checkBoxAutoUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBoxAutoUpdate.AutoSize = true;
            this.checkBoxAutoUpdate.Location = new System.Drawing.Point(12, 390);
            this.checkBoxAutoUpdate.Name = "checkBoxAutoUpdate";
            this.checkBoxAutoUpdate.Size = new System.Drawing.Size(91, 17);
            this.checkBoxAutoUpdate.TabIndex = 55;
            this.checkBoxAutoUpdate.Text = "Обновление:";
            this.checkBoxAutoUpdate.UseVisualStyleBackColor = true;
            // 
            // numericUpDownUpdateInterval
            // 
            this.numericUpDownUpdateInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numericUpDownUpdateInterval.Location = new System.Drawing.Point(102, 388);
            this.numericUpDownUpdateInterval.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownUpdateInterval.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownUpdateInterval.Name = "numericUpDownUpdateInterval";
            this.numericUpDownUpdateInterval.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownUpdateInterval.TabIndex = 54;
            this.numericUpDownUpdateInterval.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(96, 23);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(140, 20);
            this.textBoxID.TabIndex = 52;
            this.textBoxID.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBoxID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(72, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 51;
            this.label8.Text = "ID:";
            // 
            // dateTimePickerStartDate
            // 
            this.dateTimePickerStartDate.Location = new System.Drawing.Point(96, 49);
            this.dateTimePickerStartDate.Name = "dateTimePickerStartDate";
            this.dateTimePickerStartDate.Size = new System.Drawing.Size(140, 20);
            this.dateTimePickerStartDate.TabIndex = 35;
            // 
            // dateTimePickerEndTime
            // 
            this.dateTimePickerEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerEndTime.Location = new System.Drawing.Point(242, 75);
            this.dateTimePickerEndTime.Name = "dateTimePickerEndTime";
            this.dateTimePickerEndTime.ShowUpDown = true;
            this.dateTimePickerEndTime.Size = new System.Drawing.Size(78, 20);
            this.dateTimePickerEndTime.TabIndex = 40;
            this.dateTimePickerEndTime.Value = new System.DateTime(2016, 7, 22, 0, 0, 0, 0);
            // 
            // comboBoxCritical
            // 
            this.comboBoxCritical.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCritical.FormattingEnabled = true;
            this.comboBoxCritical.Items.AddRange(new object[] {
            "Все",
            "Не критичные",
            "Критичные"});
            this.comboBoxCritical.Location = new System.Drawing.Point(96, 101);
            this.comboBoxCritical.Name = "comboBoxCritical";
            this.comboBoxCritical.Size = new System.Drawing.Size(140, 21);
            this.comboBoxCritical.TabIndex = 44;
            // 
            // dateTimePickerStartTime
            // 
            this.dateTimePickerStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerStartTime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dateTimePickerStartTime.Location = new System.Drawing.Point(242, 49);
            this.dateTimePickerStartTime.Name = "dateTimePickerStartTime";
            this.dateTimePickerStartTime.ShowUpDown = true;
            this.dateTimePickerStartTime.Size = new System.Drawing.Size(78, 20);
            this.dateTimePickerStartTime.TabIndex = 37;
            this.dateTimePickerStartTime.Value = new System.DateTime(2013, 9, 20, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(69, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "По:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 36;
            this.label1.Text = "С:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 43;
            this.label4.Text = "Критичность:";
            // 
            // dateTimePickerEndDate
            // 
            this.dateTimePickerEndDate.Location = new System.Drawing.Point(96, 75);
            this.dateTimePickerEndDate.Name = "dateTimePickerEndDate";
            this.dateTimePickerEndDate.Size = new System.Drawing.Size(140, 20);
            this.dateTimePickerEndDate.TabIndex = 39;
            // 
            // listViewComments
            // 
            this.listViewComments.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnDate,
            this.columnUser,
            this.columnText});
            this.listViewComments.ContextMenuStrip = this.contextMenuStripCommentsAndHistory;
            this.listViewComments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewComments.FullRowSelect = true;
            this.listViewComments.GridLines = true;
            this.listViewComments.Location = new System.Drawing.Point(0, 0);
            this.listViewComments.MultiSelect = false;
            this.listViewComments.Name = "listViewComments";
            this.listViewComments.OwnerDraw = true;
            this.listViewComments.Size = new System.Drawing.Size(303, 413);
            this.listViewComments.TabIndex = 6;
            this.listViewComments.UseCompatibleStateImageBehavior = false;
            this.listViewComments.View = System.Windows.Forms.View.Details;
            this.listViewComments.VirtualMode = true;
            this.listViewComments.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewComments_DrawColumnHeader);
            this.listViewComments.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listViewComments_DrawSubItem);
            this.listViewComments.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.listViewComments_RetrieveVirtualItem);
            this.listViewComments.DoubleClick += new System.EventHandler(this.listViewComments_DoubleClick);
            // 
            // columnDate
            // 
            this.columnDate.Text = "Дата";
            // 
            // columnUser
            // 
            this.columnUser.Text = "Пользователь";
            // 
            // columnText
            // 
            this.columnText.Text = "Текст комментария";
            this.columnText.Width = 68;
            // 
            // contextMenuStripCommentsAndHistory
            // 
            this.contextMenuStripCommentsAndHistory.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.экспортВCSVToolStripMenuItem});
            this.contextMenuStripCommentsAndHistory.Name = "contextMenuStripCommentsAndHistory";
            this.contextMenuStripCommentsAndHistory.Size = new System.Drawing.Size(153, 26);
            this.contextMenuStripCommentsAndHistory.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStripCommentsAndHistory_Opening);
            // 
            // экспортВCSVToolStripMenuItem
            // 
            this.экспортВCSVToolStripMenuItem.Name = "экспортВCSVToolStripMenuItem";
            this.экспортВCSVToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.экспортВCSVToolStripMenuItem.Text = "Экспорт в CSV";
            this.экспортВCSVToolStripMenuItem.Click += new System.EventHandler(this.экспортВCSVToolStripMenuItem_Click);
            // 
            // listViewHistory
            // 
            this.listViewHistory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.listViewHistory.ContextMenuStrip = this.contextMenuStripCommentsAndHistory;
            this.listViewHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewHistory.FullRowSelect = true;
            this.listViewHistory.GridLines = true;
            this.listViewHistory.Location = new System.Drawing.Point(0, 0);
            this.listViewHistory.MultiSelect = false;
            this.listViewHistory.Name = "listViewHistory";
            this.listViewHistory.OwnerDraw = true;
            this.listViewHistory.Size = new System.Drawing.Size(1285, 111);
            this.listViewHistory.TabIndex = 6;
            this.listViewHistory.UseCompatibleStateImageBehavior = false;
            this.listViewHistory.View = System.Windows.Forms.View.Details;
            this.listViewHistory.VirtualMode = true;
            this.listViewHistory.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewHistory_DrawColumnHeader);
            this.listViewHistory.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listViewHistory_DrawSubItem);
            this.listViewHistory.RetrieveVirtualItem += new System.Windows.Forms.RetrieveVirtualItemEventHandler(this.listViewHistory_RetrieveVirtualItem);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Дата";
            this.columnHeader9.Width = 160;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Пользователь";
            this.columnHeader10.Width = 215;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Описание";
            this.columnHeader11.Width = 610;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStripMultiTask
            // 
            this.contextMenuStripMultiTask.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.закрытьЗадачиToolStripMenuItem,
            this.toolStripSeparator3,
            this.ЭкспортExcelToolStripMenuItem,
            this.ЭкспортWordToolStripMenuItem});
            this.contextMenuStripMultiTask.Name = "contextMenuStripMultiTask";
            this.contextMenuStripMultiTask.Size = new System.Drawing.Size(161, 76);
            // 
            // закрытьЗадачиToolStripMenuItem
            // 
            this.закрытьЗадачиToolStripMenuItem.Name = "закрытьЗадачиToolStripMenuItem";
            this.закрытьЗадачиToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.закрытьЗадачиToolStripMenuItem.Text = "Закрыть задачи";
            this.закрытьЗадачиToolStripMenuItem.Click += new System.EventHandler(this.закрытьЗадачиToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(157, 6);
            // 
            // ЭкспортExcelToolStripMenuItem
            // 
            this.ЭкспортExcelToolStripMenuItem.Name = "ЭкспортExcelToolStripMenuItem";
            this.ЭкспортExcelToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.ЭкспортExcelToolStripMenuItem.Text = "Экспорт в Excel";
            this.ЭкспортExcelToolStripMenuItem.Click += new System.EventHandler(this.ЭкспортExcelToolStripMenuItem_Click);
            // 
            // ЭкспортWordToolStripMenuItem
            // 
            this.ЭкспортWordToolStripMenuItem.Name = "ЭкспортWordToolStripMenuItem";
            this.ЭкспортWordToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.ЭкспортWordToolStripMenuItem.Text = "Экспорт в Word";
            this.ЭкспортWordToolStripMenuItem.Click += new System.EventHandler(this.ЭкспортWordToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1285, 575);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Task Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MainForm_KeyPress);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.contextMenuStripTask.ResumeLayout(false);
            this.panelSettings.ResumeLayout(false);
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownComments)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPageStatus.ResumeLayout(false);
            this.tabPageService.ResumeLayout(false);
            this.tabPageWorkGroup.ResumeLayout(false);
            this.tabPageCreator.ResumeLayout(false);
            this.tabPageExecutant.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUpdateInterval)).EndInit();
            this.contextMenuStripCommentsAndHistory.ResumeLayout(false);
            this.contextMenuStripMultiTask.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatus;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ListView listViewComments;
        private System.Windows.Forms.ColumnHeader columnDate;
        private System.Windows.Forms.ColumnHeader columnText;
        private System.Windows.Forms.ColumnHeader columnUser;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTask;
        private System.Windows.Forms.ToolStripMenuItem создатьЗадачуToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonUpdate;
        private System.Windows.Forms.ListView listViewHistory;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ListView listViewTask;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartDate;
        private System.Windows.Forms.ComboBox comboBoxCritical;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndTime;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.ToolStripMenuItem изменитьЗадачуToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem добавитьКомментарийToolStripMenuItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.NumericUpDown numericUpDownUpdateInterval;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox checkBoxAutoUpdate;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusEvent;
        private System.Windows.Forms.ToolStripButton toolStripButtonConnectionSettings;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageStatus;
        private System.Windows.Forms.TabPage tabPageService;
        private System.Windows.Forms.TabPage tabPageCreator;
        private System.Windows.Forms.TabPage tabPageExecutant;
        private System.Windows.Forms.CheckedListBox checkedListBoxStatus;
        private System.Windows.Forms.CheckedListBox checkedListBoxService;
        private System.Windows.Forms.CheckedListBox checkedListBoxCreator;
        private System.Windows.Forms.CheckedListBox checkedListBoxExecutant;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem экспортCSVToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripCommentsAndHistory;
        private System.Windows.Forms.ToolStripMenuItem экспортВCSVToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.NumericUpDown numericUpDownComments;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownHistory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDownTasks;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPageWorkGroup;
        private System.Windows.Forms.CheckedListBox checkedListBoxWorkGroup;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButtonReports;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ToolStripMenuItem событияToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelProgramVersion;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelUpdateStatus;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripMultiTask;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem ЭкспортExcelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ЭкспортWordToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem закрытьЗадачиToolStripMenuItem;
    }
}

