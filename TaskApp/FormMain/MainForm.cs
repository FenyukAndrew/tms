﻿using System;
using System.Drawing;
using System.Net;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;
using TaskApp.FormCloseTasks;

namespace TaskApp
{
    public partial class MainForm : Form, IMainForm
    {
        public event EventHandler<EventArgs> formLoad;
        public event EventHandler<EventArgs> asyncButtonsClick;
        public event EventHandler<RetrieveVirtualItemEventArgs> listViewRetrieveVirtualItem;
        public event EventHandler<EventArgs> contextMenuTaskItemsClick;
        public event EventHandler<EventArgs> formClosing;
        public event EventHandler<RetrieveVirtualItemEventArgs> listViewCommentRetrieveVirtualItem;
        public event EventHandler<EventArgs> listViewTaskSelectedIndexChange;
        public event EventHandler<RetrieveVirtualItemEventArgs> listViewHistoryRetrieveVirtualItem;
        public event EventHandler<ColumnClickEventArgs> listViewTaskColumnClick;
        public event EventHandler<DrawListViewSubItemEventArgs> listViewTaskDrawSubItem;
        public event EventHandler<EventArgs> listViewTaskDoubleClick;
        public event EventHandler<EventArgs> listViewCommentDoubleClick;
        public event EventHandler<EventArgs> toolStripButtonConnectionSettingsClick;
        public event EventHandler<EventArgs> checkBoxAutoUpdateCheckedChanged;
        public event EventHandler<EventArgs> menuItemExportCSVClick;
        public event EventHandler<EventArgs> toolStripButtonReportsClick;

        public event EventHandler<TelegramEventArgs> listViewCloseSelectedTask;
        public event EventHandler<EventArgs> listViewExportExcelSelectedTask;
        public event EventHandler<EventArgs> listViewExportWordSelectedTask;

        private readonly int PANELSIZE = 10;

        public MainForm()
        {
            InitializeComponent();
            groupBoxSettings.DoubleClick += GroupBoxSettings_DoubleClick;
            checkBoxAutoUpdate.CheckedChanged += CheckBoxAutoUpdate_CheckedChanged;

            Logger.InitLogger();//инициализация - требуется один раз в начале
        }

        private void CheckBoxAutoUpdate_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxAutoUpdateCheckedChanged?.Invoke(sender, e);
        }

        String TextStatus = "";
        Color TextStatusColor;

        public string UpdateComment
        {
            get { return toolStripStatus.Text; }
            set
            {
                TextStatus = DateTime.Now + " " + value;
                TextStatusColor = Color.Green;

                if (toolStrip1.InvokeRequired)
                    toolStrip1.Invoke(new Action(() =>
                    {
                        toolStripStatus.Text = TextStatus;
                        toolStripStatus.ForeColor = TextStatusColor;
                    }));
                else
                {
                    toolStripStatus.Text = TextStatus;
                    toolStripStatus.ForeColor = TextStatusColor;
                }
            }
        }

        public string UpdateCommentError
        {
            get { return toolStripStatus.Text; }
            set
            {
                TextStatus = DateTime.Now + " " + value;
                TextStatusColor = Color.Red;

                if (toolStrip1.InvokeRequired)
                    toolStrip1.Invoke(new Action(() =>
                    {
                        toolStripStatus.Text = TextStatus;
                        toolStripStatus.ForeColor = TextStatusColor;
                    }));
                else
                {
                    toolStripStatus.Text = TextStatus;
                    toolStripStatus.ForeColor = TextStatusColor;
                }
            }
        }

        public int ListViewTaskVirtualListSize
        {
            get { return listViewTask.VirtualListSize; }
            set
            {
                if (listViewTask.InvokeRequired)
                    listViewTask.Invoke(new Action(() => listViewTask.VirtualListSize = value));
                else
                    listViewTask.VirtualListSize = value;
            }
        }

        public int ListViewCommentVirtualListSize
        {
            get { return listViewComments.VirtualListSize; }
            set
            {
                if (listViewComments.InvokeRequired)
                    listViewComments.Invoke(new Action(() => listViewComments.VirtualListSize = value));
                else
                    listViewComments.VirtualListSize = value;
            }
        }

        public int ListViewTaskSelectedIndex
        {
            get
            {
                int result = -1;
                if (listViewTask.InvokeRequired)
                    listViewTask.Invoke(new Action(() =>
                    {
                        result = listViewTask.SelectedIndices.Count > 0 ? listViewTask.SelectedIndices[0] : -1;
                        if (result == -1)
                        {
                            listViewHistory.VirtualListSize = 0;
                            listViewComments.VirtualListSize = 0;
                        }
                    }));
                else
                {
                    result = listViewTask.SelectedIndices.Count > 0 ? listViewTask.SelectedIndices[0] : -1;
                    if (result == -1)
                    {
                        listViewHistory.VirtualListSize = 0;
                        listViewComments.VirtualListSize = 0;
                    }
                }
                return result;
            }
            set
            {
                try
                {
                    if (listViewTask.InvokeRequired)
                        listViewTask.Invoke(new Action(() =>
                        {
                            listViewTask.SelectedIndices.Clear();
                            listViewTask.SelectedIndices.Add(value);
                        }));
                    else
                    {
                        listViewTask.SelectedIndices.Clear();
                        listViewTask.SelectedIndices.Add(value);
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError("public int ListViewTaskSelectedIndex: " + ex.Message, "addEditTaskForm_buttonsClick.txt");
                }
            }
        }

        public int[] ListViewTaskSelectedIndexs
        {
            get
            {
                ListView.SelectedIndexCollection result = null;
                if (listViewTask.InvokeRequired)
                    listViewTask.Invoke(new Action(() =>
                    {
                        result = listViewTask.SelectedIndices.Count > 0 ? listViewTask.SelectedIndices : null;
                        if (result == null)
                        {
                            listViewHistory.VirtualListSize = 0;
                            listViewComments.VirtualListSize = 0;
                        }
                    }));
                else
                {

                    result = listViewTask.SelectedIndices.Count > 0 ? listViewTask.SelectedIndices : null;
                    if (result == null)
                    {
                        listViewHistory.VirtualListSize = 0;
                        listViewComments.VirtualListSize = 0;
                    }
                }

                if (result != null)
                {
                    int[] dest = new int[result.Count];
                    result.CopyTo(dest, 0);
                    return dest;
                }
                else
                {
                    return new int[0];
                }
            }
            set
            {
                try
                {
                    if (listViewTask.InvokeRequired)
                        listViewTask.Invoke(new Action(() =>
                        {
                            listViewTask.SelectedIndices.Clear();
                            foreach (var index in value)
                            {
                                listViewTask.SelectedIndices.Add(index);
                            }
                        }));
                    else
                    {
                        listViewTask.SelectedIndices.Clear();
                        foreach (var index in value)
                        {
                            listViewTask.SelectedIndices.Add(index);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError("public int ListViewTaskSelectedIndex: " + ex.Message, "addEditTaskForm_buttonsClick.txt");
                }
            }
        }

        bool IMainForm.ToolStripButtonUpdateEnabled
        {
            get { return toolStripButtonUpdate.Enabled; }
            set
            {
                if (toolStrip1.InvokeRequired)
                    toolStrip1.Invoke(new Action(() => toolStripButtonUpdate.Enabled = value));
                else
                    toolStripButtonUpdate.Enabled = value;
            }
        }

        public int ListViewHistoryVirtualListSize
        {
            get { return listViewHistory.VirtualListSize; }
            set
            {
                if (listViewHistory.InvokeRequired)
                    listViewHistory.Invoke(new Action(() => listViewHistory.VirtualListSize = value));
                else
                    listViewHistory.VirtualListSize = value;
            }
        }

        DateTimePicker IMainForm.DateTimePickerStartDate
        {
            get { return dateTimePickerStartDate; }
            set { dateTimePickerStartDate = value; }
        }

        DateTimePicker IMainForm.DateTimePickerStartTime
        {
            get { return dateTimePickerStartTime; }
            set { dateTimePickerStartTime = value; }
        }

        DateTimePicker IMainForm.DateTimePickerEndDate
        {
            get { return dateTimePickerEndDate; }
            set { dateTimePickerEndDate = value; }
        }

        DateTimePicker IMainForm.DateTimePickerEndTime
        {
            get { return dateTimePickerEndTime; }
            set { dateTimePickerEndTime = value; }
        }

        public string GetTaskId
        {
            get
            {
                var v = listViewTask.Items[listViewTask.SelectedIndices[0]];
                return listViewTask.SelectedIndices.Count > 0 ? listViewTask.Items[listViewTask.SelectedIndices[0]].ToString() : "";
            }
        }

        public string TextBoxId
        {
            get { return textBoxID.Text; }
            set { textBoxID.Text = value; }
        }

        public int FormX1
        {
            get { return this.Left; }
            set { this.Left = value; }
        }

        public int FormX2
        {
            get { return this.Width; }
            set { this.Width = value; }
        }

        public int FormY1
        {
            get { return this.Top; }
            set { this.Top = value; }
        }

        public int FormY2
        {
            get { return this.Height; }
            set { this.Height = value; }
        }

        public int SplitContainerSpliter1Distance
        {
            get { return splitContainer2.SplitterDistance; }
            set { splitContainer2.SplitterDistance = value; }
        }

        public int SplitContainerSpliter2Distance
        {
            get { return splitContainer1.SplitterDistance; }
            set { splitContainer1.SplitterDistance = value; }
        }

        public int ListViewTaskColumnId
        {
            get { return listViewTask.Columns[0].Width; }
            set { listViewTask.Columns[0].Width = value; }
        }

        public int ListViewTaskColumnDate
        {
            get { return listViewTask.Columns[1].Width; }
            set { listViewTask.Columns[1].Width = value; }
        }

        public int ListViewTaskColumnTheme
        {
            get { return listViewTask.Columns[2].Width; }
            set { listViewTask.Columns[2].Width = value; }
        }

        public int ListViewTaskColumnDescription
        {
            get { return listViewTask.Columns[3].Width; }
            set { listViewTask.Columns[3].Width = value; }
        }

        public int ListViewTaskColumnCreator
        {
            get { return listViewTask.Columns[4].Width; }
            set { listViewTask.Columns[4].Width = value; }
        }

        public int ListViewTaskColumnExecutant
        {
            get { return listViewTask.Columns[5].Width; }
            set { listViewTask.Columns[5].Width = value; }
        }

        public int ListViewTaskColumnService
        {
            get { return listViewTask.Columns[7].Width; }
            set { listViewTask.Columns[7].Width = value; }
        }

        public int ListViewTaskColumnWorkGroup
        {
            get { return listViewTask.Columns[8].Width; }
            set { listViewTask.Columns[8].Width = value; }
        }

        public int ListViewTaskColumnStatus
        {
            get { return listViewTask.Columns[6].Width; }
            set { listViewTask.Columns[6].Width = value; }
        }


        //Ддя сохранения и восстановления порядка следования колонок
        public int[] ListViewTaskDisplayIndexsColumns
        {
            get
            {
                int[] displayIndexs = new int[listViewTask.Columns.Count];
                for (int i = 0; i < listViewTask.Columns.Count; i++)
                {
                    displayIndexs[i] = listViewTask.Columns[i].DisplayIndex;
                }
                return displayIndexs;
            }
            set
            {
                for (int i = 0; i < value.Length; i++)
                {
                    listViewTask.Columns[i].DisplayIndex = value[i];
                }
            }
        }




        public int ListViewCommentColumnDate
        {
            get { return listViewComments.Columns[0].Width; }
            set { listViewComments.Columns[0].Width = value; }
        }

        public int ListViewCommentColumnUser
        {
            get { return listViewComments.Columns[1].Width; }
            set { listViewComments.Columns[1].Width = value; }
        }

        public int ListViewCommentColumnDescription
        {
            get { return listViewComments.Columns[2].Width; }
            set { listViewComments.Columns[2].Width = value; }
        }

        public int ListViewHistoryColumnDate
        {
            get { return listViewHistory.Columns[0].Width; }
            set { listViewHistory.Columns[0].Width = value; }
        }

        public int ListViewHistoryColumnUser
        {
            get { return listViewHistory.Columns[1].Width; }
            set { listViewHistory.Columns[1].Width = value; }
        }

        public int ListViewHistoryColumnDescription
        {
            get { return listViewHistory.Columns[2].Width; }
            set { listViewHistory.Columns[2].Width = value; }
        }

        public string UpdateStatus
        {
            get { return toolStripStatusEvent.Text; }
            set
            {
                if (toolStrip1.InvokeRequired)
                {
                    toolStrip1.Invoke(new Action(() =>
                    {
                        toolStripStatusEvent.Text = value;
                        toolStripStatusEvent.ForeColor = Color.Green;
                    }));
                }
                else
                {
                    toolStripStatusEvent.Text = value;
                    toolStripStatusEvent.ForeColor = Color.Green;
                }
            }
        }

        public string UpdateStatusError
        {
            get { return toolStripStatusEvent.Text; }
            set
            {
                if (toolStrip1.InvokeRequired)
                {
                    toolStrip1.Invoke(new Action(() =>
                    {
                        toolStripStatusEvent.Text = value;
                        toolStripStatusEvent.ForeColor = Color.Red;
                    }));
                }
                else
                {
                    toolStripStatusEvent.Text = value;
                    toolStripStatusEvent.ForeColor = Color.Red;
                }
            }
        }

        ToolStripButton IMainForm.ToolStripButtonUpdate
        {
            get { return toolStripButtonUpdate; }
            set { toolStripButtonUpdate = value; }
        }

        public bool ListViewCommentEnabled
        {
            get { return listViewComments.Enabled; }
            set
            {
                if (listViewComments.InvokeRequired)
                    listViewComments.Invoke(new Action(() =>
                    {
                        // if (value == false)
                        listViewComments.VirtualListSize = 0;
                        // listViewComments.VirtualMode = value;
                    }));
                //  else
                //      listViewComments.Enabled = value;
            }
        }

        public bool ListViewHistoryEnabled
        {
            get { return listViewHistory.Enabled; }
            set
            {
                if (listViewHistory.InvokeRequired)
                    listViewHistory.Invoke(new Action(() =>
                    {
                        // if (value == false)
                        listViewHistory.VirtualListSize = 0;
                        // listViewHistory.VirtualMode = value;
                    }));
                //   else
                //       listViewHistory.Enabled = value;
            }
        }

        CheckedListBox IMainForm.checkedListBoxStatus
        {
            get { return checkedListBoxStatus; }
            set { checkedListBoxStatus = value; }
        }

        CheckedListBox IMainForm.checkedListBoxService
        {
            get { return checkedListBoxService; }
            set { checkedListBoxService = value; }
        }

        CheckedListBox IMainForm.checkedListBoxCreator
        {
            get { return checkedListBoxCreator; }
            set { checkedListBoxCreator = value; }
        }

        CheckedListBox IMainForm.checkedListBoxExecutant
        {
            get { return checkedListBoxExecutant; }
            set { checkedListBoxExecutant = value; }
        }

        public ComboBox ComboBoxCritical
        {
            get { return comboBoxCritical; }
            set { comboBoxCritical = value; }
        }

        public bool CheckBoxAutoupdateChecked
        {
            get { return checkBoxAutoUpdate.Checked; }
            set
            {
                if (checkBoxAutoUpdate.InvokeRequired)
                    checkBoxAutoUpdate.Invoke(new Action(() => checkBoxAutoUpdate.Checked = value));
                else
                    checkBoxAutoUpdate.Checked = value;
            }
        }

        public int numericUpDownUpdateIntervalValue
        {
            get { return (int)numericUpDownUpdateInterval.Value; }
        }

        public string ListViewCommentTag
        {
            get { return (string)listViewComments.Tag; }
            set { listViewComments.Tag = value; }
        }

        public string ListViewHistoryTag
        {
            get { return (string)listViewHistory.Tag; }
            set { listViewHistory.Tag = value; }
        }

        public string FormText
        {
            get { return Text; }
            set { Text = value; }
        }

        public int ListViewTaskFontSize
        {
            get { return (int)listViewTask.Font.Size; }
            set { numericUpDownTasks.Value = value; }
        }

        public int ListViewHistoryFontSize
        {
            get { return (int)listViewHistory.Font.Size; }
            set { numericUpDownHistory.Value = value; }
        }

        public int ListViewCommentFontSize
        {
            get { return (int)listViewComments.Font.Size; }
            set { numericUpDownComments.Value = value; }
        }

        public int NumericUpDownUpdateInterval
        {
            get { return (int)numericUpDownUpdateInterval.Value; }
            set { numericUpDownUpdateInterval.Value = value; }
        }

        public bool CheckBoxUpdateState
        {
            get { return checkBoxAutoUpdate.Checked; }
            set { checkBoxAutoUpdate.Checked = value; }
        }

        CheckedListBox IMainForm.checkedListBoxWorkGroup
        {
            get { return checkedListBoxWorkGroup; }
            set { checkedListBoxWorkGroup = value; }
        }

        public Form mainForm
        {
            get { return this; }
        }

        public string CheckUpdateProgramStatus
        {
            get { return toolStripStatusLabelUpdateStatus.Text; }
            set
            {
                if (toolStrip1.InvokeRequired)
                {
                    toolStrip1.Invoke(new Action(() =>
                    {
                        toolStripStatusLabelUpdateStatus.Text = value;
                        toolStripStatusLabelUpdateStatus.ForeColor = Color.Green;
                    }));
                }
                else
                {
                    toolStripStatusLabelUpdateStatus.Text = value;
                    toolStripStatusLabelUpdateStatus.ForeColor = Color.Green;
                }
            }
        }

        public string CheckUpdateProgramStatusError
        {
            get { return toolStripStatusLabelUpdateStatus.Text; }
            set
            {
                if (toolStrip1.InvokeRequired)
                {
                    toolStrip1.Invoke(new Action(() =>
                    {
                        toolStripStatusLabelUpdateStatus.Text = value;
                        toolStripStatusLabelUpdateStatus.ForeColor = Color.Red;
                    }));
                }
                else
                {
                    toolStripStatusLabelUpdateStatus.Text = value;
                    toolStripStatusLabelUpdateStatus.ForeColor = Color.Red;
                }
            }
        }

        private void GroupBoxSettings_DoubleClick(object sender, EventArgs e)
        {
            if (panelSettings.Width < 50)
            {
                panelSettings.Width = 350;
            }
            else
            {
                panelSettings.Width = PANELSIZE;
            }
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusLabelUpdateStatus.Text = "";
            Type type = listViewTask.GetType();
            PropertyInfo propertyInfo = type.GetProperty("DoubleBuffered", BindingFlags.NonPublic | BindingFlags.Instance);
            propertyInfo.SetValue(listViewTask, true, null);

            dateTimePickerStartDate.Value = DateTime.Now.AddDays(-91);
            //dateTimePickerEndDate.Value = DateTime.Now.AddDays(1);
            dateTimePickerEndTime.Value = DateTime.Parse("2000-01-01 23:59:59");
            panelSettings.Width = PANELSIZE;
            formLoad?.Invoke(sender, e);
        }

        private void ContextMenuStripTaskItemsClick(object sender, EventArgs e)
        {
            contextMenuTaskItemsClick?.Invoke(sender, e);
        }


        private void listViewData_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            listViewRetrieveVirtualItem?.Invoke(sender, e);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            string key = "{\"keyboard\":[ [\"Принять\", \"Отклонить\"], [\"Управление задачами\"] ] }";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.telegram.org/bot263264363:AAGQ4TWM4Br9zZ5uUudpDJQW_zjUpcBamm8/sendMessage?chat_id=227643235&text=\"текст\"" + "&reply_markup=" + key);
            var response = request.GetResponseAsync();
        }

        private void listViewComments_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listViewComments_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listViewComments_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            listViewCommentRetrieveVirtualItem?.Invoke(sender, e);
        }

        int lastSelectIndex = -1;

        private void listViewData_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            Решения для проблемы с Shift не найдено
            if (Control.ModifierKeys == Keys.Shift)
            {
                ListView listView = ((ListView)sender);

                //int curSelectIndex = listView.Items.IndexOf(listView.FocusedItem);

                Point pos = this.PointToClient(Cursor.Position);
                var item = GetItemAt(pos.X, pos.Y);

                int upper = Math.Max(lastSelectIndex, curSelectIndex);
                int lower = Math.Min(lastSelectIndex, curSelectIndex);

                if (lower > -1)
                {
                    for (int i = lower; i < upper; i++)
                    {
                        ((ListView)sender).SelectedIndices.Add(i);
                        //((ListView)sender).SetItemCheckState(i, CheckState.Checked);
                    }
                }

                for (int i=0; i<listView.Items.Count;i++)
                {
                    listView.Items[i].Selected=true;
                }
            }*/

            int selectedCount = ((ListView)sender).SelectedIndices.Count;
            if (selectedCount > 0)
            {
                lastSelectIndex = ((ListView)sender).SelectedIndices[0];

                if (selectedCount == 1)
                {//При выборе 2 элементов и более не доступны комментарии и история
                    listViewComments.Enabled = true;
                    listViewHistory.Enabled = true;

                    if (listViewTaskSelectedIndexChange != null)
                    {
                        listViewTaskSelectedIndexChange(sender, e);
                        listViewComments.Refresh();
                        listViewHistory.Refresh();
                    }

                    listViewTask.ContextMenuStrip = contextMenuStripTask;

                    toolStripStatus.Text = TextStatus;
                    toolStripStatus.ForeColor = TextStatusColor;
                }
                else
                {
                    listViewComments.VirtualListSize = 0;
                    listViewComments.Enabled = false;
                    listViewHistory.VirtualListSize = 0;
                    listViewHistory.Enabled = false;

                    listViewTask.ContextMenuStrip = contextMenuStripMultiTask;

                    toolStripStatus.Text = "Выбрано задач: " + selectedCount;
                    toolStripStatus.ForeColor = Color.Blue;
                }

            }
            else
            {
                listViewComments.VirtualListSize = 0;
                listViewHistory.VirtualListSize = 0;

                toolStripStatus.Text = TextStatus;
                toolStripStatus.ForeColor = TextStatusColor;
            }
        }

        private void toolStripButtonUpdate_Click(object sender, EventArgs e)
        {
            if (toolStripButtonReports.Enabled == false)
                toolStripButtonReports.Enabled = true;

            ToolStripButton button = (ToolStripButton)sender;
            if (button.Name == "toolStripButtonUpdate")
            {
                asyncButtonsClick?.Invoke(sender, e);
            }
        }

        private void listViewHistory_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listViewHistory_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listViewHistory_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            listViewHistoryRetrieveVirtualItem?.Invoke(sender, e);
        }

        private void panel1_DoubleClick(object sender, EventArgs e)
        {

        }

        private void listViewTask_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listViewTask_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            listViewTaskDrawSubItem?.Invoke(sender, e);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            formClosing?.Invoke(sender, e);
        }

        private void listViewTask_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ((ListView)sender).SelectedIndices.Clear();
            listViewTaskColumnClick?.Invoke(sender, e);
        }

        public void listViewTaskInvalidate()
        {
            if (listViewTask.InvokeRequired)
                listViewTask.Invoke(new Action(() => listViewTask.Refresh()));
            else
                listViewTask.Refresh();
        }

        private void contextMenuStripTask_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ContextMenuStrip cm = (ContextMenuStrip)sender;
            if (checkedListBoxStatus.Items.Count == 0 || checkedListBoxService.Items.Count == 0 || checkedListBoxExecutant.Items.Count == 0 || checkedListBoxCreator.Items.Count == 0)
            {
                cm.Items[0].Enabled = false;
            }
            else
            {
                cm.Items[0].Enabled = true;
            }

            if (listViewTask.SelectedIndices.Count > 0)
            {
                cm.Items[1].Enabled = true;
                cm.Items[2].Enabled = true;
                cm.Items[3].Enabled = true;
                cm.Items[6].Enabled = true;
            }
            else
            {
                cm.Items[1].Enabled = false;
                cm.Items[2].Enabled = false;
                cm.Items[3].Enabled = false;
                cm.Items[6].Enabled = false;
            }

            if (listViewTask.Items.Count > 0)
                cm.Items[5].Enabled = true;
            else
                cm.Items[5].Enabled = false;
        }

        private void listViewTask_DoubleClick(object sender, EventArgs e)
        {
            if (((ListView)sender).SelectedIndices.Count > 0)
            {
                listViewTaskDoubleClick?.Invoke(((ListView)sender).SelectedIndices[0], e);
            }
        }

        private void listViewComments_DoubleClick(object sender, EventArgs e)
        {
            if (listViewTask.SelectedIndices.Count > 0)
            {
                if (((ListView)sender).SelectedIndices.Count > 0)
                {
                    listViewCommentDoubleClick?.Invoke(new int[] { listViewTask.SelectedIndices[0], ((ListView)sender).SelectedIndices[0] }, e);
                }
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || e.KeyChar == '\b') return;
            else
                e.Handled = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            if (tb.Text.Length > 0)
            {
                EnabledDisableComponents(false);
            }
            else
            {
                EnabledDisableComponents(true);
            }
        }

        private void EnabledDisableComponents(bool value)
        {
            if (checkedListBoxStatus.Items.Count > 0)
            {
                checkedListBoxStatus.ItemCheck -= checkedListBox_ItemCheck;
                checkedListBoxCreator.ItemCheck -= checkedListBox_ItemCheck;
                checkedListBoxExecutant.ItemCheck -= checkedListBox_ItemCheck;
                if (value == false)
                {
                    checkedListBoxStatus.SetItemChecked(0, true);
                    checkedListBoxService.SetItemChecked(0, true);
                    checkedListBoxCreator.SetItemChecked(0, true);
                    checkedListBoxExecutant.SetItemChecked(0, true);
                    checkedListBoxWorkGroup.SetItemChecked(0, true);
                }
                else
                {
                    if (checkedListBoxStatus.CheckedIndices.Count <= checkedListBoxStatus.Items.Count - 1)
                        checkedListBoxStatus.SetItemChecked(0, false);
                    if (checkedListBoxCreator.CheckedIndices.Count <= checkedListBoxCreator.Items.Count - 1)
                        checkedListBoxCreator.SetItemChecked(0, false);
                    if (checkedListBoxExecutant.CheckedIndices.Count <= checkedListBoxExecutant.Items.Count - 1)
                        checkedListBoxExecutant.SetItemChecked(0, false);
                }
                checkedListBoxStatus.ItemCheck += checkedListBox_ItemCheck;
                checkedListBoxCreator.ItemCheck += checkedListBox_ItemCheck;
                checkedListBoxExecutant.ItemCheck += checkedListBox_ItemCheck;

                dateTimePickerStartDate.Enabled = value;
                dateTimePickerStartTime.Enabled = value;
                dateTimePickerEndDate.Enabled = value;
                dateTimePickerEndTime.Enabled = value;
                checkedListBoxStatus.Enabled = value;
                checkedListBoxService.Enabled = value;
                comboBoxCritical.Enabled = value;
                checkedListBoxCreator.Enabled = value;
                checkedListBoxExecutant.Enabled = value;
                checkedListBoxWorkGroup.Enabled = value;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripButtonUpdate.PerformClick();
        }

        public void listViewCommentInvalidate()
        {
            listViewComments.Invalidate();
        }

        public void listViewHistoryInvalidate()
        {
            listViewHistory.Invalidate();
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            var v = e.KeyChar;
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void toolStripButtonConnectionSettings_Click(object sender, EventArgs e)
        {
            toolStripButtonConnectionSettingsClick?.Invoke(sender, e);
        }

        private void checkedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox chlb = (CheckedListBox)sender;

            var selText = chlb.SelectedItem;

            if (selText != null)
            {
                chlb.ItemCheck -= checkedListBox_ItemCheck;
                if (e.Index == 0)
                //if (((CheckListBoxItem)selText).Text == "Все")
                {
                    for (int i = 1; i < chlb.Items.Count; i++)
                    {
                        chlb.SetItemChecked(i, e.NewValue == CheckState.Checked ? true : false);
                    }
                }
                else
                {
                    if (e.NewValue == CheckState.Unchecked)
                    {
                        chlb.SetItemChecked(0, false);
                    }
                }
                chlb.ItemCheck += checkedListBox_ItemCheck;
            }

        }

        private void contextMenuStripCommentsAndHistory_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ContextMenuStrip menu = (ContextMenuStrip)sender;
            ListView lv = (ListView)menu.SourceControl;

            if (lv.Items.Count > 0)
                menu.Items[0].Enabled = true;
            else
                menu.Items[0].Enabled = false;
        }

        private void экспортВCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = (ToolStripMenuItem)sender;
            var parent = mi.GetCurrentParent();
            var lv = ((ContextMenuStrip)parent).SourceControl;

            if (lv != null)
                menuItemExportCSVClick?.Invoke(lv, e);
            else
                MessageBox.Show("Что-то пошло не так. Операция отменена.");
        }

        private void ListViewFontChange(ListView lv, float fontSize)
        {
            Font font = lv.Font;
            font = new Font(font.Name, fontSize);
            lv.Font = font;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown nud = (NumericUpDown)sender;

            if (nud.Name == "numericUpDownComments")
            {
                ListViewFontChange(listViewComments, (float)nud.Value);
            }
            else
            {
                if (nud.Name == "numericUpDownTasks")
                {
                    ListViewFontChange(listViewTask, (float)nud.Value);
                }
                else
                {
                    if (nud.Name == "numericUpDownHistory")
                    {
                        ListViewFontChange(listViewHistory, (float)nud.Value);
                    }
                }
            }
        }

        private void toolStripButtonReports_Click(object sender, EventArgs e)
        {
            toolStripButtonReportsClick?.Invoke(sender, e);
        }

        public void FullScreen()
        {

        }

        public void CloseForm()
        {
            if (InvokeRequired)
                Invoke(new Action(() => Close()));
            else
                Close();
        }

        private void закрытьЗадачиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseTasksForm cf = new CloseTasksForm();
            PCloseTasksForm pcf = new PCloseTasksForm(cf, listViewCloseSelectedTask);
            cf.StartPosition = FormStartPosition.CenterScreen;
            cf.ShowDialog();

            /*
             DialogResult result = MessageBox.Show("Закрыть выбранные задачи?", "Закрытие задач", MessageBoxButtons.YesNo);

            if (result.Equals(DialogResult.Yes))
            {
                //Перебор в выбранном списке и закрытие задач
                listViewCloseSelectedTask?.Invoke(sender, e);
            }*/
        }

        private void ЭкспортExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*DialogResult result = MessageBox.Show("Экспортировать выбранные задачи?", "Экспорт задач в Excel", MessageBoxButtons.YesNo);

            if (result.Equals(DialogResult.Yes))
            {
            }*/
            //Перебор в выбранном списке и экспорт задач
            listViewExportExcelSelectedTask?.Invoke(sender, e);
        }

    private void ЭкспортWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*DialogResult result = MessageBox.Show("Экспортировать выбранные задачи?", "Экспорт задач в Word", MessageBoxButtons.YesNo);

            if (result.Equals(DialogResult.Yes))
            {
            }*/
            //Перебор в выбранном списке и экспорт задач
            listViewExportWordSelectedTask?.Invoke(sender, e);
        }
    }
}
