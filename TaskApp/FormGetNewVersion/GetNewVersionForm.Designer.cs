﻿namespace TaskApp.FormGetNewVersion
{
    partial class GetNewVersionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCurrentVersion = new System.Windows.Forms.TextBox();
            this.textBoxAvailableVersion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonUpdateNow = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(9, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(341, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Доступна обновленная версия Task Manager";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Текущая версия:";
            // 
            // textBoxCurrentVersion
            // 
            this.textBoxCurrentVersion.Location = new System.Drawing.Point(173, 41);
            this.textBoxCurrentVersion.Name = "textBoxCurrentVersion";
            this.textBoxCurrentVersion.ReadOnly = true;
            this.textBoxCurrentVersion.Size = new System.Drawing.Size(100, 20);
            this.textBoxCurrentVersion.TabIndex = 2;
            // 
            // textBoxAvailableVersion
            // 
            this.textBoxAvailableVersion.Location = new System.Drawing.Point(173, 67);
            this.textBoxAvailableVersion.Name = "textBoxAvailableVersion";
            this.textBoxAvailableVersion.ReadOnly = true;
            this.textBoxAvailableVersion.Size = new System.Drawing.Size(100, 20);
            this.textBoxAvailableVersion.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(67, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Доступная версия:";
            // 
            // buttonUpdateNow
            // 
            this.buttonUpdateNow.Location = new System.Drawing.Point(93, 93);
            this.buttonUpdateNow.Name = "buttonUpdateNow";
            this.buttonUpdateNow.Size = new System.Drawing.Size(161, 31);
            this.buttonUpdateNow.TabIndex = 5;
            this.buttonUpdateNow.Text = "Обновить сейчас";
            this.buttonUpdateNow.UseVisualStyleBackColor = true;
            this.buttonUpdateNow.Click += new System.EventHandler(this.buttonUpdateNow_Click);
            // 
            // GetNewVersionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 135);
            this.Controls.Add(this.buttonUpdateNow);
            this.Controls.Add(this.textBoxAvailableVersion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxCurrentVersion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GetNewVersionForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Обновление программы";
            this.Load += new System.EventHandler(this.GetNewVersionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCurrentVersion;
        private System.Windows.Forms.TextBox textBoxAvailableVersion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonUpdateNow;
    }
}