﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormGetNewVersion
{
    public partial class GetNewVersionForm : Form, IGetNewVersionForm
    {
        public GetNewVersionForm()
        {
            InitializeComponent();
        }

        string IGetNewVersionForm.textBoxAvailableVersion
        {
            get { return textBoxAvailableVersion.Text; }
            set { textBoxAvailableVersion.Text = value; }
        }

        string IGetNewVersionForm.textBoxCurrentVersion
        {
            get { return textBoxCurrentVersion.Text; }
            set { textBoxCurrentVersion.Text = value; }
        }

        public event EventHandler<EventArgs> buttonsClick;
        public event EventHandler<EventArgs> formLoad;

        private void GetNewVersionForm_Load(object sender, EventArgs e)
        {
            formLoad?.Invoke(sender, e);
        }

        private void buttonUpdateNow_Click(object sender, EventArgs e)
        {
            buttonsClick?.Invoke(sender, e);
        }

        public void CloseForm()
        {
            //Close();
            Application.Exit();
        }
    }
}
