﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskApp.DBClasses;
using TaskApp.FormAddEvent;

namespace TaskApp.FormEventAcknowledge
{
    public class PEventAcknowledgeForm
    {
        IEventAcknowledgeForm eventAcknowledgeForm;
        MEventAcknowledgeForm mEventAcknowledgeForm;
        PMainForm _pMainForm;
        Task selectedTask;
        Event selectedEvent;
        string formCaption;
        bool buttonOffVisable;

        public PEventAcknowledgeForm(IEventAcknowledgeForm eventAcknowledgeForm, PMainForm pMainForm, Task selectedTask, Event selectedEvent, string formCaption, bool buttonOffVisable)
        {
            this.buttonOffVisable = buttonOffVisable;
            this.eventAcknowledgeForm = eventAcknowledgeForm;
            this.selectedTask = selectedTask;
            this.selectedEvent = selectedEvent;
            _pMainForm = pMainForm;
            mEventAcknowledgeForm = new MEventAcknowledgeForm();
            this.formCaption = formCaption;

            this.eventAcknowledgeForm.formLoad += EventAcknowledgeForm_formLoad;
            this.eventAcknowledgeForm.formActivated += EventAcknowledgeForm_formActivated;
            this.eventAcknowledgeForm.buttonsClick += EventAcknowledgeForm_buttonsClick;

            _pMainForm.model.endUpdateAcknowledge += Model_endUpdateAcknowledge;
        }

        private void EventAcknowledgeForm_formActivated(object sender, EventArgs e)
        {
            eventAcknowledgeForm.textBoxAcknowledgeCommentTextChanged += EventAcknowledgeForm_textBoxAcknowledgeCommentTextChanged;
        }

        private void EventAcknowledgeForm_textBoxAcknowledgeCommentTextChanged(object sender, EventArgs e)
        {
            eventAcknowledgeForm.buttonOKEnable = true;
        }

        private void Model_endUpdateAcknowledge(object sender, EventArgs e)
        {
            eventAcknowledgeForm.formClose();
        }

        private void EventAcknowledgeForm_formLoad(object sender, EventArgs e)
        {
            eventAcknowledgeForm.textBoxTaskId = selectedTask.Id.ToString();
            eventAcknowledgeForm.textBoxTaskTheme = selectedTask.Theme;
            eventAcknowledgeForm.textBoxTaskDescription = selectedTask.Text;
            eventAcknowledgeForm.textBoxEventCreator = selectedEvent.CreatorUser.FullName;
            eventAcknowledgeForm.textBoxEventCreationTime = selectedEvent.CreationTime.ToString();
            eventAcknowledgeForm.textBoxEventTime = selectedEvent.EventTime.ToString();
            eventAcknowledgeForm.textBoxEventType = _pMainForm.model._EventType[selectedEvent.EventType];
            eventAcknowledgeForm.textBoxEventDescription = selectedEvent.EventText;
            var oldComment = selectedEvent.ExecutantUsers.Where(v => v.Value._User.Id.ToString() == _pMainForm.model.CurrentUserID).ToArray();
            if (oldComment.Length > 0)
                eventAcknowledgeForm.textBoxAcknowledgeComment.Text = oldComment[0].Value.AcknowledgeText;

            eventAcknowledgeForm.formText = formCaption;
            eventAcknowledgeForm.buttonCloseAndOffVisable = buttonOffVisable;
        }

        private async void EventAcknowledgeForm_buttonsClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Name == "buttonCancel")
            {
                eventAcknowledgeForm.formClose();
            }
            else
            {
                if (button.Name == "buttonOK")
                {
                    eventAcknowledgeForm.buttonOKEnable = false;
                    eventAcknowledgeForm.textBoxAcknowledgeComment.Enabled = false;

                    string sql = "UPDATE eventUsers SET AcknowledgeTime='" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + "', AcknowledgeComment='" + eventAcknowledgeForm.textBoxAcknowledgeComment.Text.Replace("\'", "\\'").Replace("\"", "\\\"") + "' " +
                        "WHERE EventID='" + selectedEvent.EventID + "' AND UserID='" + _pMainForm.model.CurrentUserID + "'";

                    var v = await _pMainForm.model.AcknowledgeEvent(sql, selectedTask);
                }
                else
                {
                    if (button.Name == "buttonCloseAndOff")
                    {
                        if (!_pMainForm.tempOffEvent.ContainsKey(selectedEvent.EventID))
                        {
                            _pMainForm.tempOffEvent.Add(selectedEvent.EventID, selectedEvent);
                        }
                        eventAcknowledgeForm.formClose();
                    }
                }
            }
        }
    }
}
