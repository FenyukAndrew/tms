﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormEventAcknowledge
{
    public interface IEventAcknowledgeForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        TextBox textBoxAcknowledgeComment { get; set; }
        string textBoxTaskId { get; set; }
        string textBoxTaskTheme { get; set; }
        string textBoxTaskDescription { get; set; }
        string textBoxEventCreator { get; set; }
        string textBoxEventCreationTime { get; set; }
        string textBoxEventTime { get; set; }
        string textBoxEventType { get; set; }
        string textBoxEventDescription { get; set; }
        bool buttonOKEnable { get; set; }
        bool buttonCancelEnable { get; set; }
        bool buttonCloseAndOffVisable { get; set; }
        string formText { get; set; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> formLoad;
        event EventHandler<EventArgs> formActivated;
        event EventHandler<EventArgs> buttonsClick;
        event EventHandler<EventArgs> textBoxAcknowledgeCommentTextChanged;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void formClose();
    }
}
