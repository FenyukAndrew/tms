﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormAddEditTask
{
    public class PAddEditTaskForm
    {
        PMainForm _pMainForm;
        IAddEditTaskForm _addEditTaskForm;
        MAddEditTaskForm maddEditTaskForm;

        public PAddEditTaskForm(AddEditTaskForm addEditTaskForm, PMainForm pMainForm)
        {
            _addEditTaskForm = addEditTaskForm;
            _pMainForm = pMainForm;
            maddEditTaskForm = new MAddEditTaskForm();

            _addEditTaskForm.buttonsClick += _addEditTaskForm_buttonsClick;
            _addEditTaskForm.formLoad += _addEditTaskForm_formLoad;
            _addEditTaskForm.changeService += _addEditTaskForm_changeService;
        }

        const String c_SignatureTask = " [Задача]";

        const String c_SignatureDamage = " [Порча]";
        const String c_SignatureWear = " [Износ]";

        public PAddEditTaskForm(AddEditTaskForm addEditTaskForm, PMainForm pMainForm, Task task)
        {
            _addEditTaskForm = addEditTaskForm;
            _pMainForm = pMainForm;
            maddEditTaskForm = new MAddEditTaskForm(task);
            initializeComboBoxes();

            //Раскодирование checkbox Задача в теме
            String nameTheme = task.Theme;
            if (nameTheme.Contains(c_SignatureTask))
            {
                nameTheme = nameTheme.Replace(c_SignatureTask, "");
                _addEditTaskForm.CheckBoxTaskIncident = true;
            }
            if (nameTheme.Contains(c_SignatureDamage))
            {
                nameTheme = nameTheme.Replace(c_SignatureDamage, "");
                nameTheme = nameTheme.Replace(c_SignatureWear, "");
                _addEditTaskForm.ComboBoxReasonSelectedItem = c_SignatureDamage;
            }
            else
                if (nameTheme.Contains(c_SignatureWear))
                {
                    nameTheme = nameTheme.Replace(c_SignatureDamage, "");
                    nameTheme = nameTheme.Replace(c_SignatureWear, "");
                    _addEditTaskForm.ComboBoxReasonSelectedItem = c_SignatureWear;
                }
            _addEditTaskForm.TextBoxThemeText = nameTheme;

            _addEditTaskForm.RichTextBoxDescriptionText = task.Text;
            _addEditTaskForm.CheckBoxCritical = task.Critical;
            _addEditTaskForm.comboBoxStatusEnabled = true;
            _addEditTaskForm.FormText = "Редактирование задачи ID: " + task.Id;

            _addEditTaskForm.buttonsClick += _addEditTaskForm_buttonsClick;

            _addEditTaskForm.changeService += _addEditTaskForm_changeService;
        }

        private void _addEditTaskForm_changeService(object sender, EventArgs e)
        {
            fillExecutantComboBox();
        }


        private void fillExecutantComboBox()
        {

            if (!_addEditTaskForm.StatusComboboxValue.Equals("Согласование"))
            {
                int maskService = 0xFFFF;//Если не выбран сервис, то отображать всех
                                         //var _service = _pMainForm.model._Service.Where(t => t.Value.Name.Equals(_addEditTaskForm.ServiceComboboxValue)).ToArray();
                var _service = _pMainForm.model._AllService.Where(t => t.Value.Name.Equals(_addEditTaskForm.ServiceComboboxValue)).ToArray();

                if (_service.Length > 0) maskService = (int)Math.Pow(2, _service[0].Value.Id - 1);

                int maskWorkgroup = 0xFFFF;
                var _workGroup = _pMainForm.model._WorkGroup.Where(v => v.Value.Name == _addEditTaskForm.WorkGroupComboboxValue).ToArray();
                if (_workGroup.Length > 0) maskWorkgroup = (int)Math.Pow(2, _workGroup[0].Value.Id - 1);

                int selectedIndex = -1;
                //_addEditTaskForm.ExecutantComboBoxValue = "";
                int realIndex = 0;//Индекс для выбора пользователя с учетом удаленных (невидимых) строк
                for (int i = 0; i < _pMainForm.model._RealExecutantUser.Count; i++)
                {
                    User curUser = _pMainForm.model._RealExecutantUser.ElementAt(i).Value;
                    if (//(!curUser.Delete) &&
                        ((curUser.Services & maskService) != 0) &&
                        ((curUser.Workgroups & maskWorkgroup) != 0)
                        )
                    {
                        _addEditTaskForm.ExecutantComboBoxValue = curUser.FullName;
                        if ((maddEditTaskForm._Task != null) && (maddEditTaskForm._Task.ExecutantUser == curUser))
                        {
                            selectedIndex = realIndex;
                        };
                        realIndex++;
                    }
                }
                _addEditTaskForm.ComboBoxFullNameSelectedIndex = selectedIndex;
            }
            else
            {
                int selectedIndex = -1;
                for (int i = 0; i < _pMainForm.model._UserApproval.Count; i++)
                {
                    User curUser = _pMainForm.model._UserApproval.ElementAt(i).Value;
                   _addEditTaskForm.ExecutantComboBoxValue = curUser.FullName;
                   if ((maddEditTaskForm._Task != null) && (maddEditTaskForm._Task.ExecutantUser == curUser))
                   {
                      selectedIndex = i;
                   };
                }
                _addEditTaskForm.ComboBoxFullNameSelectedIndex = selectedIndex;

            }
        }

        private void initializeComboBoxes()
        {
            int selectedIndex = -1;
            for (int i = 0; i < _pMainForm.model._Status.Count; i++)
            {
                _addEditTaskForm.StatusComboboxValue = _pMainForm.model._Status[i + 1].Name;
                if (maddEditTaskForm._Task._Status.Id == _pMainForm.model._Status[i + 1].Id)
                    selectedIndex = i;
            }
            _addEditTaskForm.ComboBoxStatusSelectedIndex = selectedIndex;
            selectedIndex = -1;

            //var usedServices = _pMainForm.model._Service;//Преведущая фильтрация сервисов
            //МЕНЯТЬ СИНХРОННО С ПОЛУЧЕНИЕМ ДАННЫХ - СМ. ВЫШЕ fillExecutantComboBox() и ниже addEditTaskForm_buttonsClick

            var usedServices = _pMainForm.model._AllService;//Пользователь может создавать инцинденты и задачи на любой сервис
            for (int i = 0; i < usedServices.Count; i++)
            {
                _addEditTaskForm.ServiceComboboxValue = usedServices.ElementAt(i).Value.Name;
                if (maddEditTaskForm._Task._Service != null)
                    if (maddEditTaskForm._Task._Service.Name == usedServices.ElementAt(i).Value.Name)
                        selectedIndex = i;
            }
            _addEditTaskForm.ComboBoxServiceSelectedIndex = selectedIndex;
            selectedIndex = -1;

            for (int i = 0; i < _pMainForm.model._WorkGroup.Count; i++)
            {
                _addEditTaskForm.WorkGroupComboboxValue = _pMainForm.model._WorkGroup.ElementAt(i).Value.Name;
                if (maddEditTaskForm._Task._WorkGroup != null)
                    if (maddEditTaskForm._Task._WorkGroup.Name == _pMainForm.model._WorkGroup.ElementAt(i).Value.Name)
                        selectedIndex = i;
            }
            _addEditTaskForm.ComboBoxWorkGroupSelectedIndex = selectedIndex;

            //Инициализация списка исполнителей зависит от содержимого ComboBoxService и WorkGroupCombobox
            fillExecutantComboBox();
        }

        /// <summary>
        /// Создание новой задачи - все ComboBox не имеют значений по умолчанию
        /// </summary>
        private void _addEditTaskForm_formLoad(object sender, EventArgs e)
        {
            for (int i = 1; i < _pMainForm.model._Status.Count; i++)
            {
                _addEditTaskForm.StatusComboboxValue = _pMainForm.model._Status[i].Name;
            }
            _addEditTaskForm.ComboBoxStatusSelectedIndex = _pMainForm.model._Status.Count > 0 ? 0 : -1;

            //_addEditTaskForm.ExecutantComboBoxValue = "";
            for (int i = 0; i < _pMainForm.model._RealExecutantUser.Count; i++)
            {
                 _addEditTaskForm.ExecutantComboBoxValue = _pMainForm.model._RealExecutantUser.ElementAt(i).Value.FullName;
            }

            /*for (int i = 0; i < _pMainForm.model._Service.Count; i++)
            {
                _addEditTaskForm.ServiceComboboxValue = _pMainForm.model._Service.ElementAt(i).Value.Name;
            }*/

            for (int i = 0; i < _pMainForm.model._AllService.Count; i++)
            {
                _addEditTaskForm.ServiceComboboxValue = _pMainForm.model._AllService.ElementAt(i).Value.Name;
            }

            for (int i = 0; i < _pMainForm.model._WorkGroup.Count; i++)
            {
                _addEditTaskForm.WorkGroupComboboxValue = _pMainForm.model._WorkGroup.ElementAt(i).Value.Name;
            }
        }

        public delegate void DelegateAdd(string theme, string taskText, string creatorUserID, string executantUserID, string status, string service, bool critical, string workGroupId, bool tlgUnswer);
        public delegate void DelegateUpd(string theme, string taskText, string creatorUserID, string executantUserID, string status, string service, bool critical, string workGroupId, Task task, bool tlgUnswer);
        private void _addEditTaskForm_buttonsClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Name == "buttonOk")
            {
                _addEditTaskForm.ComponentsEnabled(false);
                try
                {
                    if (_addEditTaskForm.TextBoxThemeText == "")
                    {
                        MessageBox.Show("Поле \"Тема\" не длолжно быть пустым.");
                        _addEditTaskForm.ComponentsEnabled(true);
                        return;
                    }
                    if (_addEditTaskForm.RichTextBoxDescriptionText == "")
                    {
                        MessageBox.Show("Поле \"Описание задачи\" не должно быть пустым.");
                        _addEditTaskForm.ComponentsEnabled(true);
                        return;
                    }
                    if (_addEditTaskForm.ServiceComboboxValue == "")
                    {
                        MessageBox.Show("Поле \"Сервис\" выберите из списка.");
                        _addEditTaskForm.ComponentsEnabled(true);
                        return;
                    }
                    if (_addEditTaskForm.WorkGroupComboboxValue == "")
                    {
                        MessageBox.Show("Поле \"Рабочая группа\" выберите из списка.");
                        _addEditTaskForm.ComponentsEnabled(true);
                        return;
                    }
                    //проверка выбранного исполнителя более пока не требуется
                    //         if (_addEditTaskForm.ExecutantComboBoxValue == "")
                    //         {
                    //             MessageBox.Show("Поле \"Исполнитель\" выберите из списка.");
                    //             _addEditTaskForm.ComponentsEnabled(true);
                    //             return;
                    //         }
                    var user = _pMainForm.model._User.Where(v => v.Value.FullName == _addEditTaskForm.ExecutantComboBoxValue).ToArray();
                    var service = _pMainForm.model._AllService.Where(v => v.Value.Name == _addEditTaskForm.ServiceComboboxValue).ToArray();
                    var status = _pMainForm.model._Status[_addEditTaskForm.ComboBoxStatusSelectedIndex + 1].Id.ToString();
                    var workGroup = _pMainForm.model._WorkGroup.Where(v => v.Value.Name == _addEditTaskForm.WorkGroupComboboxValue).ToArray();

                    //Кодирование checkbox Задача в теме
                    String nameTheme = _addEditTaskForm.TextBoxThemeText + ((_addEditTaskForm.CheckBoxTaskIncident) ? c_SignatureTask : "");

                    Object selectedReason = _addEditTaskForm.ComboBoxReasonSelectedItem;
                    if (selectedReason != null)
                    {
                        String str = selectedReason.ToString();
                        if (str.Length>0)
                        {
                            nameTheme += " [" + _addEditTaskForm.ComboBoxReasonSelectedItem + "]";
                        }
                    }

                    if (maddEditTaskForm._Task == null)
                    {
                        DelegateAdd del = new DelegateAdd(_pMainForm.model.addTask);
                        System.Threading.Tasks.Task newTask = System.Threading.Tasks.Task.Factory.StartNew(() => del.BeginInvoke(nameTheme,
                            _addEditTaskForm.RichTextBoxDescriptionText,
                            _pMainForm.model.CurrentUserID,
                            user.Count() > 0 ? user[0].Value.Id.ToString() : "",
                            status,
                            service.Length > 0 ? service[0].Value.Id.ToString() : "",
                            _addEditTaskForm.CheckBoxCritical,
                            workGroup.Length > 0 ? workGroup[0].Value.Id.ToString() : "", 
                            _addEditTaskForm.checkBoxInfo, new AsyncCallback(CreateTaskCallBack), null));
                    }
                    else
                    {
                        string s = _addEditTaskForm.RichTextBoxDescriptionText;
                        DelegateUpd del = new DelegateUpd(_pMainForm.model.updTask);

                        System.Threading.Tasks.Task newTask = System.Threading.Tasks.Task.Factory.StartNew(() => del.BeginInvoke(nameTheme,
                            _addEditTaskForm.RichTextBoxDescriptionText,
                            _pMainForm.model.CurrentUserID,
                            user.Count() > 0 ? user[0].Value.Id.ToString() : "",
                            status,
                            service.Length > 0 ? service[0].Value.Id.ToString() : "",
                            _addEditTaskForm.CheckBoxCritical,
                            workGroup.Length > 0 ? workGroup[0].Value.Id.ToString() : "",
                            maddEditTaskForm._Task,
                            _addEditTaskForm.checkBoxInfo, new AsyncCallback(CreateTaskCallBack), null));
                    }
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "addEditTaskForm_buttonsClick.txt");
                    _addEditTaskForm.ComponentsEnabled(true);
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                   
                }
            }
            else
            {
                if (button.Name == "buttonCancel")
                {
                    _addEditTaskForm.CloseForm();
                }
            }
        }

        private void CreateTaskCallBack(IAsyncResult aRes)
        {
            _addEditTaskForm.CloseForm();
        }

        public void ShowForm()
        {
            _addEditTaskForm.ShowForm();
        }
    }
}
