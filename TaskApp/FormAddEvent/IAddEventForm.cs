﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormAddEvent
{
    public interface IAddEventForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ListView listViewUsers { get; set; }
        ListView listViewEvent { get; set; }
        ListView listViewEventUsers { get; set; }
        ComboBox comboBoxType { get; set; }
        TextBox textBoxEvent { get; set; }
        DateTimePicker DateTimePickerEventDate { get; set; }
        DateTimePicker DateTimePickerEventTime { get; set; }
        ContextMenuStrip contextMenuStripEvent { get; set; }


        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> formLoad;
        event EventHandler<EventArgs> buttonsClick;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    }
}
