﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TaskApp.FormEventAcknowledge;

namespace TaskApp.FormAddEvent
{
    public class PAddEventForm
    {
        IAddEventForm addEventForm;
        PMainForm _pMainForm;
        MAddEventForm mAddEventForm;
        private Task SelectedTask { get; set; }

        

        public PAddEventForm(IAddEventForm _addEventForm, PMainForm pMainForm, Task selectedTask)
        {
            addEventForm = _addEventForm;
            _pMainForm = pMainForm;
            this.SelectedTask = selectedTask;
            mAddEventForm = new MAddEventForm();

            addEventForm.formLoad += AddEventForm_formLoad;
            addEventForm.buttonsClick += AddEventForm_buttonsClick;
            addEventForm.listViewEvent.RetrieveVirtualItem += ListViewEvent_RetrieveVirtualItem;
            addEventForm.listViewEvent.DoubleClick += ListViewEvent_DoubleClick;
            addEventForm.listViewEventUsers.RetrieveVirtualItem += ListViewEventUsers_RetrieveVirtualItem;
            addEventForm.listViewEvent.SelectedIndexChanged += ListViewEvent_SelectedIndexChanged;
            addEventForm.contextMenuStripEvent.Opening += ContextMenuStripEvent_Opening;
            addEventForm.contextMenuStripEvent.Click += ContextMenuStripEvent_Click;

            addEventForm.listViewEvent.DrawSubItem += ListViewEvent_DrawSubItem;
            addEventForm.listViewEvent.VirtualListSize = selectedTask.EventList.Count;

            _pMainForm.model.endCreationEvent += Model_endCreationEvent;
            _pMainForm.model.updateEventList += Model_updateEventList;
        }

        
        private void ListViewEvent_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            var time = SelectedTask.EventList.ElementAt(e.ItemIndex).Value.EventTime - DateTime.Now;
            var _event = SelectedTask.EventList.ElementAt(e.ItemIndex).Value;

            if (e.Item.Selected)
                e.DrawDefault = true;
            else
            {
                bool flag = true;
                if (_event.EventType == 0)
                {
                    foreach (var v in _event.ExecutantUsers)
                    {
                        if (v.Value.AcknowledgeText == null || v.Value.AcknowledgeText == "")
                        {
                            flag = false;
                            break;
                        }
                    }
                }
                else
                {
                    var res =_event.ExecutantUsers.Where(v => v.Value.AcknowledgeText != null && v.Value.AcknowledgeText != "").Count();
                    if (res == 0)
                        flag = false;
                }

                using (StringFormat sf = new StringFormat())
                {
                    if (flag)
                    {
                        e.Graphics.FillRectangle(new SolidBrush(Color.LightGray), e.Bounds);
                        e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf);
                    }
                    else
                    {
                        //предупредительный цвет
                        if (time > TimeSpan.Zero && time < _pMainForm.model.BeforeEventTime)
                        {
                            e.Graphics.FillRectangle(new SolidBrush(Color.LightYellow), e.Bounds);
                            e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf);
                        }
                        else
                        {
                            //цвет просроченного события
                            if (time < TimeSpan.Zero)
                            {
                                e.Graphics.FillRectangle(new SolidBrush(Color.Red), e.Bounds);
                                e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf);
                            }
                            else
                            {
                                if (time > _pMainForm.model.BeforeEventTime)
                                {
                                    e.Graphics.FillRectangle(new SolidBrush(Color.LightGreen), e.Bounds);
                                    e.Graphics.DrawString(e.SubItem.Text, ((ListView)sender).Font, new SolidBrush(Color.Black), e.Bounds, sf);
                                }
                                else
                                {
                                    e.DrawDefault = true;
                                }
                            }
                        }
                    }
                }
            }
        }
        
        private void ListViewEvent_DoubleClick(object sender, EventArgs e)
        {
            var selIndexes = addEventForm.listViewEvent.SelectedIndices;
            if (selIndexes.Count > 0)
            {
                if (SelectedTask.EventList.ElementAt(selIndexes[0]).Value.ExecutantUsers.Where(v => v.Key.ToString() == _pMainForm.model.CurrentUserID).ToArray().Length > 0)
                {
                    EventAcknowledgeForm eAF = new EventAcknowledgeForm();
                    PEventAcknowledgeForm pEAF = new PEventAcknowledgeForm(eAF, _pMainForm, SelectedTask, SelectedTask.EventList.ElementAt(selIndexes[0]).Value, "Подтверждение события", false);
                    eAF.StartPosition = FormStartPosition.CenterParent;
                    eAF.ShowDialog();
                }
            }
        }

        private void ContextMenuStripEvent_Click(object sender, EventArgs e)
        {
            ContextMenuStrip contextMenuStrip = (ContextMenuStrip)sender;

            if (contextMenuStrip.Items[0].Selected)
            {
                ListViewEvent_DoubleClick(sender, e);
            }
        }

        private void ContextMenuStripEvent_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (addEventForm.listViewEvent.SelectedIndices.Count > 0)
                ((ContextMenuStrip)sender).Items[0].Enabled = true;
            else
                ((ContextMenuStrip)sender).Items[0].Enabled = false;
        }

        private void Model_updateEventList(object sender, EventArgs e)
        {
//            lock (_pMainForm.model.DictionaryTempTask)
            {
                var val = _pMainForm.model.TaskSnapShot.Where(v => v.Key == SelectedTask.Id).ToArray();

                if (val.Length > 0)
                    SelectedTask = val[0].Value;

                if (addEventForm.listViewEvent.InvokeRequired)
                    addEventForm.listViewEvent.Invoke(new Action(() => addEventForm.listViewEvent.VirtualListSize = SelectedTask.EventList.Count));
                else
                    addEventForm.listViewEvent.VirtualListSize = SelectedTask.EventList.Count;

                addEventForm.listViewEvent.Invalidate();
                addEventForm.listViewEventUsers.Invalidate();
            }
        }
        

        private void ListViewEvent_SelectedIndexChanged(object sender, EventArgs e)
        {
            var selIndexes = ((ListView)sender).SelectedIndices;
            if (selIndexes.Count > 0)
                addEventForm.listViewEventUsers.VirtualListSize = SelectedTask.EventList.ElementAt(selIndexes[0]).Value.ExecutantUsers.Count;
            else
                addEventForm.listViewEventUsers.VirtualListSize = 0;
        }

        private void ListViewEventUsers_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            try
            {
                var v = SelectedTask.EventList.ElementAt(addEventForm.listViewEvent.SelectedIndices[0]).Value;
                var v2 = v.ExecutantUsers.ElementAt(e.ItemIndex);
                ListViewItem lvi = new ListViewItem(new string[] { v2.Value._User.FullName,
                    v2.Value.AcknowledgeTime.ToString(),
                    v2.Value.AcknowledgeText });
                e.Item = lvi;
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "ListViewEventUsers_RetrieveVirtualItem.txt");
            }
        }

        private void ListViewEvent_RetrieveVirtualItem(object sender, RetrieveVirtualItemEventArgs e)
        {
            try
            {
                ListViewItem lvi = new ListViewItem(new string[] { SelectedTask.Id.ToString(),
                    SelectedTask.EventList.ElementAt(e.ItemIndex).Value.CreationTime.ToString(),
                    SelectedTask.EventList.ElementAt(e.ItemIndex).Value.CreatorUser.FullName,
                    SelectedTask.EventList.ElementAt(e.ItemIndex).Value.EventTime.ToString(),
                    SelectedTask.EventList.ElementAt(e.ItemIndex).Value.EventText,
                    _pMainForm.model._EventType[SelectedTask.EventList.ElementAt(e.ItemIndex).Value.EventType] });
                e.Item = lvi;
            }
            catch (Exception ex)
            {
                ErrorEventLog.LogError(ex.Message, "ListViewEvent_RetrieveVirtualItem.txt");
            }
        }

        private void Model_endCreationEvent(object sender, EventArgs e)
        {
            addEventForm.textBoxEvent.Text = "";

            foreach(var item in addEventForm.listViewUsers.CheckedItems)
            {
                ((ListViewItem)item).Checked = false;
            }
        }

        private async void AddEventForm_buttonsClick(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Name == "buttonAdd")
            {
                button.Enabled = false;
                if (addEventForm.textBoxEvent.Text == "")
                {
                    MessageBox.Show("Поле с текстом события не должно быть пустым.");
                    button.Enabled = true;
                    return;
                }
                if (addEventForm.listViewUsers.CheckedItems.Count <= 0)
                {
                    MessageBox.Show("Необходимо выбрать хобя бы одного пользователя.");
                    button.Enabled = true;
                    return;
                }
                if (addEventForm.comboBoxType.SelectedIndex == -1)
                {
                    MessageBox.Show("Необходимо выбрать тип события.");
                    button.Enabled = true;
                    return;
                }

                Dictionary<int, User> checkedUsers = new Dictionary<int, User>();
                foreach(var checkItem in addEventForm.listViewUsers.CheckedItems)
                {
                    var res = _pMainForm.model._User[(int)((ListViewItem)checkItem).Tag];
                    checkedUsers.Add(res.Id, res);
                }

                string eventTime = addEventForm.DateTimePickerEventDate.Value.ToString("yyyy-MM-dd") + " " + addEventForm.DateTimePickerEventTime.Value.ToString("HH:mm:ss");

                var res2 = await _pMainForm.model.addEvent(addEventForm.textBoxEvent.Text, checkedUsers, SelectedTask, eventTime, addEventForm.comboBoxType.SelectedIndex);

                button.Enabled = true;
            }
        }

        private void AddEventForm_formLoad(object sender, EventArgs e)
        {
            foreach(var user in _pMainForm.model._User)
            {
                addEventForm.listViewUsers.Items.Add(new ListViewItem(new string[] { user.Value.FullName, user.Value.Email, user.Value.Phone }));
                addEventForm.listViewUsers.Items[addEventForm.listViewUsers.Items.Count - 1].Tag = user.Key;
            }

            foreach (var val in _pMainForm.model._EventType)
            {
                addEventForm.comboBoxType.Items.Add(val.Value);
            }
        }
    }
}
