﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormProgress.ExportTask
{
    class PExportWordForm
    {
        /*public void HelloWorld(string docName)
        {
            // Create a Wordprocessing document. 
            using (WordprocessingDocument package = WordprocessingDocument.Create(docName, WordprocessingDocumentType.Document))
            {
                // Add a new main document part. 
                package.AddMainDocumentPart();

                // Create the Document DOM. 
                package.MainDocumentPart.Document =
                  new Document(
                    new Body(
                      new Paragraph(
                        new Run(
                          new Text("Hello World!")))));

                // Save changes to the main document part. 
                package.MainDocumentPart.Document.Save();
            }
        }*/

        IProgressForm progressForm;
        delegate void wordExportDelegate();

        WordprocessingDocument wordprocessingDocument;
        Body mainBody;

        bool breakFlag = false;

        List<Task> tasks;

        string fileName;

        //PMainForm pMainForm, 
        public PExportWordForm(IProgressForm progressForm, List<Task> _tasks, string fileName)
        {
            this.fileName = fileName;
            this.progressForm = progressForm;
            this.tasks = _tasks;

            wordprocessingDocument = WordprocessingDocument.Create(fileName, WordprocessingDocumentType.Document);
            // Add a new main document part. 
            wordprocessingDocument.AddMainDocumentPart();

            // Create the Document DOM. 
            wordprocessingDocument.MainDocumentPart.Document =
                  new Document(
                    new Body(
                      /*new Paragraph(
                        new Run(
                          new Text("Hello World!")))*/));
            mainBody = wordprocessingDocument.MainDocumentPart.Document.Body;

            wordExportDelegate sd = WordExport;
            IAsyncResult asyncRes = sd.BeginInvoke(new AsyncCallback(CallbackMethod), null);

            progressForm.buttonsClick += ProgressForm_buttonsClick;
        }

        private void ProgressForm_buttonsClick(object sender, EventArgs e)
        {
            breakFlag = true;
        }

        private void CallbackMethod(IAsyncResult asyncRes)
        {
            try
            {
                if (!breakFlag)
                {
                    // Save changes to the main document part. 
                    wordprocessingDocument.MainDocumentPart.Document.Save();
                    // Close the document.
                    wordprocessingDocument.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            progressForm.FormClose();
        }

        private String ClearString(String text)
        {
            text = text.Replace("\b", "").Replace("\t", " ").Replace("\v", " ").Replace("\f", " ").Replace("\r", " ");
            text = text.Replace("\n\n", "\n");
            text = text.Replace("\n ", "\n");
            text = text.Replace(" \n", "\n");
            text = text.Replace("  ", " ");
            return text;
        }

        private Table createTable()// Create an empty table.
        {
            Table table = new Table();

            // Create a TableProperties object and specify its border information.
            TableProperties tblProp = new TableProperties(
                new TableBorders(
                    new TopBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.BasicThinLines)
                        //,Size = 2 Ширина линии
                    },
                    new BottomBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.BasicThinLines)
                        //,Size = 20
                    },
                    new LeftBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.BasicThinLines)
                        //,Size = 20
                    },
                    new RightBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.BasicThinLines)
                        //,Size = 24
                    },
                    new InsideHorizontalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.BasicThinLines)
                        //,Size = 24
                    },
                    new InsideVerticalBorder()
                    {
                        Val =
                        new EnumValue<BorderValues>(BorderValues.BasicThinLines)
                        //,Size = 24
                    }
                )
            );

            // Append the TableProperties object to the empty table.
            table.AppendChild<TableProperties>(tblProp);

            // Append the table to the document.
            //wordprocessingDocument.MainDocumentPart.Document.Body.
            mainBody.Append(table);

            return table;

        }

        private void AddCell(Table table, String text)
        {
            // Create a row.
            TableRow tr = new TableRow();

            // Create a cell.
            TableCell tc1 = new TableCell();

            // Specify the width property of the table cell.
            tc1.Append(new TableCellProperties(
                //new TableCellWidth() { Type = TableWidthUnitValues.Auto }//, Width = "2400"
                //new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct }
                new TableWidth() { Type = TableWidthUnitValues.Nil } //Занимает весь размер листа
            ));
            /*    
                Nil	No Width. When the item is serialized out as xml, its value is "nil".
                Pct	Width in Fiftieths of a Percent. When the item is serialized out as xml, its value is "pct".
                Dxa	Width in Twentieths of a Point. When the item is serialized out as xml, its value is "dxa".
                Auto	Automatically Determined Width. When the item is serialized out as xml, its value is "auto".
            */

            // Specify the table cell content.
            tc1.Append(new Paragraph(new Run(new Text(text))));

            // Append the table cell to the table row.
            tr.Append(tc1);

            // Append the table row to the table.
            table.Append(tr);
        }

        public void WordExport()
        {
            try
            {
                SectionProperties sectionProps = new SectionProperties();
                //PageSetup pageSetup = new PageSetup() { BlackAndWhite = true; draft = "false"; paperHeight = "1189"; paperWidth = "841"; paperUnits = "mm" };
                //PageSize pageSize = new PageSize() { Width = 0; Height = 0; };
                //sectionProps.Append(pageSize);

                PageMargin pageMargin = new PageMargin() { Top = 333, Right = (UInt32Value)333U, Bottom = 333, Left = (UInt32Value)333U, Header = (UInt32Value)0U, Footer = (UInt32Value)0U, Gutter = (UInt32Value)0U };
                sectionProps.Append(pageMargin);
                mainBody.Append(sectionProps);

                {
                    /*PageMargins pageMargins1 = new PageMargins();
                    pageMargins1.Left = 0.25D;
                    pageMargins1.Right = 0.25D;
                    pageMargins1.Top = 0.25D;
                    pageMargins1.Bottom = 0.25D;
                    pageMargins1.Header = 0.0D;//0.3D=0.8см
                    pageMargins1.Footer = 0.0D;//0.3D
                    worksheetPart.Worksheet.AppendChild(pageMargins1);*/

                    //Если данные для отображения в списке есть, то запускается цикл прорисовки текста и фона ячеек
                    int count = tasks.Count;
                    int i = 0;
                    foreach (Task curTask in tasks)
                    {
                        Table curTable=createTable();

                        {
                            //Задание свойств шрифта - для каждой таблицы нужно делать заново
                            RunProperties runProp = new RunProperties(); // Create run properties.

                            //RunFonts runFont = new RunFonts() { Ascii = "Arial" };// Create font
                            //runProp.Append(runFont);

                            FontSize size = new FontSize() { Val = new StringValue("5") };
                            runProp.Append(size);

                            Run run = new Run();
                            run.PrependChild<RunProperties>(runProp);//Установка свойств шрифта
                            Paragraph paragraph = new Paragraph(run);
                            mainBody.Append(paragraph);
                        }

                        progressForm.progressBar.Invoke(new Action(() => progressForm.progressBar.Value = (i * 100) / count)); i++;
                        if (breakFlag)
                        {
                            break;
                        }
                        else
                        {
                            String text = "Номер: " + curTask.Id.ToString() + "       Дата: " + curTask.CreationDate.ToShortDateString() + "         " + curTask._WorkGroup.Name;
                            AddCell(curTable, text);
                            AddCell(curTable, ClearString(curTask.Theme));
                            AddCell(curTable, ClearString(curTask.Text));

                            foreach(Comment comment in curTask.Comments)
                            {
                                AddCell(curTable, comment._User.FullName + "   " + comment._DateTime.ToShortDateString() + " : " + ClearString(comment.CommentText));
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (breakFlag)
            {
                try
                {
                    // Save changes to the main document part. 
                    wordprocessingDocument.MainDocumentPart.Document.Save();
                    // Close the document.
                    wordprocessingDocument.Close();
                }
                catch { }
            }
        }
    }
}

