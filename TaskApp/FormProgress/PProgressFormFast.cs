﻿using System;
/*using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;*/
using System.Windows.Forms;
/*using System.Drawing;
using System.Reflection;*/

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Linq;

namespace TaskApp.FormProgress
{
    public class PProgressFormFast
    {
        IProgressForm progressForm;
        PMainForm _pMainForm;
        delegate void excelExportDelegate();

        SpreadsheetDocument spreadsheetDocument;
        WorkbookPart workbookpart;
        Sheets sheets;
        WorksheetPart worksheetPart;
        SheetData sheetData;

        bool breakFlag = false;

        ListView[] listViewArr;

        string fileName;

        public PProgressFormFast(IProgressForm progressForm, PMainForm pMainForm, ListView[] listView, string fileName)
        {
            this.fileName = fileName;
            this.progressForm = progressForm;
            _pMainForm = pMainForm;
            this.listViewArr = listView;

            // Create a spreadsheet document by supplying the file name.
            spreadsheetDocument = SpreadsheetDocument.Create(fileName, SpreadsheetDocumentType.Workbook);

            // Add a WorkbookPart to the document.
            workbookpart = spreadsheetDocument.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();

            // Add Sheets to the Workbook.
            sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

            addStyles();

            /*sheets = new Excel.Worksheet[listViewArr.Length];
            application = new Excel.Application();
            workbook = application.Workbooks.Add(Type.Missing);

            workbook.Windows[1].Caption = fileName;*/

            excelExportDelegate sd = ExcelExport;
            IAsyncResult asyncRes = sd.BeginInvoke(new AsyncCallback(CallbackMethod), null);

            progressForm.buttonsClick += ProgressForm_buttonsClick;
        }

        private void ProgressForm_buttonsClick(object sender, EventArgs e)
        {
            breakFlag = true;
        }

        private void CallbackMethod(IAsyncResult asyncRes)
        {
            try
            {
                if (!breakFlag)
                {
                    // Close the document.
                    spreadsheetDocument.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            /*for (int i = 0; i < sheets.Length; i++)
            {
                if (sheets[i] != null)
                Marshal.ReleaseComObject(sheets[i]);
            }
            
            if (workbook != null)
                Marshal.ReleaseComObject(workbook);
            if (application != null)
                Marshal.ReleaseComObject(application);*/
            GC.Collect();
            GC.WaitForPendingFinalizers();
            progressForm.FormClose();
        }

        // Given a Worksheet and an address (like "AZ254"), either return a 
        // cell reference, or create the cell reference and return it.
        /*private Cell InsertCellInWorksheet(Worksheet ws, string addressName)
        {
            UInt32 rowNumber = GetRowIndex(addressName);
            Row row = GetRow(sheetData, rowNumber);

            // If the cell you need already exists, return it.
            // If there is not a cell with the specified column name, insert one.  
            Cell refCell = row.Elements<Cell>().
                Where(c => c.CellReference.Value == addressName).FirstOrDefault();
            if (refCell != null)
            {
                cell = refCell;
            }
            else
            {
                cell = CreateCell(row, addressName);
            }
            return cell;
        }

        // Add a cell with the specified address to a row.
        private Cell CreateCell(Row row, String address)
        {
            Cell cellResult;
            Cell refCell = null;

            // Cells must be in sequential order according to CellReference. 
            // Determine where to insert the new cell.
            foreach (Cell cell in row.Elements<Cell>())
            {
                if (string.Compare(cell.CellReference.Value, address, true) > 0)
                {
                    refCell = cell;
                    break;
                }
            }

            cellResult = new Cell();
            cellResult.CellReference = address;

            row.InsertBefore(cellResult, refCell);
            return cellResult;
        }

        // Return the row at the specified rowIndex located within
        // the sheet data passed in via wsData. If the row does not
        // exist, create it.
        private Row GetRow(SheetData wsData, UInt32 rowIndex)
        {
            var row = wsData.Elements<Row>().
            Where(r => r.RowIndex.Value == rowIndex).FirstOrDefault();
            if (row == null)
            {
                row = new Row();
                row.RowIndex = rowIndex;
                wsData.Append(row);
            }
            return row;
        }

        // Given an Excel address such as E5 or AB128, GetRowIndex
        // parses the address and returns the row index.
        private UInt32 GetRowIndex(string address)
        {
            string rowPart;
            UInt32 l;
            UInt32 result = 0;

            for (int i = 0; i < address.Length; i++)
            {
                if (UInt32.TryParse(address.Substring(i, 1), out l))
                {
                    rowPart = address.Substring(i, address.Length - i);
                    if (UInt32.TryParse(rowPart, out l))
                    {
                        result = l;
                        break;
                    }
                }
            }
            return result;
        }*/

        private static double GetWidth(string font, int fontSize, string text)
        {
            System.Drawing.Font stringFont = new System.Drawing.Font(font, fontSize);
            return GetWidth(stringFont, text);
        }

        private static double GetWidth(System.Drawing.Font stringFont, string text)
        {
            // This formula is based on this article plus a nudge ( + 0.2M )
            // http://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.column.width.aspx
            // Truncate(((256 * Solve_For_This + Truncate(128 / 7)) / 256) * 7) = DeterminePixelsOfString

            System.Drawing.Size textSize = TextRenderer.MeasureText(text, stringFont);
            double width = (double)(((textSize.Width / (double)7) * 256) - (128 / 7)) / 256;
            width = (double)decimal.Round((decimal)width + 0.2M, 2);

            return width;
        }

        struct TextSize
        {
            public TextSize(double _width, double _height) { width = _width; height = _height; }

            public double width;
            public double height;
        }

        static System.Drawing.Size proposedSize = new System.Drawing.Size(int.MaxValue, int.MaxValue);

        //System.Drawing.Font stringFont = new System.Drawing.Font("Calibri", 11);//font, fontSize
        //double width = GetWidth(stringFont, strText);
        private static TextSize GetSizeText(System.Drawing.Font stringFont, string text)
        {
            // This formula is based on this article plus a nudge ( + 0.2M )
            // http://msdn.microsoft.com/en-us/library/documentformat.openxml.spreadsheet.column.width.aspx
            // Truncate(((256 * Solve_For_This + Truncate(128 / 7)) / 256) * 7) = DeterminePixelsOfString

            //TextFormatFlags.SingleLine
            //TextFormatFlags.VerticalCenter
            System.Drawing.Size textSize = TextRenderer.MeasureText(text, stringFont, proposedSize, TextFormatFlags.NoPadding);
            double width = (double)(((textSize.Width / (double)7) * 256) - (128 / 7)) / 256;
            width = (double)decimal.Round((decimal)width + 0.2M, 2);

            return new TextSize(width, textSize.Height);
        }

        void addStyles()
        {
            var stylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            stylesPart.Stylesheet = new Stylesheet();

            // Font list
            // Create a bold font
            stylesPart.Stylesheet.Fonts = new Fonts();
            Font bold_font = new Font();         // Bold font
            Bold bold = new Bold();
            bold_font.Append(bold);

            // Add fonts to list
            stylesPart.Stylesheet.Fonts.AppendChild(new Font());
            stylesPart.Stylesheet.Fonts.AppendChild(bold_font); // Bold gets fontid = 1
            stylesPart.Stylesheet.Fonts.Count = 2;

            // Create fills list
            stylesPart.Stylesheet.Fills = new Fills();

            // create red fill for failed tests
            var formatRed = new PatternFill() { PatternType = PatternValues.Solid };
            formatRed.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("FF5500") }; // red fill
            formatRed.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Create green fill for passed tests
            var formatGreen = new PatternFill() { PatternType = PatternValues.Solid };
            formatGreen.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("99DD00") }; // green fill
            formatGreen.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Create blue fill
            var formatBlue = new PatternFill() { PatternType = PatternValues.Solid };
            formatBlue.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("66AAFF") };
            formatBlue.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Create Light Green fill
            var formatLightGreen = new PatternFill() { PatternType = PatternValues.Solid };
            formatLightGreen.ForegroundColor = new ForegroundColor { Rgb = HexBinaryValue.FromString("F1F8E0") };
            formatLightGreen.BackgroundColor = new BackgroundColor { Indexed = 64 };

            // Append fills to list
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } }); // required, reserved by Excel
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } }); // required, reserved by Excel
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatRed }); // Red gets fillid = 2
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatGreen }); // Green gets fillid = 3
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatBlue }); // Blue gets fillid = 4, old format1
            stylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = formatLightGreen }); // LightGreen gets fillid = 5, old format2
            stylesPart.Stylesheet.Fills.Count = 6;

            // Create border list
            stylesPart.Stylesheet.Borders = new Borders();

            // Create thin borders for passed/failed tests and default cells
            LeftBorder leftThin = new LeftBorder() { Style = BorderStyleValues.Thin };
            RightBorder rightThin = new RightBorder() { Style = BorderStyleValues.Thin };
            TopBorder topThin = new TopBorder() { Style = BorderStyleValues.Thin };
            BottomBorder bottomThin = new BottomBorder() { Style = BorderStyleValues.Thin };

            Border borderThin = new Border();
            borderThin.Append(leftThin);
            borderThin.Append(rightThin);
            borderThin.Append(topThin);
            borderThin.Append(bottomThin);

            // Create thick borders for headings
            LeftBorder leftThick = new LeftBorder() { Style = BorderStyleValues.Thick };
            RightBorder rightThick = new RightBorder() { Style = BorderStyleValues.Thick };
            TopBorder topThick = new TopBorder() { Style = BorderStyleValues.Thick };
            BottomBorder bottomThick = new BottomBorder() { Style = BorderStyleValues.Thick };

            Border borderThick = new Border();
            borderThick.Append(leftThick);
            borderThick.Append(rightThick);
            borderThick.Append(topThick);
            borderThick.Append(bottomThick);

            // Add borders to list
            stylesPart.Stylesheet.Borders.AppendChild(new Border());
            stylesPart.Stylesheet.Borders.AppendChild(borderThin);
            stylesPart.Stylesheet.Borders.AppendChild(borderThick);
            stylesPart.Stylesheet.Borders.Count = 3;


            //Добавление форматов даты
            stylesPart.Stylesheet.NumberingFormats = new NumberingFormats();

            NumberingFormat dateStyle = new NumberingFormat();
            dateStyle.NumberFormatId = UInt32Value.FromUInt32(3);
            dateStyle.FormatCode = StringValue.FromString("yyyy-MM-dd hh:mm:ss");
            stylesPart.Stylesheet.NumberingFormats.Append(dateStyle);


            // Create blank cell format list
            stylesPart.Stylesheet.CellStyleFormats = new CellStyleFormats();
            stylesPart.Stylesheet.CellStyleFormats.Count = 1;
            stylesPart.Stylesheet.CellStyleFormats.AppendChild(new CellFormat());

            // Create cell format list
            stylesPart.Stylesheet.CellFormats = new CellFormats();
            // empty one for index 0, seems to be required
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat());


            // cell format for failed tests, Styleindex = 1, Red fill and bold text
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 2, FillId = 2, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });

            // cell format for passed tests, Styleindex = 2, Green fill and bold text
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 2, FillId = 3, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });

            // cell format for blue background, Styleindex = 3, blue fill and bold text
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 1, FillId = 4, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center });

            // default cell style and rest default, Styleindex = 4
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top });

            // default cell style and rest default, Styleindex = 5
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, WrapText = true });

            // default cell style and rest default, Styleindex = 6 
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { NumberFormatId = dateStyle.NumberFormatId, ApplyNumberFormat = BooleanValue.FromBoolean(true), ApplyFont = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Center, Vertical=VerticalAlignmentValues.Top });

            // default cell style and rest default, Styleindex =7
            stylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat { }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Right, Vertical = VerticalAlignmentValues.Top });

            //stylesPart.Stylesheet.CellFormats.Count = 7;//Работает и так
            stylesPart.Stylesheet.Save();

            /*
            //жирность
            (sheets[l].Cells[2, i] as Excel.Range).Font.Bold = true;
            //размер шрифта
            (sheets[l].Cells[2, i] as Excel.Range).Font.Size = 12;
            //название шрифта
            (sheets[l].Cells[2, i] as Excel.Range).Font.Name = "Times New Roman";
            //цвет текста
            (sheets[l].Cells[2, i] as Excel.Range).Font.Color = Color.White;
            //стиль границы
            (sheets[l].Cells[2, i] as Excel.Range).Borders[Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Excel.XlLineStyle.xlDouble;
            //толщина границы
            (sheets[l].Cells[2, i] as Excel.Range).Borders[Excel.XlBordersIndex.xlEdgeBottom].Weight = Excel.XlBorderWeight.xlMedium;
            //выравнивание по горизонтали
            (sheets[l].Cells[2, i] as Excel.Range).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            (sheets[l].Cells[2, i] as Excel.Range).Interior.Color = Color.DarkBlue;
            //выравнивание по вертикали
            (sheets[l].Cells[2, i] as Excel.Range).VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            (sheets[l].Cells[2, i] as Excel.Range).Borders.Color = Color.LightGray;*/

            // Save the stylesheet formats
            stylesPart.Stylesheet.Save();
        }

        int[] widthColumns = new int[] { 7, 19, 25, 35, 40, 40, 15, 13, 10, 37, 37, 19, 19, 19, 17 };
        Boolean[] flagBestFit = new Boolean[] { true, true, true, false, true, false, true, true, true, true, true, true, true, true, true};

        void addColumns()
        {
            // Create custom widths for columns
            Columns lstColumns = worksheetPart.Worksheet.GetFirstChild<Columns>();
            Boolean needToInsertColumns = false;
            if (lstColumns == null)
            {
                lstColumns = new Columns();
                needToInsertColumns = true;
            }
            //Ширина колонок задается отдельно
            //Min = 1, Max = 7 - задаются номера колонок с по
            // Min = 1, Max = 1 ==> Apply this to column 1 (A)
            // Min = 2, Max = 2 ==> Apply this to column 2 (B)
            // Width = 25 ==> Set the width to 25
            // CustomWidth = true ==> Tell Excel to use the custom width
            /*lstColumns.Append(new Column() { Min = 1, Max = 1, Width = 7, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 2, Max = 2, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 3, Max = 3, Width = 25, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 4, Max = 4, Width = 35, CustomWidth = true, BestFit = false });
            lstColumns.Append(new Column() { Min = 5, Max = 5, Width = 40, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 6, Max = 6, Width = 40, CustomWidth = true, BestFit = false });
            lstColumns.Append(new Column() { Min = 7, Max = 7, Width = 15, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 8, Max = 8, Width = 13, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 9, Max = 9, Width = 10, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 10, Max = 10, Width = 37, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 11, Max = 11, Width = 37, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 12, Max = 12, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 13, Max = 13, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 14, Max = 14, Width = 19, CustomWidth = true, BestFit = true });
            lstColumns.Append(new Column() { Min = 15, Max = 15, Width = 17, CustomWidth = true, BestFit = true });
            */

            for(uint i=0;i<widthColumns.Length; i++)
            {
                uint a = i + 1;
                lstColumns.Append(new Column() { Min = a, Max = a, Width = widthColumns[i], CustomWidth = true, BestFit = flagBestFit[i] });
            }

            // Only insert the columns if we had to create a new columns element
            if (needToInsertColumns)
                worksheetPart.Worksheet.InsertAt(lstColumns, 0);
        }

        //Каждая ячейка вставляется один раз в таблицу - поэтому нет необходимости проверять наличие ячейки или строки
        private Cell CreateCell(Row row, String columnName, UInt32 rowIndex)
        {
            Cell cellResult = new Cell();
            cellResult.CellReference = columnName + rowIndex;

            Cell refCell = null;
            row.InsertBefore(cellResult, refCell);
            return cellResult;
        }
        private Row AddRow(UInt32 rowIndex)
        {
            Row row = new Row();
            row.RowIndex = rowIndex;
            //wsData.Append(row);
            sheetData.Append(row);
            return row;
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;

            while (dividend > 0)
            {
                int mod = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + mod).ToString() + columnName;
                dividend = (int)((dividend - mod) / 26);
            }

            return columnName;
        }

        public void ExcelExport()
        {
            try
            {
                System.Drawing.Font stringFont = new System.Drawing.Font("Calibri", 11);//font, fontSize

                uint sheetId = 1;
                for (int l = 0; l < listViewArr.Length; l++)
                {
                    // Add a WorksheetPart to the WorkbookPart.
                    worksheetPart = workbookpart.AddNewPart<WorksheetPart>();

                    sheetData = new SheetData();
                    worksheetPart.Worksheet = new Worksheet(sheetData);

                    // Get a unique ID for the new worksheet.
                    if (sheets.Elements<Sheet>().Count() > 0)
                    {
                        sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    // Append a new worksheet and associate it with the workbook.
                    Sheet sheet = new Sheet()
                    {
                        Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                        SheetId = sheetId,
                        Name = listViewArr[l].Tag.ToString()
                    };

                    sheets.Append(sheet);

                    addColumns();

                    uint numRow = 2;

                    Row curRow = AddRow(numRow);

                    //double maxWidthDescription = 0;

                    progressForm.labelSheet = listViewArr[l].Tag.ToString();
                    int lvColumnsCount = listViewArr[l].Columns.Count;
                    //Цикл формирования заголовков столбцов
                    for (int i = 1; i < lvColumnsCount + 1; i++)
                    {
                        if (breakFlag)
                        {
                            break;
                        }
                        else
                        {
                            listViewArr[l].Invoke(new Action(() =>
                            {
                                /*Cell cell = CreateCell(curRow, GetExcelColumnName(i), numRow);
                                cell.StyleIndex = 1;
                                cell.DataType = CellValues.InlineString;
                                InlineString inlineString1 = new InlineString();
                                Text text1 = new Text();
                                text1.Text = listViewArr[l].Columns[i - 1].Text;
                                inlineString1.Append(text1);
                                cell.Append(inlineString1);*/

                                string text = listViewArr[l].Columns[i - 1].Text;

                                Cell cell = CreateCell(curRow, GetExcelColumnName(i), numRow);//Номер строки должен совпадать с номером уже созданной строки
                                cell.StyleIndex = (uint)(l+1);//Нумерация стилей начинается с 1
                                cell.DataType = CellValues.String;
                                cell.CellValue = new CellValue(listViewArr[l].Columns[i - 1].Text);

                            }));
                        }
                    }

                    //Если данные для отображения в списке есть, то запускается цикл прорисовки текста и фона ячеек
                    int count = listViewArr[l].Items.Count;

                    for (int i = 0; i < count; i++)
                    {
                        Row useRow = AddRow(++numRow);

                        progressForm.progressBar.Invoke(new Action(() => progressForm.progressBar.Value = (i * 100) / count));
                        if (breakFlag)
                        {
                            break;
                        }
                        else
                        {
                            listViewArr[l].Invoke(new Action(() =>
                            {
                                var msg = listViewArr[l].Items[i];
                                //sheets[l].Cells[i + 3, 1].NumberFormat = "@";
                                //sheets[l].Cells[i + 3, 1] = msg.Text;

                                Cell cell = CreateCell(useRow, GetExcelColumnName(1), numRow);//Номер строки должен совпадать с номером уже созданной строки
                                cell.StyleIndex = (uint)7;
                                cell.DataType = CellValues.Number;
                                cell.CellValue = new CellValue(msg.Text);

                                double max_height = 0;
                                for (int j = 1; j < lvColumnsCount; j++)
                                {
                                    //sheets[l].Cells[i + 3, j + 1].NumberFormat = "@";
                                    //sheets[l].Cells[i + 3, j + 1] = msg.SubItems[j].Text;

                                    cell = CreateCell(useRow, GetExcelColumnName(j+1), numRow);//Номер строки должен совпадать с номером уже созданной строки
                                    cell.StyleIndex = (uint)4;
                                    cell.DataType = CellValues.String;
                                    String text = msg.SubItems[j].Text;
                                    cell.CellValue = new CellValue(text.Replace('\b',' '));

                                    if ((j == 3) || (j == 5) || (j == 8))
                                    {
                                        cell.StyleIndex = (uint)5;
                                        TextSize textSize = GetSizeText(stringFont, text.Replace("\n","").Replace("\r",""));
                                        double k = Math.Ceiling(textSize.width / widthColumns[j]);
                                        double real_height = textSize.height * ( k>1 ? k : 1);
                                        //double real_height = 15 * (k > 1 ? k : 1);
                                        if (max_height < real_height) max_height = real_height;
                                    }

                                    if ((j >= 11) && (j <= 13))
                                    {//Заполнение дат
                                        //cell.DataType = CellValues.Date;//Office2010 only
                                        cell.StyleIndex = (uint)6;
                                    }
                                    if (j == 14)
                                    {
                                        cell.StyleIndex = (uint)7;
                                    }
                                }
                                useRow.Height = max_height;
                                useRow.CustomHeight = true;
                            }));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (breakFlag)
            {
                try
                {
                    // Close the document.
                    spreadsheetDocument.Close();
                }
                catch { }
            }
            /*else
                application.Visible = true;*/
        }
    }
}
