﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormReports
{
    public interface IReportsForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        CheckedListBox checkedListBoxStatus { get; set; }
        CheckedListBox checkedListBoxService { get; set; }
        CheckedListBox checkedListBoxWorkGroup { get; set; }
        CheckedListBox checkedListBoxCreator { get; set; }
        CheckedListBox checkedListBoxExecutant { get; set; }
        TabControl tabControlReport { get; set; }
        Button buttonCreate { get; set; }
        GroupBox gb1 { get; set; }
        GroupBox gb2 { get; set; }
        GroupBox gb3 { get; set; }
        GroupBox gb4 { get; set; }
        ListView listViewReportTask { get; set; }
        ListView listViewReportClosed { get; set; }
        ListView listViewAtWork { get; set; }
        DateTime DateBefore { get; }
        DateTime DateAfter { get; }
        int Critical { get; }
        string textBoxInWork { get; set; }
        string textBoxClosed { get; set; }
        string textBoxComming { get; set; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> buttonsClick;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
