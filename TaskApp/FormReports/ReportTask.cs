﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskApp.DBClasses;

namespace TaskApp.FormReports
{
    public class ReportTask : Task
    {
        public Source _Source { get; set; }
        public string EndUnswer { get; set; }
        private DateTime finishDate;
        public DateTime AcceptedDate { get; set; }

        public DateTime FinishDate
        {
            get { return finishDate; }
            set
            {
                finishDate = value;
                ExecutingTime = value - CreationDate;
            }
        }
        public TimeSpan ExecutingTime { get; set; }
        
        public ReportTask(int id, WorkGroup workGroup, Source source, string theme, Service service, string text, bool critilac, Status status, User creatorUser, User executantUser, DateTime creationDate, List<Comment> comments, List<History> history)
            : base(id, creationDate, theme, text, comments, creatorUser, executantUser, status, history, critilac, service, workGroup)
        {
            Id = id;
            _WorkGroup = workGroup;
            _Source = source;
            Theme = theme;
            _Service = service;
            Text = text;
            Critical = critilac;
            _Status = status;
            CreatorUser = creatorUser;
            ExecutantUser = executantUser;
            CreationDate = creationDate;
    }
    }
}
