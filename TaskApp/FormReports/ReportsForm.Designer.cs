﻿namespace TaskApp.FormReports
{
    partial class ReportsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportsForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControlReport = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listViewAtWork = new System.Windows.Forms.ListView();
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader24 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader25 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader26 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader27 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader28 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader29 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader30 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.listViewFinished = new System.Windows.Forms.ListView();
            this.columnHeader31 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader32 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader33 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader34 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader35 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader36 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader37 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader38 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader39 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader40 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader41 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader42 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader43 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader44 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader45 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.listViewReportCome = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxComming = new System.Windows.Forms.TextBox();
            this.textBoxClosed = new System.Windows.Forms.TextBox();
            this.textBoxInWork = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonExportExcelFast = new System.Windows.Forms.Button();
            this.buttonExportCSV = new System.Windows.Forms.Button();
            this.buttonExportExcel = new System.Windows.Forms.Button();
            this.buttonCreate = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageStatus = new System.Windows.Forms.TabPage();
            this.checkedListBoxStatus = new System.Windows.Forms.CheckedListBox();
            this.tabPageService = new System.Windows.Forms.TabPage();
            this.checkedListBoxService = new System.Windows.Forms.CheckedListBox();
            this.tabPageWorkGroup = new System.Windows.Forms.TabPage();
            this.checkedListBoxWorkGroup = new System.Windows.Forms.CheckedListBox();
            this.tabPageCreator = new System.Windows.Forms.TabPage();
            this.checkedListBoxCreator = new System.Windows.Forms.CheckedListBox();
            this.tabPageExecutant = new System.Windows.Forms.TabPage();
            this.checkedListBoxExecutant = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxCritical = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerEndTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePickerStartTime = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControlReport.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageStatus.SuspendLayout();
            this.tabPageService.SuspendLayout();
            this.tabPageWorkGroup.SuspendLayout();
            this.tabPageCreator.SuspendLayout();
            this.tabPageExecutant.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControlReport);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox5);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox4);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(1459, 627);
            this.splitContainer1.SplitterDistance = 459;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabControlReport
            // 
            this.tabControlReport.Controls.Add(this.tabPage1);
            this.tabControlReport.Controls.Add(this.tabPage2);
            this.tabControlReport.Controls.Add(this.tabPage3);
            this.tabControlReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlReport.Location = new System.Drawing.Point(0, 0);
            this.tabControlReport.Name = "tabControlReport";
            this.tabControlReport.SelectedIndex = 0;
            this.tabControlReport.Size = new System.Drawing.Size(1459, 459);
            this.tabControlReport.TabIndex = 9;
            this.tabControlReport.SelectedIndexChanged += new System.EventHandler(this.tabControlReport_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.listViewAtWork);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1451, 433);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Общее в работе";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // listViewAtWork
            // 
            this.listViewAtWork.AllowColumnReorder = true;
            this.listViewAtWork.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25,
            this.columnHeader26,
            this.columnHeader27,
            this.columnHeader28,
            this.columnHeader29,
            this.columnHeader30});
            this.listViewAtWork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewAtWork.FullRowSelect = true;
            this.listViewAtWork.GridLines = true;
            this.listViewAtWork.Location = new System.Drawing.Point(0, 0);
            this.listViewAtWork.MultiSelect = false;
            this.listViewAtWork.Name = "listViewAtWork";
            this.listViewAtWork.OwnerDraw = true;
            this.listViewAtWork.Size = new System.Drawing.Size(1451, 433);
            this.listViewAtWork.TabIndex = 9;
            this.listViewAtWork.UseCompatibleStateImageBehavior = false;
            this.listViewAtWork.View = System.Windows.Forms.View.Details;
            this.listViewAtWork.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewTask_DrawColumnHeader);
            this.listViewAtWork.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listViewAtWork_DrawSubItem);
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "ID";
            this.columnHeader16.Width = 79;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Назначенная РГ";
            this.columnHeader17.Width = 111;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Источник поступления";
            this.columnHeader18.Width = 128;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Тема";
            this.columnHeader19.Width = 108;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Сервис";
            this.columnHeader20.Width = 98;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Описание";
            this.columnHeader21.Width = 87;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Критичность";
            this.columnHeader22.Width = 86;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Статус";
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Решение";
            this.columnHeader24.Width = 67;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Инициатор";
            this.columnHeader25.Width = 113;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Назначенное лицо";
            this.columnHeader26.Width = 116;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Дата создания";
            this.columnHeader27.Width = 104;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Дата принятия";
            this.columnHeader28.Width = 101;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Дата решения";
            this.columnHeader29.Width = 95;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Время решения";
            this.columnHeader30.Width = 92;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.listViewFinished);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1451, 433);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Общее устранено";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // listViewFinished
            // 
            this.listViewFinished.AllowColumnReorder = true;
            this.listViewFinished.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35,
            this.columnHeader36,
            this.columnHeader37,
            this.columnHeader38,
            this.columnHeader39,
            this.columnHeader40,
            this.columnHeader41,
            this.columnHeader42,
            this.columnHeader43,
            this.columnHeader44,
            this.columnHeader45});
            this.listViewFinished.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewFinished.FullRowSelect = true;
            this.listViewFinished.GridLines = true;
            this.listViewFinished.Location = new System.Drawing.Point(0, 0);
            this.listViewFinished.MultiSelect = false;
            this.listViewFinished.Name = "listViewFinished";
            this.listViewFinished.OwnerDraw = true;
            this.listViewFinished.Size = new System.Drawing.Size(1451, 433);
            this.listViewFinished.TabIndex = 9;
            this.listViewFinished.UseCompatibleStateImageBehavior = false;
            this.listViewFinished.View = System.Windows.Forms.View.Details;
            this.listViewFinished.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewTask_DrawColumnHeader);
            this.listViewFinished.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listViewFinished_DrawSubItem);
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "ID";
            this.columnHeader31.Width = 79;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Назначенная РГ";
            this.columnHeader32.Width = 111;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Источник поступления";
            this.columnHeader33.Width = 128;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "Тема";
            this.columnHeader34.Width = 108;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "Сервис";
            this.columnHeader35.Width = 98;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Описание";
            this.columnHeader36.Width = 87;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Text = "Критичность";
            this.columnHeader37.Width = 86;
            // 
            // columnHeader38
            // 
            this.columnHeader38.Text = "Статус";
            // 
            // columnHeader39
            // 
            this.columnHeader39.Text = "Решение";
            this.columnHeader39.Width = 67;
            // 
            // columnHeader40
            // 
            this.columnHeader40.Text = "Инициатор";
            this.columnHeader40.Width = 113;
            // 
            // columnHeader41
            // 
            this.columnHeader41.Text = "Назначенное лицо";
            this.columnHeader41.Width = 116;
            // 
            // columnHeader42
            // 
            this.columnHeader42.Text = "Дата создания";
            this.columnHeader42.Width = 104;
            // 
            // columnHeader43
            // 
            this.columnHeader43.Text = "Дата принятия";
            this.columnHeader43.Width = 101;
            // 
            // columnHeader44
            // 
            this.columnHeader44.Text = "Дата решения";
            this.columnHeader44.Width = 95;
            // 
            // columnHeader45
            // 
            this.columnHeader45.Text = "Время решения";
            this.columnHeader45.Width = 93;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listViewReportCome);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1451, 433);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Поступившие";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // listViewReportCome
            // 
            this.listViewReportCome.AllowColumnReorder = true;
            this.listViewReportCome.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader7,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader8,
            this.columnHeader14,
            this.columnHeader12,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader15,
            this.columnHeader11,
            this.columnHeader13});
            this.listViewReportCome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewReportCome.FullRowSelect = true;
            this.listViewReportCome.GridLines = true;
            this.listViewReportCome.Location = new System.Drawing.Point(0, 0);
            this.listViewReportCome.MultiSelect = false;
            this.listViewReportCome.Name = "listViewReportCome";
            this.listViewReportCome.OwnerDraw = true;
            this.listViewReportCome.Size = new System.Drawing.Size(1451, 433);
            this.listViewReportCome.TabIndex = 8;
            this.listViewReportCome.UseCompatibleStateImageBehavior = false;
            this.listViewReportCome.View = System.Windows.Forms.View.Details;
            this.listViewReportCome.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewTask_DrawColumnHeader);
            this.listViewReportCome.DrawSubItem += new System.Windows.Forms.DrawListViewSubItemEventHandler(this.listViewReportTask_DrawSubItem);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 79;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Назначенная РГ";
            this.columnHeader2.Width = 111;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Источник поступления";
            this.columnHeader7.Width = 128;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Тема";
            this.columnHeader3.Width = 108;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Сервис";
            this.columnHeader4.Width = 98;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Описание";
            this.columnHeader5.Width = 87;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Критичность";
            this.columnHeader6.Width = 86;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Статус";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Решение";
            this.columnHeader14.Width = 67;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Инициатор";
            this.columnHeader12.Width = 113;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Назначенное лицо";
            this.columnHeader9.Width = 116;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Дата создания";
            this.columnHeader10.Width = 104;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Дата принятия";
            this.columnHeader15.Width = 101;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Дата решения";
            this.columnHeader11.Width = 95;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Время решения";
            this.columnHeader13.Width = 97;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxComming);
            this.groupBox5.Controls.Add(this.textBoxClosed);
            this.groupBox5.Controls.Add(this.textBoxInWork);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Location = new System.Drawing.Point(698, 10);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(187, 93);
            this.groupBox5.TabIndex = 63;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Итого:";
            // 
            // textBoxComming
            // 
            this.textBoxComming.Location = new System.Drawing.Point(120, 66);
            this.textBoxComming.Name = "textBoxComming";
            this.textBoxComming.ReadOnly = true;
            this.textBoxComming.Size = new System.Drawing.Size(58, 20);
            this.textBoxComming.TabIndex = 5;
            this.textBoxComming.Text = "0";
            // 
            // textBoxClosed
            // 
            this.textBoxClosed.Location = new System.Drawing.Point(120, 43);
            this.textBoxClosed.Name = "textBoxClosed";
            this.textBoxClosed.ReadOnly = true;
            this.textBoxClosed.Size = new System.Drawing.Size(58, 20);
            this.textBoxClosed.TabIndex = 4;
            this.textBoxClosed.Text = "0";
            // 
            // textBoxInWork
            // 
            this.textBoxInWork.Location = new System.Drawing.Point(120, 20);
            this.textBoxInWork.Name = "textBoxInWork";
            this.textBoxInWork.ReadOnly = true;
            this.textBoxInWork.Size = new System.Drawing.Size(58, 20);
            this.textBoxInWork.TabIndex = 3;
            this.textBoxInWork.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Поступившие:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Общее устранено:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Общее в работе:";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.buttonExportExcelFast);
            this.groupBox4.Controls.Add(this.buttonExportCSV);
            this.groupBox4.Controls.Add(this.buttonExportExcel);
            this.groupBox4.Controls.Add(this.buttonCreate);
            this.groupBox4.Location = new System.Drawing.Point(1207, 10);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(240, 133);
            this.groupBox4.TabIndex = 62;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Управление:";
            // 
            // buttonExportExcelFast
            // 
            this.buttonExportExcelFast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExportExcelFast.Location = new System.Drawing.Point(131, 53);
            this.buttonExportExcelFast.Name = "buttonExportExcelFast";
            this.buttonExportExcelFast.Size = new System.Drawing.Size(103, 36);
            this.buttonExportExcelFast.TabIndex = 64;
            this.buttonExportExcelFast.Text = "Export Excel Быстро";
            this.buttonExportExcelFast.UseVisualStyleBackColor = true;
            this.buttonExportExcelFast.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonExportCSV
            // 
            this.buttonExportCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExportCSV.Location = new System.Drawing.Point(6, 90);
            this.buttonExportCSV.Name = "buttonExportCSV";
            this.buttonExportCSV.Size = new System.Drawing.Size(228, 36);
            this.buttonExportCSV.TabIndex = 63;
            this.buttonExportCSV.Text = "Export CSV";
            this.buttonExportCSV.UseVisualStyleBackColor = true;
            this.buttonExportCSV.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonExportExcel
            // 
            this.buttonExportExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExportExcel.Location = new System.Drawing.Point(6, 53);
            this.buttonExportExcel.Name = "buttonExportExcel";
            this.buttonExportExcel.Size = new System.Drawing.Size(119, 36);
            this.buttonExportExcel.TabIndex = 62;
            this.buttonExportExcel.Text = "Export Excel";
            this.buttonExportExcel.UseVisualStyleBackColor = true;
            this.buttonExportExcel.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonCreate
            // 
            this.buttonCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCreate.Location = new System.Drawing.Point(6, 16);
            this.buttonCreate.Name = "buttonCreate";
            this.buttonCreate.Size = new System.Drawing.Size(228, 36);
            this.buttonCreate.TabIndex = 61;
            this.buttonCreate.Text = "Сформировать";
            this.buttonCreate.UseVisualStyleBackColor = true;
            this.buttonCreate.Click += new System.EventHandler(this.buttons_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.tabControl1);
            this.groupBox3.Location = new System.Drawing.Point(283, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(409, 142);
            this.groupBox3.TabIndex = 60;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageStatus);
            this.tabControl1.Controls.Add(this.tabPageService);
            this.tabControl1.Controls.Add(this.tabPageWorkGroup);
            this.tabControl1.Controls.Add(this.tabPageCreator);
            this.tabControl1.Controls.Add(this.tabPageExecutant);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 16);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(403, 123);
            this.tabControl1.TabIndex = 57;
            // 
            // tabPageStatus
            // 
            this.tabPageStatus.Controls.Add(this.checkedListBoxStatus);
            this.tabPageStatus.Location = new System.Drawing.Point(4, 22);
            this.tabPageStatus.Name = "tabPageStatus";
            this.tabPageStatus.Size = new System.Drawing.Size(395, 97);
            this.tabPageStatus.TabIndex = 0;
            this.tabPageStatus.Text = "Статус";
            // 
            // checkedListBoxStatus
            // 
            this.checkedListBoxStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxStatus.FormattingEnabled = true;
            this.checkedListBoxStatus.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxStatus.Name = "checkedListBoxStatus";
            this.checkedListBoxStatus.Size = new System.Drawing.Size(395, 97);
            this.checkedListBoxStatus.TabIndex = 1;
            this.checkedListBoxStatus.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageService
            // 
            this.tabPageService.Controls.Add(this.checkedListBoxService);
            this.tabPageService.Location = new System.Drawing.Point(4, 22);
            this.tabPageService.Name = "tabPageService";
            this.tabPageService.Size = new System.Drawing.Size(395, 97);
            this.tabPageService.TabIndex = 1;
            this.tabPageService.Text = "Сервис";
            this.tabPageService.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxService
            // 
            this.checkedListBoxService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxService.FormattingEnabled = true;
            this.checkedListBoxService.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxService.Name = "checkedListBoxService";
            this.checkedListBoxService.Size = new System.Drawing.Size(395, 97);
            this.checkedListBoxService.TabIndex = 1;
            this.checkedListBoxService.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageWorkGroup
            // 
            this.tabPageWorkGroup.Controls.Add(this.checkedListBoxWorkGroup);
            this.tabPageWorkGroup.Location = new System.Drawing.Point(4, 22);
            this.tabPageWorkGroup.Name = "tabPageWorkGroup";
            this.tabPageWorkGroup.Size = new System.Drawing.Size(395, 97);
            this.tabPageWorkGroup.TabIndex = 4;
            this.tabPageWorkGroup.Text = "Рабочие группы";
            this.tabPageWorkGroup.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxWorkGroup
            // 
            this.checkedListBoxWorkGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxWorkGroup.FormattingEnabled = true;
            this.checkedListBoxWorkGroup.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxWorkGroup.Name = "checkedListBoxWorkGroup";
            this.checkedListBoxWorkGroup.Size = new System.Drawing.Size(395, 97);
            this.checkedListBoxWorkGroup.TabIndex = 2;
            this.checkedListBoxWorkGroup.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageCreator
            // 
            this.tabPageCreator.Controls.Add(this.checkedListBoxCreator);
            this.tabPageCreator.Location = new System.Drawing.Point(4, 22);
            this.tabPageCreator.Name = "tabPageCreator";
            this.tabPageCreator.Size = new System.Drawing.Size(395, 97);
            this.tabPageCreator.TabIndex = 2;
            this.tabPageCreator.Text = "Инициатор";
            this.tabPageCreator.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxCreator
            // 
            this.checkedListBoxCreator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxCreator.FormattingEnabled = true;
            this.checkedListBoxCreator.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxCreator.Name = "checkedListBoxCreator";
            this.checkedListBoxCreator.Size = new System.Drawing.Size(395, 97);
            this.checkedListBoxCreator.TabIndex = 1;
            this.checkedListBoxCreator.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // tabPageExecutant
            // 
            this.tabPageExecutant.Controls.Add(this.checkedListBoxExecutant);
            this.tabPageExecutant.Location = new System.Drawing.Point(4, 22);
            this.tabPageExecutant.Name = "tabPageExecutant";
            this.tabPageExecutant.Size = new System.Drawing.Size(395, 97);
            this.tabPageExecutant.TabIndex = 3;
            this.tabPageExecutant.Text = "Исполнитель";
            this.tabPageExecutant.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxExecutant
            // 
            this.checkedListBoxExecutant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxExecutant.FormattingEnabled = true;
            this.checkedListBoxExecutant.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxExecutant.Name = "checkedListBoxExecutant";
            this.checkedListBoxExecutant.Size = new System.Drawing.Size(395, 97);
            this.checkedListBoxExecutant.TabIndex = 0;
            this.checkedListBoxExecutant.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox_ItemCheck);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxCritical);
            this.groupBox2.Location = new System.Drawing.Point(11, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(266, 42);
            this.groupBox2.TabIndex = 59;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Критичность:";
            // 
            // comboBoxCritical
            // 
            this.comboBoxCritical.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCritical.FormattingEnabled = true;
            this.comboBoxCritical.Items.AddRange(new object[] {
            "Все",
            "Не критичные",
            "Критичные"});
            this.comboBoxCritical.Location = new System.Drawing.Point(6, 14);
            this.comboBoxCritical.Name = "comboBoxCritical";
            this.comboBoxCritical.Size = new System.Drawing.Size(250, 21);
            this.comboBoxCritical.TabIndex = 52;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimePickerStartDate);
            this.groupBox1.Controls.Add(this.dateTimePickerEndDate);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dateTimePickerEndTime);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.dateTimePickerStartTime);
            this.groupBox1.Location = new System.Drawing.Point(11, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(266, 73);
            this.groupBox1.TabIndex = 58;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Интервал:";
            // 
            // dateTimePickerStartDate
            // 
            this.dateTimePickerStartDate.Location = new System.Drawing.Point(32, 19);
            this.dateTimePickerStartDate.Name = "dateTimePickerStartDate";
            this.dateTimePickerStartDate.Size = new System.Drawing.Size(140, 20);
            this.dateTimePickerStartDate.TabIndex = 45;
            // 
            // dateTimePickerEndDate
            // 
            this.dateTimePickerEndDate.Location = new System.Drawing.Point(32, 45);
            this.dateTimePickerEndDate.Name = "dateTimePickerEndDate";
            this.dateTimePickerEndDate.Size = new System.Drawing.Size(140, 20);
            this.dateTimePickerEndDate.TabIndex = 49;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 46;
            this.label1.Text = "С:";
            // 
            // dateTimePickerEndTime
            // 
            this.dateTimePickerEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerEndTime.Location = new System.Drawing.Point(178, 45);
            this.dateTimePickerEndTime.Name = "dateTimePickerEndTime";
            this.dateTimePickerEndTime.ShowUpDown = true;
            this.dateTimePickerEndTime.Size = new System.Drawing.Size(78, 20);
            this.dateTimePickerEndTime.TabIndex = 50;
            this.dateTimePickerEndTime.Value = new System.DateTime(2016, 7, 22, 0, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 48;
            this.label2.Text = "По:";
            // 
            // dateTimePickerStartTime
            // 
            this.dateTimePickerStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerStartTime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dateTimePickerStartTime.Location = new System.Drawing.Point(178, 19);
            this.dateTimePickerStartTime.Name = "dateTimePickerStartTime";
            this.dateTimePickerStartTime.ShowUpDown = true;
            this.dateTimePickerStartTime.Size = new System.Drawing.Size(78, 20);
            this.dateTimePickerStartTime.TabIndex = 47;
            this.dateTimePickerStartTime.Value = new System.DateTime(2013, 9, 20, 0, 0, 0, 0);
            // 
            // ReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1459, 627);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReportsForm";
            this.Text = "Отчеты";
            this.Load += new System.EventHandler(this.ReportsForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControlReport.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageStatus.ResumeLayout(false);
            this.tabPageService.ResumeLayout(false);
            this.tabPageWorkGroup.ResumeLayout(false);
            this.tabPageCreator.ResumeLayout(false);
            this.tabPageExecutant.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndTime;
        private System.Windows.Forms.ComboBox comboBoxCritical;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerEndDate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageStatus;
        private System.Windows.Forms.CheckedListBox checkedListBoxStatus;
        private System.Windows.Forms.TabPage tabPageService;
        private System.Windows.Forms.CheckedListBox checkedListBoxService;
        private System.Windows.Forms.TabPage tabPageWorkGroup;
        private System.Windows.Forms.CheckedListBox checkedListBoxWorkGroup;
        private System.Windows.Forms.TabPage tabPageCreator;
        private System.Windows.Forms.CheckedListBox checkedListBoxCreator;
        private System.Windows.Forms.TabPage tabPageExecutant;
        private System.Windows.Forms.CheckedListBox checkedListBoxExecutant;
        private System.Windows.Forms.ListView listViewReportCome;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Button buttonCreate;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonExportCSV;
        private System.Windows.Forms.Button buttonExportExcel;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.TabControl tabControlReport;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ListView listViewAtWork;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView listViewFinished;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBoxComming;
        private System.Windows.Forms.TextBox textBoxClosed;
        private System.Windows.Forms.TextBox textBoxInWork;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonExportExcelFast;
    }
}