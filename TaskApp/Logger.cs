﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskApp
{
    class Logger
    {
        private static ILog log = LogManager.GetLogger("LOGGER");


        public static ILog Log
        {
            get { return log; }
        }

        static Boolean flagStart = false;
        public static void InitLogger()
        {
            if (!flagStart)
            {
                flagStart = true;
                XmlConfigurator.Configure();
            }
        }
    }
}
