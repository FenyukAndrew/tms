﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TaskApp.FormConnectionSettings
{
#pragma warning disable CS0436 // Type conflicts with imported type
    public partial class ConnectionSettingsForm : Form, IConnectionSettingsForm
#pragma warning restore CS0436 // Type conflicts with imported type
    {
        public ConnectionSettingsForm()
        {
            InitializeComponent();
        }

        public TextBox textBoxPassword
        {
            get { return textBoxPass; }
            set { textBoxPass = value; }
        }

        TextBox TaskApp.FormConnectionSettings.IConnectionSettingsForm.textBoxBase
        {
            get { return textBoxBase; }
            set { textBoxBase = value; }
        }

        TextBox IConnectionSettingsForm.textBoxLogin
        {
            get { return textBoxLogin; }
            set { textBoxLogin = value; }
        }

        TextBox IConnectionSettingsForm.textBoxPort
        {
            get { return textBoxPort; }
            set { textBoxPort = value; }
        }

        TextBox IConnectionSettingsForm.textBoxServerName
        {
            get { return textBoxServerName; }
            set { textBoxServerName = value; }
        }

        public event EventHandler<EventArgs> buttonOkClick;
        public event EventHandler<EventArgs> formLoad;

        private void ConnectionSettingsForm_Load(object sender, EventArgs e)
        {
            formLoad?.Invoke(sender, e);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            buttonOkClick?.Invoke(sender, e);
        }

        public void FormClose()
        {
            Close();
        }
        
        private void ConnectionSettingsForm_Activated(object sender, EventArgs e)
        {
            
        }
    }
}
