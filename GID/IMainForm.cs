﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace GID
{
    public interface IMainForm
    {
        //СВОЙСТВА//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        string textBoxServiceText { get; set; }
        string textBoxFinalUserText { get; set; }
        string textBoxWorkGroupText { get; set; }
        string textBoxCommentText { get; set; }
        string textBoxThemeText { get; set; }
        string textBoxDescriptionText { get; set; }
        string labelAllCountText { get; set; }
        string labelErrorCountText { get; set; }
        string toolStripTextBoxInfoText { get; set; }
        string textBoxFullNameText { get; set; }
        string formCaption { set; }
        CheckedListBox checkedListBoxExcelPages { get; set; }
        CheckedListBox checkedListBoxService { get; set; }
        CheckedListBox checkedListBoxWorkGroup { get; set; }
        CheckedListBox checkedListBoxCreator { get; set; }
        ListView listViewData { get; set; }
        bool selectTaskIncident { get; set; }
        bool saveButtonEnabled { get; set; }
        int timeout { get; set; }
        Bitmap buttonLoginImage { get; set; }
        Color buttonAutoCreateBackColor { get; set; }
        ProgressBar progressBarStatistic { get; set; }

        //СОБЫТИЯ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        event EventHandler<EventArgs> formClosing;
        event EventHandler<EventArgs> formLoad;
        event EventHandler<EventArgs> timeoutChanged;
        event EventHandler<EventArgs> saveButtonEnabledEvent;
        event EventHandler<EventArgs> buttonsClick;
        event EventHandler<EventArgs> toolStripButtonClick;
        event EventHandler<EventArgs> contextMenuStripClickItem;
        event EventHandler<EventArgs> contextMenuStripOpening;
        event EventHandler<EventArgs> saveFileDialogEvent;
        event EventHandler<EventArgs> toolStripButtonPropClick;

        //МЕТОДЫ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
