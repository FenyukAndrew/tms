﻿namespace GID
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label11 = new System.Windows.Forms.Label();
            this.buttonAutoCreate = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.progressBarStatistic = new System.Windows.Forms.ProgressBar();
            this.label9 = new System.Windows.Forms.Label();
            this.labelErrorCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelAllCount = new System.Windows.Forms.Label();
            this.textBoxService = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxTheme = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxFinalUser = new System.Windows.Forms.TextBox();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.GroupBoxMainForm = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxFullName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxWorkGroup = new System.Windows.Forms.TextBox();
            this.textBoxComment = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bMCInfoToolStripMenuItem = new System.Windows.Forms.ToolStripSeparator();
            this.создатьИнцидентBMCToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonOpenFile = new System.Windows.Forms.Button();
            this.checkedListBoxExcelPages = new System.Windows.Forms.CheckedListBox();
            this.buttonAll = new System.Windows.Forms.Button();
            this.buttonNone = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.buttonExcel = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageTasks = new System.Windows.Forms.TabPage();
            this.listViewData = new System.Windows.Forms.ListView();
            this.tabPageService = new System.Windows.Forms.TabPage();
            this.checkedListBoxService = new System.Windows.Forms.CheckedListBox();
            this.tabPageWorkGroup = new System.Windows.Forms.TabPage();
            this.checkedListBoxWorkGroup = new System.Windows.Forms.CheckedListBox();
            this.tabPageCreator = new System.Windows.Forms.TabPage();
            this.checkedListBoxCreator = new System.Windows.Forms.CheckedListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numericUpDownTimeout = new System.Windows.Forms.NumericUpDown();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonAutentication = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCSV = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonProp = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBoxInfo = new System.Windows.Forms.ToolStripTextBox();
            this.checkBoxTask = new System.Windows.Forms.CheckBox();
            this.groupBox3.SuspendLayout();
            this.GroupBoxMainForm.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPageTasks.SuspendLayout();
            this.tabPageService.SuspendLayout();
            this.tabPageWorkGroup.SuspendLayout();
            this.tabPageCreator.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeout)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(401, 151);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(85, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Задержка(сек):";
            // 
            // buttonAutoCreate
            // 
            this.buttonAutoCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonAutoCreate.BackColor = System.Drawing.SystemColors.Control;
            this.buttonAutoCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAutoCreate.Location = new System.Drawing.Point(683, 146);
            this.buttonAutoCreate.Name = "buttonAutoCreate";
            this.buttonAutoCreate.Size = new System.Drawing.Size(314, 23);
            this.buttonAutoCreate.TabIndex = 7;
            this.buttonAutoCreate.Text = "Автоматическое создание задач";
            this.buttonAutoCreate.UseVisualStyleBackColor = false;
            this.buttonAutoCreate.Click += new System.EventHandler(this.buttons_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.progressBarStatistic);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.labelErrorCount);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.labelAllCount);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(3, 169);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1001, 28);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // progressBarStatistic
            // 
            this.progressBarStatistic.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarStatistic.Location = new System.Drawing.Point(185, 5);
            this.progressBarStatistic.Name = "progressBarStatistic";
            this.progressBarStatistic.Size = new System.Drawing.Size(808, 23);
            this.progressBarStatistic.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(104, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Ошибок:";
            // 
            // labelErrorCount
            // 
            this.labelErrorCount.AutoSize = true;
            this.labelErrorCount.Location = new System.Drawing.Point(153, 9);
            this.labelErrorCount.Name = "labelErrorCount";
            this.labelErrorCount.Size = new System.Drawing.Size(13, 13);
            this.labelErrorCount.TabIndex = 3;
            this.labelErrorCount.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Всего:";
            // 
            // labelAllCount
            // 
            this.labelAllCount.AutoSize = true;
            this.labelAllCount.Location = new System.Drawing.Point(52, 9);
            this.labelAllCount.Name = "labelAllCount";
            this.labelAllCount.Size = new System.Drawing.Size(13, 13);
            this.labelAllCount.TabIndex = 1;
            this.labelAllCount.Text = "0";
            // 
            // textBoxService
            // 
            this.textBoxService.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxService.Location = new System.Drawing.Point(506, 70);
            this.textBoxService.Name = "textBoxService";
            this.textBoxService.Size = new System.Drawing.Size(169, 20);
            this.textBoxService.TabIndex = 10;
            this.textBoxService.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(456, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Сервис:";
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSave.Enabled = false;
            this.buttonSave.Image = ((System.Drawing.Image)(resources.GetObject("buttonSave.Image")));
            this.buttonSave.Location = new System.Drawing.Point(959, 69);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(28, 22);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttons_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(426, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Исполнитель:";
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxDescription.Location = new System.Drawing.Point(507, 15);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.Size = new System.Drawing.Size(481, 44);
            this.textBoxDescription.TabIndex = 3;
            this.textBoxDescription.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(444, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Описание:";
            // 
            // textBoxTheme
            // 
            this.textBoxTheme.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxTheme.Location = new System.Drawing.Point(63, 18);
            this.textBoxTheme.Multiline = true;
            this.textBoxTheme.Name = "textBoxTheme";
            this.textBoxTheme.Size = new System.Drawing.Size(336, 41);
            this.textBoxTheme.TabIndex = 1;
            this.textBoxTheme.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(23, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Тема:";
            // 
            // textBoxFinalUser
            // 
            this.textBoxFinalUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxFinalUser.Location = new System.Drawing.Point(506, 96);
            this.textBoxFinalUser.Name = "textBoxFinalUser";
            this.textBoxFinalUser.Size = new System.Drawing.Size(169, 20);
            this.textBoxFinalUser.TabIndex = 11;
            this.textBoxFinalUser.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // GroupBoxMainForm
            // 
            this.GroupBoxMainForm.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxMainForm.Controls.Add(this.checkBoxTask);
            this.GroupBoxMainForm.Controls.Add(this.label10);
            this.GroupBoxMainForm.Controls.Add(this.textBoxFullName);
            this.GroupBoxMainForm.Controls.Add(this.label12);
            this.GroupBoxMainForm.Controls.Add(this.textBoxWorkGroup);
            this.GroupBoxMainForm.Controls.Add(this.textBoxComment);
            this.GroupBoxMainForm.Controls.Add(this.label15);
            this.GroupBoxMainForm.Controls.Add(this.textBoxFinalUser);
            this.GroupBoxMainForm.Controls.Add(this.textBoxService);
            this.GroupBoxMainForm.Controls.Add(this.label7);
            this.GroupBoxMainForm.Controls.Add(this.buttonSave);
            this.GroupBoxMainForm.Controls.Add(this.label5);
            this.GroupBoxMainForm.Controls.Add(this.textBoxDescription);
            this.GroupBoxMainForm.Controls.Add(this.label4);
            this.GroupBoxMainForm.Controls.Add(this.textBoxTheme);
            this.GroupBoxMainForm.Controls.Add(this.label3);
            this.GroupBoxMainForm.Location = new System.Drawing.Point(6, 19);
            this.GroupBoxMainForm.Name = "GroupBoxMainForm";
            this.GroupBoxMainForm.Size = new System.Drawing.Size(995, 121);
            this.GroupBoxMainForm.TabIndex = 2;
            this.GroupBoxMainForm.TabStop = false;
            this.GroupBoxMainForm.Text = "Шаблон TMS";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(2, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Заказчик:";
            // 
            // textBoxFullName
            // 
            this.textBoxFullName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxFullName.Enabled = false;
            this.textBoxFullName.Location = new System.Drawing.Point(63, 96);
            this.textBoxFullName.Name = "textBoxFullName";
            this.textBoxFullName.Size = new System.Drawing.Size(336, 20);
            this.textBoxFullName.TabIndex = 30;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(37, 73);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "РГ:";
            // 
            // textBoxWorkGroup
            // 
            this.textBoxWorkGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxWorkGroup.Location = new System.Drawing.Point(63, 70);
            this.textBoxWorkGroup.Name = "textBoxWorkGroup";
            this.textBoxWorkGroup.Size = new System.Drawing.Size(336, 20);
            this.textBoxWorkGroup.TabIndex = 29;
            // 
            // textBoxComment
            // 
            this.textBoxComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textBoxComment.Location = new System.Drawing.Point(763, 70);
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new System.Drawing.Size(190, 20);
            this.textBoxComment.TabIndex = 22;
            this.textBoxComment.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(682, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 21;
            this.label15.Text = "Комментарий:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem,
            this.bMCInfoToolStripMenuItem,
            this.создатьИнцидентBMCToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(157, 54);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // bMCInfoToolStripMenuItem
            // 
            this.bMCInfoToolStripMenuItem.Name = "bMCInfoToolStripMenuItem";
            this.bMCInfoToolStripMenuItem.Size = new System.Drawing.Size(153, 6);
            this.bMCInfoToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // создатьИнцидентBMCToolStripMenuItem
            // 
            this.создатьИнцидентBMCToolStripMenuItem.Name = "создатьИнцидентBMCToolStripMenuItem";
            this.создатьИнцидентBMCToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.создатьИнцидентBMCToolStripMenuItem.Text = "Создать задачу";
            this.создатьИнцидентBMCToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // buttonOpenFile
            // 
            this.buttonOpenFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonOpenFile.Location = new System.Drawing.Point(3, 146);
            this.buttonOpenFile.Name = "buttonOpenFile";
            this.buttonOpenFile.Size = new System.Drawing.Size(91, 23);
            this.buttonOpenFile.TabIndex = 0;
            this.buttonOpenFile.Text = "Открыть файл";
            this.buttonOpenFile.UseVisualStyleBackColor = true;
            this.buttonOpenFile.Click += new System.EventHandler(this.buttons_Click);
            // 
            // checkedListBoxExcelPages
            // 
            this.checkedListBoxExcelPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxExcelPages.FormattingEnabled = true;
            this.checkedListBoxExcelPages.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxExcelPages.Name = "checkedListBoxExcelPages";
            this.checkedListBoxExcelPages.Size = new System.Drawing.Size(186, 557);
            this.checkedListBoxExcelPages.TabIndex = 2;
            // 
            // buttonAll
            // 
            this.buttonAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonAll.Location = new System.Drawing.Point(3, 5);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(71, 23);
            this.buttonAll.TabIndex = 4;
            this.buttonAll.Text = "All";
            this.buttonAll.UseVisualStyleBackColor = true;
            this.buttonAll.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonNone
            // 
            this.buttonNone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonNone.Location = new System.Drawing.Point(112, 5);
            this.buttonNone.Name = "buttonNone";
            this.buttonNone.Size = new System.Drawing.Size(71, 23);
            this.buttonNone.TabIndex = 5;
            this.buttonNone.Text = "None";
            this.buttonNone.UseVisualStyleBackColor = true;
            this.buttonNone.Click += new System.EventHandler(this.buttons_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonLoad.Location = new System.Drawing.Point(100, 146);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(88, 23);
            this.buttonLoad.TabIndex = 6;
            this.buttonLoad.Text = "Загрузить";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttons_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.checkedListBoxExcelPages);
            this.splitContainer1.Panel1.Controls.Add(this.panel2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer1.Panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.splitContainer1_Panel2_Paint);
            this.splitContainer1.Size = new System.Drawing.Size(1197, 588);
            this.splitContainer1.SplitterDistance = 186;
            this.splitContainer1.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.buttonExcel);
            this.panel2.Controls.Add(this.buttonAll);
            this.panel2.Controls.Add(this.buttonNone);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 557);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(186, 31);
            this.panel2.TabIndex = 8;
            // 
            // buttonExcel
            // 
            this.buttonExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonExcel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonExcel.BackgroundImage")));
            this.buttonExcel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonExcel.Location = new System.Drawing.Point(81, 6);
            this.buttonExcel.Name = "buttonExcel";
            this.buttonExcel.Size = new System.Drawing.Size(25, 22);
            this.buttonExcel.TabIndex = 16;
            this.buttonExcel.UseVisualStyleBackColor = true;
            this.buttonExcel.Click += new System.EventHandler(this.buttons_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPageTasks);
            this.tabControl1.Controls.Add(this.tabPageService);
            this.tabControl1.Controls.Add(this.tabPageWorkGroup);
            this.tabControl1.Controls.Add(this.tabPageCreator);
            this.tabControl1.Location = new System.Drawing.Point(2, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(997, 376);
            this.tabControl1.TabIndex = 57;
            // 
            // tabPageTasks
            // 
            this.tabPageTasks.Controls.Add(this.listViewData);
            this.tabPageTasks.Location = new System.Drawing.Point(4, 22);
            this.tabPageTasks.Name = "tabPageTasks";
            this.tabPageTasks.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTasks.Size = new System.Drawing.Size(989, 350);
            this.tabPageTasks.TabIndex = 0;
            this.tabPageTasks.Text = "Excel";
            // 
            // listViewData
            // 
            this.listViewData.ContextMenuStrip = this.contextMenuStrip1;
            this.listViewData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewData.FullRowSelect = true;
            this.listViewData.GridLines = true;
            this.listViewData.Location = new System.Drawing.Point(3, 3);
            this.listViewData.Name = "listViewData";
            this.listViewData.OwnerDraw = true;
            this.listViewData.Size = new System.Drawing.Size(983, 344);
            this.listViewData.TabIndex = 5;
            this.listViewData.UseCompatibleStateImageBehavior = false;
            this.listViewData.View = System.Windows.Forms.View.Details;
            this.listViewData.DrawColumnHeader += new System.Windows.Forms.DrawListViewColumnHeaderEventHandler(this.listViewData_DrawColumnHeader_1);
            // 
            // tabPageService
            // 
            this.tabPageService.Controls.Add(this.checkedListBoxService);
            this.tabPageService.Location = new System.Drawing.Point(4, 22);
            this.tabPageService.Name = "tabPageService";
            this.tabPageService.Size = new System.Drawing.Size(989, 350);
            this.tabPageService.TabIndex = 1;
            this.tabPageService.Text = "Сервис";
            this.tabPageService.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxService
            // 
            this.checkedListBoxService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxService.FormattingEnabled = true;
            this.checkedListBoxService.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxService.Name = "checkedListBoxService";
            this.checkedListBoxService.Size = new System.Drawing.Size(989, 350);
            this.checkedListBoxService.TabIndex = 1;
            // 
            // tabPageWorkGroup
            // 
            this.tabPageWorkGroup.Controls.Add(this.checkedListBoxWorkGroup);
            this.tabPageWorkGroup.Location = new System.Drawing.Point(4, 22);
            this.tabPageWorkGroup.Name = "tabPageWorkGroup";
            this.tabPageWorkGroup.Size = new System.Drawing.Size(989, 350);
            this.tabPageWorkGroup.TabIndex = 4;
            this.tabPageWorkGroup.Text = "Рабочие группы";
            this.tabPageWorkGroup.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxWorkGroup
            // 
            this.checkedListBoxWorkGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxWorkGroup.FormattingEnabled = true;
            this.checkedListBoxWorkGroup.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxWorkGroup.Name = "checkedListBoxWorkGroup";
            this.checkedListBoxWorkGroup.Size = new System.Drawing.Size(989, 350);
            this.checkedListBoxWorkGroup.TabIndex = 2;
            // 
            // tabPageCreator
            // 
            this.tabPageCreator.Controls.Add(this.checkedListBoxCreator);
            this.tabPageCreator.Location = new System.Drawing.Point(4, 22);
            this.tabPageCreator.Name = "tabPageCreator";
            this.tabPageCreator.Size = new System.Drawing.Size(989, 350);
            this.tabPageCreator.TabIndex = 2;
            this.tabPageCreator.Text = "Пользователи";
            this.tabPageCreator.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxCreator
            // 
            this.checkedListBoxCreator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBoxCreator.FormattingEnabled = true;
            this.checkedListBoxCreator.Location = new System.Drawing.Point(0, 0);
            this.checkedListBoxCreator.Name = "checkedListBoxCreator";
            this.checkedListBoxCreator.Size = new System.Drawing.Size(989, 350);
            this.checkedListBoxCreator.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.numericUpDownTimeout);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.buttonAutoCreate);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.GroupBoxMainForm);
            this.groupBox2.Controls.Add(this.buttonLoad);
            this.groupBox2.Controls.Add(this.buttonOpenFile);
            this.groupBox2.Location = new System.Drawing.Point(3, 385);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1007, 200);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Настройка";
            // 
            // numericUpDownTimeout
            // 
            this.numericUpDownTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownTimeout.Location = new System.Drawing.Point(489, 147);
            this.numericUpDownTimeout.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownTimeout.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTimeout.Name = "numericUpDownTimeout";
            this.numericUpDownTimeout.Size = new System.Drawing.Size(49, 20);
            this.numericUpDownTimeout.TabIndex = 15;
            this.numericUpDownTimeout.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownTimeout.ValueChanged += new System.EventHandler(this.numericUpDownTimeout_ValueChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAutentication,
            this.toolStripButtonCSV,
            this.toolStripButtonProp,
            this.toolStripSeparator1,
            this.toolStripTextBoxInfo});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1197, 25);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonAutentication
            // 
            this.toolStripButtonAutentication.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonAutentication.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAutentication.Image")));
            this.toolStripButtonAutentication.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAutentication.Name = "toolStripButtonAutentication";
            this.toolStripButtonAutentication.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonAutentication.Text = "Авторизация";
            this.toolStripButtonAutentication.Click += new System.EventHandler(this.toolStripButtonAutentication_Click);
            // 
            // toolStripButtonCSV
            // 
            this.toolStripButtonCSV.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCSV.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonCSV.Image")));
            this.toolStripButtonCSV.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCSV.Name = "toolStripButtonCSV";
            this.toolStripButtonCSV.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCSV.Text = "Экспорт в csv";
            this.toolStripButtonCSV.Click += new System.EventHandler(this.toolStripButtonCSV_Click);
            // 
            // toolStripButtonProp
            // 
            this.toolStripButtonProp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonProp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonProp.Image")));
            this.toolStripButtonProp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonProp.Name = "toolStripButtonProp";
            this.toolStripButtonProp.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonProp.Text = "Конфигурация";
            this.toolStripButtonProp.Click += new System.EventHandler(this.toolStripButtonProp_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBoxInfo
            // 
            this.toolStripTextBoxInfo.Name = "toolStripTextBoxInfo";
            this.toolStripTextBoxInfo.ReadOnly = true;
            this.toolStripTextBoxInfo.Size = new System.Drawing.Size(250, 25);
            // 
            // checkBoxTask
            // 
            this.checkBoxTask.AutoSize = true;
            this.checkBoxTask.Checked = true;
            this.checkBoxTask.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTask.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxTask.Location = new System.Drawing.Point(685, 98);
            this.checkBoxTask.Name = "checkBoxTask";
            this.checkBoxTask.Size = new System.Drawing.Size(68, 17);
            this.checkBoxTask.TabIndex = 32;
            this.checkBoxTask.Text = "Задача";
            this.checkBoxTask.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1197, 616);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "GID";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.GroupBoxMainForm.ResumeLayout(false);
            this.GroupBoxMainForm.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPageTasks.ResumeLayout(false);
            this.tabPageService.ResumeLayout(false);
            this.tabPageWorkGroup.ResumeLayout(false);
            this.tabPageCreator.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTimeout)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonAutoCreate;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ProgressBar progressBarStatistic;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelErrorCount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelAllCount;
        private System.Windows.Forms.TextBox textBoxService;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxTheme;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxFinalUser;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox GroupBoxMainForm;
        private System.Windows.Forms.TextBox textBoxComment;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьИнцидентBMCToolStripMenuItem;
        private System.Windows.Forms.Button buttonOpenFile;
        private System.Windows.Forms.CheckedListBox checkedListBoxExcelPages;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.Button buttonNone;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonAutentication;
        private System.Windows.Forms.NumericUpDown numericUpDownTimeout;
        private System.Windows.Forms.ToolStripButton toolStripButtonCSV;
        private System.Windows.Forms.ToolStripButton toolStripButtonProp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBoxInfo;
        private System.Windows.Forms.Button buttonExcel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxFullName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxWorkGroup;
        private System.Windows.Forms.ListView listViewData;
        private System.Windows.Forms.ToolStripSeparator bMCInfoToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageTasks;
        private System.Windows.Forms.TabPage tabPageService;
        private System.Windows.Forms.CheckedListBox checkedListBoxService;
        private System.Windows.Forms.TabPage tabPageWorkGroup;
        private System.Windows.Forms.CheckedListBox checkedListBoxWorkGroup;
        private System.Windows.Forms.TabPage tabPageCreator;
        private System.Windows.Forms.CheckedListBox checkedListBoxCreator;
        private System.Windows.Forms.CheckBox checkBoxTask;
    }
}

