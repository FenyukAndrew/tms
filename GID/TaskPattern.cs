﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskApp;

namespace GID
{

    //Шаблон инцидента 
    [Serializable]
    public class TaskPattern
    {
        private string theme;
        private string description;
        private string service;
        private string executant;
        private string workGroup;
        private string atachComment;

        /// <summary>
        /// Комментарий к вложению
        /// </summary>
        public string AtachComment
        {
            get { return atachComment; }
            set { atachComment = value; }
        }
        
        public string _WorkGroup
        {
            get { return workGroup; }
            set { workGroup = value; }
        }
        
        /// <summary>
        /// Тема инцидента
        /// </summary>
        public string Theme
        {
            get { return theme; }
            set { theme = value; }
        }

        /// <summary>
        /// Описание инцидента
        /// </summary>
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        /// <summary>
        /// Сервис инцидента
        /// </summary>
        public string _Service
        {
            get { return service; }
            set { service = value; }
        }

        /// <summary>
        /// Исполнитель инцидента
        /// </summary>
        public string Executant
        {
            get { return executant; }
            set { executant = value; }
        }
        
        public string Timeout { get; set; }

        public TaskPattern()
        {
            //Стандартный порядок полей при импорте
            theme = "{0}";
            description = "{1}";
            workGroup = "{2}";
            service = "{3}";
            executant = "{4}";
            atachComment = "{5}";
    }
}
    
}
