﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;
using TaskApp;
using TaskApp.FormConnectionSettings;
using static System.Windows.Forms.ListViewItem;

namespace GID
{
    public class PMainForm
    {
        public IMainForm _mainForm { get; set; }
        public MMainForm model { get; set; }

        public PMainForm(MainForm mainForm)
        {
            _mainForm = mainForm;
            model = new MMainForm();

            model.updateLists += Model_updateLists;
            model.updateItemText += Model_updateItemText;

            _mainForm.toolStripButtonClick += _mainForm_toolStripButtonClick;
            _mainForm.formLoad += _mainForm_formLoad;
            _mainForm.formClosing += _mainForm_formClosing;
            _mainForm.buttonsClick += _mainForm_buttonsClick;
            _mainForm.saveButtonEnabledEvent += _mainForm_saveButtonEnabledEvent;
            _mainForm.timeoutChanged += _mainForm_timeoutChanged;
            _mainForm.contextMenuStripClickItem += _mainForm_contextMenuStripClickItem;
            _mainForm.contextMenuStripOpening += _mainForm_contextMenuStripOpening;
            _mainForm.saveFileDialogEvent += _mainForm_saveFileDialogEvent;
            _mainForm.listViewData.SelectedIndexChanged += ListViewData_SelectedIndexChanged;
            _mainForm.listViewData.DrawSubItem += ListViewData_DrawSubItem;
            _mainForm.checkedListBoxService.SelectedIndexChanged += CheckedListBoxService_SelectedIndexChanged;
            _mainForm.checkedListBoxWorkGroup.SelectedIndexChanged += CheckedListBoxService_SelectedIndexChanged;
            _mainForm.checkedListBoxCreator.SelectedIndexChanged += CheckedListBoxService_SelectedIndexChanged;
        }

        private void Model_updateItemText(object sender, ExtendedEventArgs e)
        {
            ListViewItem item = (ListViewItem)sender;
            if (_mainForm.listViewData.InvokeRequired)
                _mainForm.listViewData.Invoke(new Action(() => item.Text = item.Text  + e.Text));
            else
                item.Text = item.Text + e.Text;
        }

        private void CheckedListBoxService_SelectedIndexChanged(object sender, EventArgs e)
        {
            _mainForm.toolStripTextBoxInfoText = ((CheckedListBox)sender).SelectedItem.ToString();
        }

        private void Model_updateLists(object sender, EventArgs e)
        {
            foreach (var item in model._Service)
            {
                _mainForm.checkedListBoxService.Items.Add(item.Value.Name);
            }

            foreach(var item in model._WorkGroup)
            {
                _mainForm.checkedListBoxWorkGroup.Items.Add(item.Value.Name);
            }

            foreach (var item in model._User)
            {
                _mainForm.checkedListBoxCreator.Items.Add(item.Value.FullName);
            }
        }

        private void ListViewData_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void ListViewData_SelectedIndexChanged(object sender, EventArgs e)
        {
            var listView = (ListView)sender;
        }

        private void _mainForm_saveFileDialogEvent(object sender, EventArgs e)
        {
            model.ExportCSV(_mainForm.listViewData, ((SaveFileDialog)sender).FileName + ".csv");
        }

        private void _mainForm_contextMenuStripOpening(object sender, EventArgs e)
        {
            if (_mainForm.listViewData.SelectedIndices.Count == 0)
            {
                ((ContextMenuStrip)sender).Items[0].Enabled = false;
                ((ContextMenuStrip)sender).Items[1].Enabled = false;
                ((ContextMenuStrip)sender).Items[2].Enabled = false;
            }
            else
            {
                ((ContextMenuStrip)sender).Items[0].Enabled = true;
                ((ContextMenuStrip)sender).Items[1].Enabled = true;
                ((ContextMenuStrip)sender).Items[2].Enabled = true;
            }
        }

        private void _mainForm_contextMenuStripClickItem(object sender, EventArgs e)
        {
            if (sender.ToString() == "Удалить")
            {
                while (_mainForm.listViewData.SelectedItems.Count > 0)
                {
                    _mainForm.listViewData.Items.Remove(_mainForm.listViewData.SelectedItems[0]);
                }
            }
            else
            {
                if (sender.ToString() == "Создать задачу")
                {
                    if (model.CurrentUser != "")
                    {
                        if (_mainForm.textBoxThemeText.Length >= 3 && _mainForm.textBoxDescriptionText.Length >= 3 && _mainForm.textBoxFullNameText.Length >= 3)
                        {
                            if (_mainForm.textBoxThemeText[0] == '{' && _mainForm.textBoxDescriptionText[0] == '{')
                            {
                                if ((_mainForm.textBoxCommentText == "" || _mainForm.textBoxCommentText.Length >= 3 && _mainForm.textBoxCommentText[0] == '{') &&
                                    (_mainForm.textBoxServiceText == "" || _mainForm.textBoxServiceText.Length >= 3 && _mainForm.textBoxServiceText[0] == '{') &&
                                    (_mainForm.textBoxWorkGroupText == "" || _mainForm.textBoxWorkGroupText.Length >= 3 && _mainForm.textBoxWorkGroupText[0] == '{') &&
                                    (_mainForm.textBoxFinalUserText == "" || _mainForm.textBoxFinalUserText.Length >= 3 && _mainForm.textBoxFinalUserText[0] == '{'))
                                {
                                    var taskPattetn = model.GetTaskPatternFromSelectedListViewItem(_mainForm.listViewData);
                                    var task = model.GetTaskFromTaskPattern(taskPattetn, _mainForm.listViewData.SelectedItems[0]);
                                    if (task != null)
                                        model.CreateTask(task, _mainForm.listViewData.SelectedItems[0]);
                                }
                                else
                                {
                                    MessageBox.Show(model.tamplateFieldsErrorMessage);
                                }
                            }
                            else
                            {
                                MessageBox.Show(model.tamplateRedFieldsErrorMessage);
                            }
                        }
                        else
                        {
                            MessageBox.Show(model.tamplateRedFieldsErrorMessage);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Пользователь от имени которого запущено приложение не определен. В окне авторизации введите корректные данные и повторите попытку.");
                    }
                }
            }
        }
        

        private void _mainForm_timeoutChanged(object sender, EventArgs e)
        {
            model._TaskPattern.Timeout = ((int)sender * 1000).ToString();
            _mainForm.saveButtonEnabled = true;
        }

        private void _mainForm_saveButtonEnabledEvent(object sender, EventArgs e)
        {
            _mainForm.saveButtonEnabled = (((bool)sender));
        }

        private void _mainForm_buttonsClick(object sender, EventArgs e)
        {
            if (((Button)sender).Name == "buttonOpenFile")
            {
                OpenExcelFile();
            }
            else
            {
                if (((Button)sender).Name == "buttonLoad")
                {
                    LoadExcelPages();
                }
                else
                {
                    if (((Button)sender).Name == "buttonAll")
                    {
                        for (int i = 0; i < _mainForm.checkedListBoxExcelPages.Items.Count; i++)
                        {
                            _mainForm.checkedListBoxExcelPages.SetItemChecked(i, true);
                        }
                    }
                    else
                    {
                        if (((Button)sender).Name == "buttonNone")
                        {
                            for (int i = 0; i < _mainForm.checkedListBoxExcelPages.Items.Count; i++)
                            {
                                _mainForm.checkedListBoxExcelPages.SetItemChecked(i, false);
                            }
                        }
                        else
                        {
                            if (((Button)sender).Name == "buttonAutoCreate")
                            {
                                AutocreateButtonPress();
                            }
                            else
                            {
                                if (((Button)sender).Name == "buttonSave")
                                {
                                    model._TaskPattern.AtachComment = _mainForm.textBoxCommentText;
                                    model._TaskPattern.Description = _mainForm.textBoxDescriptionText;
                                    model._TaskPattern.Executant = _mainForm.textBoxFinalUserText;
                                    model._TaskPattern.Timeout = _mainForm.timeout.ToString();
                                    model._TaskPattern.Theme = _mainForm.textBoxThemeText;
                                    model._TaskPattern._Service = _mainForm.textBoxServiceText;
                                    model._TaskPattern._WorkGroup = _mainForm.textBoxWorkGroupText;

                                    XmlSerializer xml = new XmlSerializer(typeof(TaskPattern));
                                    using (var fStream = new FileStream(Environment.CurrentDirectory + "\\TaskPattern.xml", FileMode.Create, FileAccess.Write, FileShare.None))
                                    {
                                        xml.Serialize(fStream, model._TaskPattern);
                                    }

                                    _mainForm.saveButtonEnabled = false;
                                }
                                else
                                {
                                    if (((Button)sender).Name == "buttonExcel")
                                    {
                                        if (model.Filename != null)
                                            try
                                            {
                                                Process.Start(model.Filename);
                                            }
                                            catch(Exception ex)
                                            {
                                                MessageBox.Show(ex.Message + Environment.NewLine + "Попробуйте ещё раз.");
                                            }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        delegate void Delegate(ListView lv, ProgressBar pb, int timeout, bool flagTask);
        private void AutocreateButtonPress()
        {
            //Если автоматический режим включен, выключить иначе включить.
            if (_mainForm.buttonAutoCreateBackColor == Color.LightGreen)
            {
                _mainForm.buttonAutoCreateBackColor = Color.Silver;
                MMainForm.AutoModeIndicator = false;
            }
            else
            {
                MMainForm.AutoModeIndicator = true;
                _mainForm.buttonAutoCreateBackColor = Color.LightGreen;
                Delegate del = new Delegate(model.AutoCreateIncidents);
                System.Threading.Tasks.Task newTask = System.Threading.Tasks.Task.Factory.StartNew(() => del.BeginInvoke(_mainForm.listViewData, _mainForm.progressBarStatistic, _mainForm.timeout, _mainForm.selectTaskIncident , new AsyncCallback(AutoCreateCallBackFunc), null));
            }
        }

        //колбак метод обновляющий статистику
        private void AutoCreateCallBackFunc(IAsyncResult aRes)
        {
            try
            {
                _mainForm.progressBarStatistic.Invoke(new System.Action(() =>
                {
                    _mainForm.progressBarStatistic.Value = 0;
                    _mainForm.buttonAutoCreateBackColor = Color.Silver;
                    MMainForm.AutoModeIndicator = false;
                }));
            }
            catch
            {

            }
        }

        private void LoadExcelPages()
        {
            if (model.myOleDbConnection != null)
            {
                _mainForm.labelErrorCountText = "0";
                if (model.myOleDbConnection.State == ConnectionState.Closed)
                    model.myOleDbConnection.Open();

                int rowsCount = 0;
                _mainForm.listViewData.Clear();

                for (int i = 0; i < _mainForm.checkedListBoxExcelPages.CheckedItems.Count; i++)
                {
                    string listName = _mainForm.checkedListBoxExcelPages.CheckedItems[i].ToString();

                    //Задаем команду выборки данных
                    string select = String.Format("SELECT * FROM [{0}]", listName);

                    // Выбираем все данные с листа
                    model.myOleDbDataAdapter = new OleDbDataAdapter(select, model.myOleDbConnection);

                    //Связываем данные из файла с датасетом
                    DataSet myDataSet = new DataSet("EXCEL");

                    try
                    {
                        model.myOleDbDataAdapter.Fill(myDataSet);


                        if (myDataSet.Tables.Count > 0)
                        {
                            if (myDataSet.Tables[0].Rows.Count > 0)
                            {
                                if (myDataSet.Tables[0].Columns.Count > 0)
                                {
                                    if (_mainForm.listViewData.Columns.Count == 0)
                                        for (int c = 0; c < myDataSet.Tables[0].Columns.Count; c++)
                                        {
                                            _mainForm.listViewData.Columns.Add(myDataSet.Tables[0].Rows[i][c].ToString() + "{" + c.ToString() + "}");
                                            _mainForm.listViewData.Columns[_mainForm.listViewData.Columns.Count - 1].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                                            if (_mainForm.listViewData.Columns[_mainForm.listViewData.Columns.Count - 1].Width > 300)
                                                _mainForm.listViewData.Columns[_mainForm.listViewData.Columns.Count - 1].Width = 300;
                                        }

                                    for (int j = 1; j < myDataSet.Tables[0].Rows.Count; j++)
                                    {
                                        ListViewItem lvi = new ListViewItem();
                                        for (int c = 0; c < myDataSet.Tables[0].Columns.Count; c++)
                                        {
                                            if (c == 0)
                                                lvi.Text = myDataSet.Tables[0].Rows[j][0].ToString();
                                            else
                                                lvi.SubItems.Add(myDataSet.Tables[0].Rows[j][c].ToString());
                                        }
                                        if (lvi.Text != "")
                                            _mainForm.listViewData.Items.Add(lvi);
                                        rowsCount++;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorEventLog.LogError(ex.Message, "LoadExcelPages.txt");
                        _mainForm.labelErrorCountText = (int.Parse(_mainForm.labelErrorCountText) + 1).ToString();
                    }

                }

                for (int ix = 0; ix < _mainForm.listViewData.Items.Count; ++ix)
                {
                    var item = _mainForm.listViewData.Items[ix];
                    item.BackColor = (ix % 2 == 0) ? Color.White : Color.LightGray;
                    foreach (var v in item.SubItems)
                    {
                        ((ListViewSubItem)v).BackColor = (ix % 2 == 0) ? Color.White : Color.LightGray;
                    }
                }

                if (model.myOleDbConnection.State == ConnectionState.Open)
                    model.myOleDbConnection.Close();

                _mainForm.labelAllCountText = rowsCount.ToString();
            }
        }

        private void OpenExcelFile()
        {
            OpenFileDialog opfd = new OpenFileDialog();
            DialogResult dr = opfd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    _mainForm.checkedListBoxExcelPages.Items.Clear();

                    String extension = opfd.FileName.Substring(opfd.FileName.Length - 3, 3);
                    if ((extension == "csv") || (extension == "txt"))
                    {
                        List<string[]> res = MMainForm.ReadFile(opfd.FileName);

                        _mainForm.listViewData.Clear();
                        for (int c = 0; c < res[0].Length; c++)
                        {
                            _mainForm.listViewData.Columns.Add(res[0][c]);
                            _mainForm.listViewData.Columns[_mainForm.listViewData.Columns.Count - 1].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
                            if (_mainForm.listViewData.Columns[_mainForm.listViewData.Columns.Count - 1].Width > 300)
                                _mainForm.listViewData.Columns[_mainForm.listViewData.Columns.Count - 1].Width = 300;
                        }

                        for (int i = 1; i < res.Count; i++)
                        {
                            ListViewItem lvi = new ListViewItem();
                            for (int j = 0; j < res[i].Length; j++)
                            {
                                if (j == 0)
                                    lvi.Text = res[i][j];
                                else
                                {
                                    lvi.SubItems.Add(res[i][j]);
                                }
                            }

                            if (res[i][res[i].Length - 1] == "1;" | res[i][res[i].Length - 1] == "11;")
                            {
                                lvi.Tag = 1;
                            }
                            _mainForm.listViewData.Items.Add(lvi);
                        }

                    }
                    else
                    {
                        if (opfd.FileName.Substring(opfd.FileName.Length - 3, 3) == "xls")
                        {
                            model.LoadFile(opfd.FileName);
                            model.GetData(_mainForm.checkedListBoxExcelPages);
                        }
                        else
                        {
                            MessageBox.Show("Неизвестный формат файла!");
                        }
                    }

                    model.Filename = opfd.FileName;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

      

        private void _mainForm_formClosing(object sender, EventArgs e)
        {
            model.SaveConnectionjSettings();
        }

        private void _mainForm_formLoad(object sender, EventArgs e)
        {
            
            _mainForm.formCaption = "Гид v." + model.GetProgramVersion();
           


            XmlSerializer serializer2 = new XmlSerializer(typeof(TaskPattern));
            if (File.Exists(Environment.CurrentDirectory + "\\TaskPattern.xml"))
            {
                try
                {
                    using (StreamReader reader = new StreamReader(Environment.CurrentDirectory + "\\TaskPattern.xml"))
                    {
                        model._TaskPattern = (TaskPattern)serializer2.Deserialize(reader);
                    }

                    _mainForm.timeout = int.Parse(model._TaskPattern.Timeout == null ? "1" : model._TaskPattern.Timeout);
                }
                catch (Exception ex)
                {
                    ErrorEventLog.LogError(ex.Message, "mainForm_formLoad.txt");
                    MessageBox.Show("Файл шаблона поврежден. Заполните все обязательные поля и сохраните изменения. " + Environment.NewLine + ex.Message);
                    model._TaskPattern = new TaskPattern();
                }
            }
            else
            {
                model._TaskPattern = new TaskPattern();
            }

            try
            {
                _mainForm.textBoxThemeText = model._TaskPattern.Theme.Replace("\n", Environment.NewLine);
                _mainForm.textBoxDescriptionText = model._TaskPattern.Description.Replace("\n", Environment.NewLine);
                _mainForm.textBoxFinalUserText = model._TaskPattern.Executant.Replace("\n", Environment.NewLine);
                _mainForm.textBoxServiceText = model._TaskPattern._Service;
                _mainForm.textBoxCommentText = model._TaskPattern.AtachComment.Replace("\n", Environment.NewLine);
                _mainForm.textBoxWorkGroupText = model._TaskPattern._WorkGroup.Replace("\n", Environment.NewLine);
            }
            catch
            { };

            model.GetConnectionSettings();

            _mainForm.textBoxFullNameText = model._ConnectionSettings.LoginName;

                      
            _mainForm.saveButtonEnabled = false;
        }

        private void _mainForm_toolStripButtonClick(object sender, EventArgs e)
        {
            if (((ToolStripButton)sender).Name == "toolStripButtonAutentication")
            {
                ConnectionSettingsForm connectionSettingsForm = new ConnectionSettingsForm();
                PConnectionSettingsForm pConnectionSettingsForm = new PConnectionSettingsForm(connectionSettingsForm, this);
                connectionSettingsForm.StartPosition = FormStartPosition.CenterScreen;
                connectionSettingsForm.ShowDialog();
            }
        }

       
    }
}
