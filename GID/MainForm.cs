﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GID
{
    public partial class MainForm : Form, IMainForm
    {
        public string textBoxServiceText
        {
            get { return textBoxService.Text; }
            set { textBoxService.Text = value; }
        }

        public string textBoxFinalUserText
        {
            get { return textBoxFinalUser.Text; }
            set { textBoxFinalUser.Text = value; }
        }

        public string textBoxWorkGroupText
        {
            get { return textBoxWorkGroup.Text; } 
            set { textBoxWorkGroup.Text = value; }
        }
                
        public string textBoxCommentText
        {
            get { return textBoxComment.Text; }
            set { textBoxComment.Text = value; }
        }

        public string textBoxThemeText
        {
            get { return textBoxTheme.Text; }
            set { textBoxTheme.Text = value; }
        }

        public string textBoxDescriptionText
        {
            get { return textBoxDescription.Text; }
            set { textBoxDescription.Text = value; }
        }
        
        CheckedListBox IMainForm.checkedListBoxExcelPages
        {
            get { return checkedListBoxExcelPages; }
            set { checkedListBoxExcelPages = value; }
        }

        ListView IMainForm.listViewData
        {
            get { return listViewData; }
            set { listViewData = value; }
        }

        public bool selectTaskIncident
        {
            get { return checkBoxTask.Checked; }
            set { checkBoxTask.Checked = value; }
        }

        public bool saveButtonEnabled
        {
            get { return buttonSave.Enabled; }
            set { buttonSave.Enabled = value; }
        }

        public int timeout
        {
            get { return (int)numericUpDownTimeout.Value; }
            set { numericUpDownTimeout.Value = value; }
        }

        public string labelAllCountText
        {
            get { return labelAllCount.Text; }
            set { labelAllCount.Text = value; }
        }

        public string labelErrorCountText
        {
            get { return labelErrorCount.Text; }
            set { labelErrorCount.Text = value; }
        }

        public Bitmap buttonLoginImage
        {
            get { return (Bitmap)toolStripButtonAutentication.Image; }
            set { toolStripButtonAutentication.Image = value; }
        }

        public string toolStripTextBoxInfoText
        {
            get { return toolStripTextBoxInfo.Text; }
            set { toolStripTextBoxInfo.Text = value; }
        }

        public Color buttonAutoCreateBackColor
        {
            get { return buttonAutoCreate.BackColor; }
            set { buttonAutoCreate.BackColor = value; }
        }

        ProgressBar IMainForm.progressBarStatistic
        {
            get { return progressBarStatistic; }
            set { progressBarStatistic = value; }
        }
        
        public string textBoxFullNameText
        {
            get { return textBoxFullName.Text; }
            set { textBoxFullName.Text = value; }
        }
 
        public string formCaption
        {
            set { this.Text = value; }
        }

        CheckedListBox IMainForm.checkedListBoxService
        {
            get { return checkedListBoxService; }
            set { checkedListBoxService = value; }
        }

        CheckedListBox IMainForm.checkedListBoxWorkGroup
        {
            get { return checkedListBoxWorkGroup; }
            set { checkedListBoxWorkGroup = value; }
        }

        CheckedListBox IMainForm.checkedListBoxCreator
        {
            get { return checkedListBoxCreator; }
            set { checkedListBoxCreator = value; }
        }

        public MainForm()
        {
            InitializeComponent();
        }

        public event EventHandler<EventArgs> buttonsClick;
        public event EventHandler<EventArgs> formClosing;
        public event EventHandler<EventArgs> formLoad;
        public event EventHandler<EventArgs> toolStripButtonClick;
        public event EventHandler<EventArgs> saveButtonEnabledEvent;
        public event EventHandler<EventArgs> timeoutChanged;
        public event EventHandler<EventArgs> contextMenuStripClickItem;
        public event EventHandler<EventArgs> contextMenuStripOpening;
        public event EventHandler<EventArgs> saveFileDialogEvent;
        public event EventHandler<EventArgs> toolStripButtonPropClick;

        private void toolStripButtonAutentication_Click(object sender, EventArgs e)
        {
            toolStripButtonClick?.Invoke(sender, e);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            
            formLoad?.Invoke(sender, e);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            formClosing?.Invoke(sender, e);
        }

        

        private void buttons_Click(object sender, EventArgs e)
        {
            buttonsClick?.Invoke(sender, e);
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            saveButtonEnabledEvent?.Invoke(true, e);
        }

        private void numericUpDownTimeout_ValueChanged(object sender, EventArgs e)
        {
            timeoutChanged?.Invoke((int)((NumericUpDown)sender).Value, e);
        }

        private void listViewData_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawDefault = true;
            e.DrawBackground();
        }

        private void listViewData_DrawColumnHeader(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void listViewData_DrawSubItem(object sender, DrawListViewSubItemEventArgs e)
        {
            if (e.Item.Selected)
            {
                e.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(250, 194, 87)), e.Bounds);
            }
            e.Graphics.DrawString(e.Item.Text, new Font("Arial", 10), new SolidBrush(Color.Black), e.Bounds);
        }

        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            contextMenuStripClickItem?.Invoke(sender, e);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            contextMenuStripOpening?.Invoke(sender, e);
        }

        private void toolStripButtonCSV_Click(object sender, EventArgs e)
        {
            saveFileDialog1.FileName = DateTime.Now.ToString("ddMMyyyy hhmm");
            saveFileDialog1.ShowDialog();
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            saveFileDialogEvent?.Invoke(sender, e);
        }

        private void toolStripButtonProp_Click(object sender, EventArgs e)
        {
            toolStripButtonPropClick?.Invoke(sender, e);
        }

        private void listViewData_DrawColumnHeader_1(object sender, DrawListViewColumnHeaderEventArgs e)
        {
            e.DrawDefault = true;
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
